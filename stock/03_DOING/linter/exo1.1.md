

1. Ajouter un test de qualité de code avec un linter
`npx eslint src/`
- npx est un equivalent de npn
- il a besoin des dépendances du projet

projet source : 

création d'une branche "eslinter" à partir de master

création d'un job quality test
qui appartient au stage test
avec un script npx  eslint src/
et récupérer au niveau du cache le dossier 'node_modules', produit par `fetch_dependencies`