# Mise en oeuvre d'un Workflow


---
## Les workflow de développement basés sur Git

Un développement logiciel comporte en général plusieurs phases : développement, intégration, préproduction, production, ... Chaque phase nécessitant des actions spécifiques sur le code des applications.

Dans chaque phases, plusieurs personnes peuvent intervenir, notamment durant la phase de développement.

Plusieurs versions d'un même logiciel sont également souvent gérées en parallèle.

Pour couvrir ces besoins Git permet d'utiliser des branches pour séparer le code. Mais il est nécessaire d'organiser ces branches pour structurer le développement :

- Normaliser les noms
- Définir qui a le droit de faire faire des push sur quelles branches
- Quelles branches sont autorisées à être déployées en production
- ...

C'est dans ce but qu'a été crée le workflow de développement [GitFlow](gitflow.md).
qui propose une méthodologie pour gérer les différentes étapes du cycle de vie du développement logiciel.

GitFlow est très complet, mais assez complexe à mettre en oeuvre.

Dans la pratique devops qui privilégie des petits projets, et des petites équipes, des workflow plus simples comme GitHub Flow, ou [GitLab Flow](./gitlabflow.md) sont plutôt utilisés.

---
## Implémentation des workflow avec Gitlab

L'implémentation d'un Workflow de type GitlabFlow avec Gitlab-CI peut êtr réalisée en s'appuyant sur plusieurs leviers :

- Les [régles et filtres]
- Les branches protégées
- Les merge request
- L'utilisation de template

Protéger des branches : https://www.lirmm.fr/~fmichel/ens/cours/GitLab_Flow.pdf
