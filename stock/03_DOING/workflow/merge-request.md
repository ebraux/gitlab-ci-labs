

Toute modification du code devrait passer par une merge request

Menu **"Plan > Issues"**, "New issue"

Une fois crée, on peut l'éditer et cliquer sur **"Create Merge request"**

Par défaut, Gitlab reprend le nom de l'issue, et créer une merge request en mode "draft"
`Draft: Resolve "feature/feature-A.12"`

Une nouvelle branche est créée

Menu **"Code > Merge Request"**, **"New merge requets"**

Source Branch: la branche à partir de laquelle on souhaite créer la nouvelle branche
Une nouvelle 