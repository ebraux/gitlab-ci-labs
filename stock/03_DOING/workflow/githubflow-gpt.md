# GitHub Flow

Modèle de gestion de branches dans Git, conçu pour les équipes qui souhaitent une approche simple, efficace, et axée sur les livraisons continues (Continuous Delivery). 

Il a été introduit par GitHub pour faciliter le flux de travail des équipes qui déploient fréquemment, sans avoir besoin de la complexité ajoutée par des modèles comme Gitflow.

Contrairement à Gitflow ou GitLab Flow, **GitHub Flow** se concentre sur un flux simplifié, avec une branche principale stable et des branches de courte durée pour les nouvelles fonctionnalités ou corrections de bugs. Il est idéal pour les équipes qui travaillent sur des projets où les changements peuvent être intégrés et déployés rapidement et régulièrement.

### Principes clés de GitHub Flow :

1. **Une branche principale (`main` ou `master`)**
   - La branche principale est la seule branche longue durée dans le projet.
   - Elle doit toujours être stable, prête à être déployée, et refléter le dernier code en production.
   - Toutes les nouvelles fonctionnalités ou corrections de bugs sont fusionnées dans cette branche après avoir été testées et validées.

2. **Branches de fonctionnalités (Feature Branches)**
   - Pour chaque nouvelle fonctionnalité, correction de bug ou tâche spécifique, une **branche de fonctionnalité** est créée à partir de la branche principale.
   - Le nom de la branche reflète généralement la nature de la tâche (par exemple, `feature/nouvelle-fonctionnalite`, `fix/bug-001`).
   - Une fois le travail terminé sur cette branche, elle est fusionnée dans la branche principale via une **pull request**.

3. **Pull Requests**
   - Lorsqu'une fonctionnalité ou une correction est prête, une **pull request** (PR) est ouverte pour demander que les changements soient fusionnés dans la branche principale.
   - La pull request est un mécanisme qui permet à d'autres membres de l'équipe de revoir le code, de proposer des modifications ou de discuter de l'implémentation avant qu'elle ne soit fusionnée.
   - Une fois validée, la branche de fonctionnalité est fusionnée dans `main`.

4. **Déploiement continu**
   - Dès qu'un changement est fusionné dans `main`, il est généralement prêt à être déployé en production. GitHub Flow fonctionne bien avec des outils de **déploiement continu** (CD) où chaque changement fusionné est automatiquement déployé ou rendu disponible pour être déployé.
   - Cela garantit que la branche principale est toujours dans un état déployable.

5. **Tests continus**
   - Bien que GitHub Flow ne dicte pas de règles strictes pour les tests, il est fortement recommandé d'utiliser une **intégration continue** (CI) pour s'assurer que le code sur les branches de fonctionnalité passe par des tests automatiques avant d'être fusionné.
   - Cela garantit que les changements qui arrivent dans `main` sont toujours testés et validés.

### Exemple de workflow GitHub Flow

1. **Création d’une branche de fonctionnalité** :
   - Un développeur crée une nouvelle branche pour travailler sur une fonctionnalité : `feature/ajouter-authentification`.

2. **Développement et commits sur la branche** :
   - Le développeur travaille sur cette branche et ajoute des commits au fur et à mesure qu’il avance.

3. **Ouvrir une Pull Request (PR)** :
   - Une fois que la fonctionnalité est terminée, le développeur ouvre une pull request pour que d'autres membres de l'équipe examinent le code et valident les changements.

4. **Revue de code et tests** :
   - L'équipe fait la revue du code et exécute les tests (manuels ou automatiques) pour vérifier que tout fonctionne correctement.

5. **Fusionner la pull request** :
   - Après validation, la branche de fonctionnalité est fusionnée dans `main`.

6. **Déploiement** :
   - Si un système de CI/CD est en place, le code est automatiquement déployé en production une fois la pull request fusionnée.

7. **Suppression de la branche de fonctionnalité** :
   - La branche de fonctionnalité est ensuite supprimée, car elle n'est plus nécessaire une fois fusionnée dans `main`.

### Avantages de GitHub Flow

1. **Simplicité** :
   - GitHub Flow est beaucoup plus simple à comprendre et à mettre en œuvre que Gitflow, car il n’y a pas de branches intermédiaires comme `develop` ou `release`.
   - Cette simplicité est idéale pour les équipes qui adoptent un cycle de livraison continue et des déploiements rapides.

2. **Fusions rapides** :
   - Comme il n'y a qu'une seule branche stable (la branche principale), les développeurs peuvent rapidement fusionner leurs changements une fois qu'ils ont été validés, ce qui permet des livraisons plus fréquentes.

3. **Collaboratif** :
   - Grâce aux pull requests, les développeurs peuvent travailler en collaboration, discuter des changements, et faire des révisions de code avant de fusionner quoi que ce soit dans la branche principale.

4. **Adapté au déploiement continu** :
   - GitHub Flow est conçu pour être utilisé avec des pipelines d'intégration et de déploiement continu (CI/CD), ce qui permet de déployer fréquemment et en toute sécurité.

### Inconvénients de GitHub Flow

1. **Pas de gestion des environnements complexes** :
   - GitHub Flow fonctionne bien dans des environnements simples où il n'y a qu'une seule version du code à déployer (par exemple, production), mais il peut être limité dans des environnements plus complexes (comme ceux avec des environnements de staging ou de pré-production).
   - Pour gérer plusieurs environnements, les équipes peuvent devoir adopter un workflow plus flexible comme **GitLab Flow**, qui offre des branches spécifiques à chaque environnement.

2. **Risque de bugs en production** :
   - Puisque le code est fusionné directement dans `main` et potentiellement déployé immédiatement, il est essentiel que les tests soient bien en place pour éviter d'introduire des bugs en production. Si les tests ne sont pas rigoureux, cela peut être risqué.

3. **Non adapté aux projets avec des cycles de version longs** :
   - Si une équipe travaille sur des fonctionnalités qui prennent beaucoup de temps à développer, GitHub Flow peut ne pas être adapté. Dans ce cas, des workflows plus complexes comme Gitflow pourraient être nécessaires pour gérer le développement en parallèle.

### Conclusion

**GitHub Flow** est un modèle simple et efficace pour gérer les branches dans Git, adapté aux équipes qui cherchent à adopter des déploiements continus avec une intégration fréquente. Il est idéal pour les projets où les changements peuvent être fusionnés rapidement et où les processus de revue de code et de tests automatiques sont bien établis. Cependant, il est moins adapté aux environnements complexes ou aux équipes qui nécessitent une gestion plus stricte des versions et des environnements, où des modèles comme GitLab Flow ou Gitflow peuvent être préférables.