# Le modèle GitFlow

---
## Introduction

Le modèle Gitlabflow a été proposé par Gitlab. C'est une adaptation du modèle [gitflow](gitflow.md) avec un focus sur :

- Une simplification du processus, pour mieux correspondre aux besoins de pratiques de livraison continue (CI/CD).
- Une meilleure gestion des environnements de déploiement multi-environnements (staging, production, etc.)

---
## Les types de branches

Le GitLabFlow se base sur :

- Une **branche principale**, stable, qui contient le code prêt à être déployé. C'est la branche `main` (ou master) du projet.
- Des **Feature branches**, 
    - Permettant de développer de nouvelles fonctionnalité
    - Créé à partir de `main`
    - A pour finalité d'être à fusionné à `main`.

---
## Les processus

Les processus de GitlabFlow partent du principe de fusionner :

- Uniquement quand la `feature` est prête
- Directement dans la branche principale

Ce qui impose donc des déploiements fréquents. Et un retour arrière rapide et simple.

Avec ce modèle, dans le cas d'un projet simple, le workflow peut être basé sur les fonctionnalités. Pour chaque nouvelle fonctionnalité, création d'une `feature branch`, traitement et fusion avec `main`. Pour améliorer la traçabilité, le nom de la `feature branch` peut être adapté au cas d'usage (feature, release, hotfix).

Dans le cas d'une organisation composées de plusieurs environnements, par exemple développement, staging, production, ... il est possible d'utiliser des tags, indiquant une version du développement, et d'utiliser ce mécanisme de tags pour déployer dans un environnement spécifique. 

Dans le cas d'un projet important, ou d'une organisation avec plusieurs environnements, la gestion des `Feature branches` devient vite complexe, et le mécanisme de tags entraîne un risque d'erreur de flux entre les environnements. Sans une gestion rigoureuse, une version de développement pourrait être déployée accidentellement en production.

Pour sécuriser les flux, GitlabFlow propose la notion d'**environnements**

---
## Les environnements

GitLabFlow propose de créer une branche principale par environnement. Ce qui permet de multiplier et de cloisonner les environnements, tout en gardant un workflow simple.

La notion d'environnement n'est pas native dans GIT, il faut donc un mécanisme externe permettant leur gestion. 

La gestion des pipelines de CI/CD de Gitlab propose des fonctionnalités adaptées à ce fonctionnement, avec la gestion de condition d'execution des jobs, et d'environnements.

Avec ce fonctionnement la branche `main` peut être considérée comme la branche stable du développement similaire à `develop` dans GitFlow.

Pour éviter qu'un push sur la branche `main`, entraine une mise en production, une branche dédiée à la production est souvent mise en place.

---
## Recommendations / Bonnes pratiques

1. Ne pas faire de commits directement sur ma branche `main`
    - Utiliser systématiquement des `features branches`, et idéalement le mécanisme de pull request
2. Maintenir la branche `main` fonctionnelle
    - tester systématiquement tous les commits effectués sur cette branche
3. Pour les features branches, accepter uniquement des commits de code valide : tester systématiquement à chaque commit.
4. Tout développement doit se baser sur la branche `main`, et re-fusionner avec la branche main
5. Les correctifs de bug doivent être appliqués sur la branche main, puis ensuite propagées aux branches de feature.
7. Idéalement, les branches de staging et de production doivent être des versions de la branche main, via une gestion de tags. Si ce sont des branches dédiées, les modification ssur ces brnche doivent passer par des merge-request  main->staging et staging->production

10. Généraliser les revues de code ???
11. pour éviter les déploiement systématique en production mettre en place une branche `production`
12. utiliser des tags
13. un tags, une release

14. Ne pas faire un `rebase` sur une branche publique, car si d'autre personne utilisent cette branche, ....


---
## Ressources

- [https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/](https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/)
- [https://github.com/jadsonjs/gitlab-flow](https://github.com/jadsonjs/gitlab-flow)
- [https://about.gitlab.com/topics/version-control/what-are-gitlab-flow-best-practices/](https://about.gitlab.com/topics/version-control/what-are-gitlab-flow-best-practices/)