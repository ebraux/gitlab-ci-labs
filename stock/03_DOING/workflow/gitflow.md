# Le modèle GitFlow

---
## Introduction

Le modèle Gitflow a été proposé par Vincent Driessen en 2010.

Il repose sur 2 branches stables : 

- `develop` comme base pour tout nouveau développement
- `main` toujours prête a être déployée en production.

Il permet de :

- Bien séparer le développement des nouvelles fonctionnalités, les corrections de bugs, et les préparations de versions
- Gérer des développement en parallèle : plusieurs branche de soutien peuvent cohabiter
- Tracer avec précision ce qui a ét fait et surtout pourquoi

Mais il est  assez complexe et lourds à mettre en oeuvre, ce qui peut être un frein pour les petits projets ou les petites équipes.
Pour des projets avec des cycles très court, il entraîne la création de beaucoup de branches, ce qui peut rapidement devenir complexe sans mécanisme de gestion/automatisation.


---
## Les types de branches

Gitflow se base sur 2 types de branches : 

- Les branches principales : les versions stables du code
- Les branches de soutien : les versions du code en cours de traitement


Branches principales : version "stable", du code

- **main** (anciennement master) : branche principale qui contient toujours une version stable du code, prête pour la production.
- **develop** : dernière version du code en développement, après intégration de nouvelles fonctionnalités, versions, ou correction de bug. Le code dans cette branche n'est pas encore validée pour la production

Branches de soutien : nouvelle fonctionnalité, nouvelles versions, ....

- **Feature branches** : 
      - Développement d'une nouvelle fonctionnalité.
      - Une branche par fonctionnalité.
      - Créé à partir de `develop`
      - A pour finalité d'être à fusionné à `develop`.
      - Dans la pratique, le nom de ce type de branche est souvent `feature/nom-de-la-fonctionnalité`
- **Release branches** : 
      - Développement d'une nouvelle version.
      - Créé à partir de **develop**
      - Permet de finaliser une version : correctifs de bugs
      - A pour finalité d'être à fusionné à **develop** et **main**
      - Dans la pratique, le nom de ce type de branche est souvent`release/numéro-de-version`
  - **Hotfix branches** :
      - Correction de bug critique en production
      - Modifier le code de production, en urgence sans passer par les phase de **develop**
      - Créé à partir de **main**
      - A pour finalité d'être à fusionné à **main**  et reporté sur **develop**
      - Dans la pratique, le nom de ce type de branche est souvent : `hotfix/nom-du-bug`

---
## Les processus

Le processus de développement d'une nouvelle fonctionnalité est  :

- Création d'une `Feature branch` à partir de `develop`
- Développement, tests unitaires
- Tests d'intégration
- Puis fusion avec  `develop`

Le processus de développement d'une nouvelle version est :

- Création d'une `Release branch` à partir de `develop`
- Tests fonctionnels, ajustement et corrections mineures
- Fusion avec `develop`, et `main` : Validation d'une nouvelle version en production

Le processus de développement d'un hotfix est :

- Création d'une `Hotfix branch` à partir de main
- Correction du bug, tests
- Fusion avec `main` et `develop`


---
## Ressources :

- [https://git-flow.readthedocs.io/fr/latest/presentation.html](https://git-flow.readthedocs.io/fr/latest/presentation.html)
- [https://grafikart.fr/tutoriels/git-flow-742](https://grafikart.fr/tutoriels/git-flow-742)
- [https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow)