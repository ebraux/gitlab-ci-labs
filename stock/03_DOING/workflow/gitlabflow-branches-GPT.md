Dans un workflow **GitLab Flow**, le code passe de la branche `main` vers les branches `staging` et `production` de manière progressive et contrôlée via des **merge requests** (MR), des **pipelines CI/CD**, et parfois des **approbations manuelles**. Le processus typique pour ce flux de travail ressemble à ceci :

1. **Développement dans des branches de fonctionnalités** (`feature branches`).
2. **Merge request vers la branche `main`**.
3. **Fusion du code testé et validé dans `main`**.
4. **Merge request de `main` vers `staging`** (pour tests en environnement de pré-production).
5. **Merge request de `staging` vers `production`** (pour déploiement en production).

Voici une explication plus détaillée du processus, en illustrant le passage entre les branches `main`, `staging`, et `production` :

### 1. **Travail dans les Branches de Fonctionnalités** (Branches `feature`)

- Les développeurs créent des branches de fonctionnalités basées sur la branche `main` (par exemple, `feature/nouvelle-fonction`).
- Ils travaillent sur cette fonctionnalité en y ajoutant des commits.

### 2. **Merge Request vers la Branche `main`**

- Une fois qu'une fonctionnalité est terminée, une **merge request** (MR) est créée pour fusionner cette branche de fonctionnalité dans la branche `main`.
- Le pipeline CI est déclenché pour exécuter des **tests automatisés** sur la merge request.
- Si les tests passent et si la merge request est approuvée (selon les règles de revue de code de l'équipe), le code est fusionné dans `main`.

### 3. **Fusion dans `main` et Déploiement vers `staging`**

- Une fois le code fusionné dans la branche `main`, l'étape suivante est de le déployer dans un environnement de **pré-production** ou `staging` pour d'autres tests avant d'aller en production.
  
#### Deux scénarios pour ce déploiement :
  - **Manuellement via une Merge Request** :
    - Après avoir fusionné dans `main`, une **nouvelle merge request** est ouverte pour fusionner `main` dans `staging`. C'est un processus manuel où l'équipe décide quand envoyer le code en pré-production.
    - Le pipeline CI/CD exécute des tests supplémentaires avant de déployer automatiquement dans l'environnement de staging après la fusion.
  
  - **Automatiquement** (optionnel) :
    - Vous pouvez configurer GitLab pour que tout code fusionné dans `main` soit automatiquement déployé dans `staging`, déclenchant automatiquement un déploiement après les tests réussis sur `main`.

Voici un exemple de commande Git pour créer une merge request vers `staging` depuis `main` :

```bash
git checkout staging
git merge main
git push origin staging
```

Cette approche repose sur le déclenchement d'un pipeline CI/CD pour tester et déployer le code dans l'environnement de staging.

### 4. **Tests dans l'Environnement de Staging**

- Une fois que le code est sur la branche `staging` et déployé en pré-production, des tests supplémentaires peuvent être effectués (tests manuels, tests d'intégration, validation par les parties prenantes).
- Si les tests sur staging sont concluants, le code est prêt pour la production.

### 5. **Fusion de `staging` vers `production`**

- Après validation en staging, une **merge request** est créée pour fusionner `staging` dans `production`. Encore une fois, cette étape peut être manuelle pour garantir qu'une décision humaine soit prise avant de passer en production.
  
Voici la commande pour fusionner `staging` dans `production` :

```bash
git checkout production
git merge staging
git push origin production
```

- Une fois la fusion effectuée, un pipeline CI/CD est déclenché pour déployer automatiquement le code dans l'environnement de **production**.
- En général, le déploiement vers `production` est manuel et nécessite des validations supplémentaires (via des approbations ou des boutons de déploiement manuel dans GitLab CI).

### 6. **Déploiement en Production**

- Après la fusion dans `production`, le pipeline GitLab déploie le code sur les serveurs de production. Ce processus peut être entièrement automatisé, mais il est souvent configuré pour nécessiter une intervention humaine (par exemple, un bouton de déploiement manuel dans GitLab CI).

### Exemple de `.gitlab-ci.yml` avec Workflow GitLab Flow

Voici une version améliorée du fichier `.gitlab-ci.yml` qui illustre ce passage du code de `main` à `staging`, puis de `staging` à `production` :

```yaml
stages:
  - install
  - test
  - deploy

variables:
  PYTHON_VERSION: "3.9"
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  VIRTUAL_ENV: "$CI_PROJECT_DIR/venv"
  FLASK_ENV: development

cache:
  paths:
    - .cache/pip

before_script:
  - apt-get update && apt-get install -y python$PYTHON_VERSION python$PYTHON_VERSION-dev python$PYTHON_VERSION-venv
  - python$PYTHON_VERSION -m venv $VIRTUAL_ENV
  - source $VIRTUAL_ENV/bin/activate
  - pip install --upgrade pip
  - pip install -r requirements.txt

# Étape 1 : Installer les dépendances sur toutes les merge requests
install_dependencies:
  stage: install
  script:
    - echo "Installation des dépendances"
  only:
    - merge_requests

# Étape 2 : Lancer les tests unitaires uniquement sur les merge requests vers main
test:
  stage: test
  script:
    - echo "Lancement des tests unitaires"
    - pytest tests/
  only:
    - merge_requests
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'

# Étape 3 : Déployer sur staging après tests
deploy_staging:
  stage: deploy
  script:
    - echo "Déploiement sur l'environnement staging"
  environment:
    name: staging
    url: https://staging.example.com
  only:
    - staging
  when: manual  # Le déploiement doit être approuvé manuellement

# Étape 4 : Déployer sur production manuellement après validation
deploy_production:
  stage: deploy
  script:
    - echo "Déploiement sur l'environnement production"
  environment:
    name: production
    url: https://www.example.com
  only:
    - production
  when: manual  # Le déploiement doit être approuvé manuellement

cleanup:
  stage: deploy
  script:
    - echo "Nettoyage des fichiers temporaires"
  when: always
```

### Récapitulatif du Processus :
1. **Merge request vers `main`** → Tests automatisés → Fusion dans `main`.
2. **Merge request vers `staging`** → Déploiement manuel dans staging → Tests supplémentaires.
3. **Merge request vers `production`** → Déploiement manuel en production.

Ce processus garantit que le code passe par des étapes de validation strictes avant d'être déployé en production.