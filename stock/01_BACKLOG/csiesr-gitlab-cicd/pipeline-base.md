un processus, décomposé en job, et chapitré en stage

pipiline
 - stage
   - job

Un seul pipeline par projet

On peut avoir des point d'entrée, en fonction des branche, ...

le fichier gitlab-ci.yaml, et doit se trouver à la racine du dépot.
(configurable dans la config du projet si on veut le mettre ailleurs)

C'ets du yaml


par dfaut 5 stage sont pré-definis
- .pre
- build
- test
- deploy
- .post

".pre" et ".post" sont gérés par gitlab

On peut directement affecter des jobs à "build", "test" et "deploy"

Un job :
- un nom
- rattaché à un "stage"
- une action
  
il y a des nom réserve : images, service ,...

Il y a plusiseurs status pour un job : failed, warning,... 


---
## démarre un pipeline

editeur de pipeline
- permet de nous aider à travailler sur le pipelin
- configure le pipeleine : créer le fichier gitlab-ci.yaml

Propose les 3 stages par defaut
on peut ajouter nos stages.

ensuite liste des jobs

Par défaut, chaque commit déclenche le pipeline.

on peut ensuite voir le lancement du job
accèder à son état
et les logs.

CI/CD catalogue
https://gitlab.com/explore/catalog

---
## infos Gitlab
``` yaml
# This file is a template, and might need editing before it works on your project.
# This is a sample GitLab CI/CD configuration file that should run without any modifications.
# It demonstrates a basic 3 stage CI/CD pipeline. Instead of real tests or scripts,
# it uses echo commands to simulate the pipeline execution.
#
# A pipeline is composed of independent jobs that run scripts, grouped into stages.
# Stages run in sequential order, but jobs within stages run in parallel.
#
# For more information, see: https://docs.gitlab.com/ee/ci/yaml/index.html#stages
#
# You can copy and paste this template into a new `.gitlab-ci.yml` file.
# You should not add this template to an existing `.gitlab-ci.yml` file by using the `include:` keyword.
#
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Getting-Started.gitlab-ci.yml
```
