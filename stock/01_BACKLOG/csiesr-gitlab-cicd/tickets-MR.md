

pas uniquem de tickets de dfaut ou de maintennace.

le worflow :
1. démarrer un ticket
   1. donner le plus d'infos possibles dans la description
   2. assigner à qualq'un
   3. réaliser des jalons (sprint) (optionnel)
   4. associer un label (optionnel)
      1. par exmeplsi on met des ticket non techniques
   5. date d'échanace
2. on retrouve le ticket dans la liste du projet
3. assigner
   1. activation d'un compteur de temps à l'assignation
4. tableau  des tickets : par dafut : ouvert/fermé
   1. peut être personnalisé avec des labels

Suivi du temps : on peut estimere le temps
dans l'onglet analyse, on peut ensuite voir la fiabilité des estimations

On peut ensuite transformer le ticket en requète de fusion (merge request)
depui le ticket, bouton create MR
- créer une branche, dont le nom dépendra du nom du ticket
- on peut associer un releteur; milestone ...
- options de fusion :
  - supprimer la branche quand c'est fini
  - écraser les validations : fait un squash : désactivé pardéfaut
      - Attention génére un long messge de commit

> dans le parametre du projet, on peut configurer l'option du squash cochée par défaut

une branche est créée.
Une merge request est créée, avec le status brouillon
Dans l'interface, on peut copier le nom de la branche

la MR est crée en mode "brouillon".
- c'est pas la MR qui est un brouillon
- c'est le travail dans la MR qui est un brouillon (draft)


``` bash
git branch
* main

git pull origin 1-modifier-le-titre-en-rouge
From gitlab.com:ebraux/simple-calculator
 * branch            1-modifier-le-titre-en-rouge -> FETCH_HEAD
Already up to date.

git switch 1-modifier-le-titre-en-rouge


Différence avec les pull requet de github : elles se fnt en amont, et non à la fin 

passer la MR en mode pas bruillon
editer la MR, et décoccher "broillin"
ça enlev drfat dan sle titre

La MR peut maintenant être validée
on peut encore changer le comportement de la fusion

on clique sur fusionner

La modif est intégrée à main
- la branche est supprimée
- le ticket est fermé

Attention, le ticket est fermé automatique uniquement sur la branche par défaut

---
- on peut les gerer avec des labels pour les organiser
- on peut les planifier avec des label
- on peut faire du kamban

Gitlab propose un jeux de label par dfaut :
bug, confirmed, critical, ...

Dans le tableux, on peut affiner des colonne en fonction des labels
si on déplace les ticket dans le tableau (drag and drop, le label cahneg)
si on ferme un ticket, les labels restent.

labels et kamban.
le changement de label doit se faire manuellement
c'est pas idéal.
il n'y a aps de relation entre les tickets et les MR.
Donc pas mal de gestion nativement .beaucoup de manuel.


on peut faire des modéles de ticket.
- titre
- description
Ajouter dans un dossier .gitlab/issue_templates du projet
 On peut aussi ajouter des infos dans le ticket : https://docs.gitlab.com/ee/user/project/quick_actions.html

Les template sont gérés par projets, pas de gestion centralisée