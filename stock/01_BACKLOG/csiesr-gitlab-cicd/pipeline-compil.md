
ecrire un nouveau job

``` bash
stages:          # List of stages for jobs, and their order of execution
  - build
  - test
  - deploy


fetch_dependencies:
    stage: build
    script: 
        - npm install

build_js:
    stage: build
    script: 
```

ça plante
- les 2 jobs s’exécutent en même temps
- l'installe de mpm echoue.

Par défaut, les jobs s’exécutent en parallèle (dans un stage).
Mais on peut gérer l’ordonnancement avec `needs`
``` bash
build_js:
    stage: build
    script: 
        - npm run build
    needs:
      - fetch_dependencies
```

Et chaque runner fonctionne à partir d'une image de base. ici par défaut : ruby, qui n'a pas npm
on peut gérer l'image avec  `image: node:lts`
- soit au niveau de chaque job
- soit de façon globale

Le build plante, car les dépendances ne sont pas présente.

chaque job s'execute dans un conteneur indepenandt, qui est créé et supprimé à la fin de chaqu ejob.
pour conserver , on utiliser des artcifacts

Le "fetch_dependencies" produit le dossier "nodes_modules" qui contient les dépendnaces.
``` bash
    artifacts:
        paths:
          - node_modules
```

mais dans le cas présent, le nod_module n'a pas d'interêt a etre conservés.
il vaut mieux utiliser le cache.