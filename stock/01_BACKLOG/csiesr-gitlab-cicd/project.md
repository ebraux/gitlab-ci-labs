# projet vide

nom : Majuscule, espaces, OK: c'ets 'slugifié"

Project URL
- personnel :  dans 
- Si des groupes on été créés: créer un projet de groupe

Cible de déploiement
- structutre de dépoiment : K8S, Heroku, Gitlab Pages, ...
- Permet de proposer directement un pipeline type pour ces différents environnements
  
Niveau de visibilité :
- privé : inviter les gens pour voir le projet
- interne : uniquement aux utilisteurs connecté
- public : accessible sans authentification

Configuration :
initialiser avec un README : 
- initialise le projet
- ne pas cocher pour intégrer un projet existant.
  

récupérer le projet 
le modifier, et faire un push.

En http, c'est possible. mais pas recommandé.
Utiliser des clés SSH
- plus de sécurité : pas de mot de passe qui transite
- plus de simplicité : pas de saisie de mot de passe

Le https reste une solution viable, pour des usages ponctuels.
Autre option : définir un TOKEN, qui permettre de faire une authentification, sans utiliser ses credentials.

A la première connexion, il faut accepter la signature du serveur gitlab.