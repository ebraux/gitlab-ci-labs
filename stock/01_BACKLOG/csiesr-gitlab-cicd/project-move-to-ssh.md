# Move project url form HTTPS to SSH

Si on a cloné un projet en HTTP, il est connu en tant qu'origine HTTP.


Si on veut utiliser une clé, il faut modifier le protocole utilisé pour origine.
- copier l'adresse utilisant le protocole git dans l'interface
- remplacer l'origine dans le projet
  - `git rm origine`
  - `git remote add origin git@gitlab.com:ebraux/simple-project.git`
  - `git push -u origin main`