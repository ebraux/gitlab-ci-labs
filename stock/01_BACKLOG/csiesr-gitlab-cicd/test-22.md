

bug 2+2 = 22
à a jouter dans le test unitaire, pour ne pas géré si c'ets une sting

- créer un ticket
- se l'assigner
- créer une MR (activer suppression et squash)
- se l'assigner.

en local, raprtier la branche
`git pull origin NOM_BRANCH`
`git switch  NOM_BRANCH`

modification du fichier de test
ajout d'un test. fichier sum.test.js --> test somme en chaine de caractère
``` bash
test(' 1+2 ... strings')
  sum '1' + '24
```

lancer le test en manuel : `npm test`
--> le test échoue

Dans la chiane d eCI
git add + git commit + push
Dans gitlab, on retrouve dans la MR :
 - le pipeline de CI s'es déclenché
 - le test échoue dans le pipeline
Dans la MR, on voit que le pipeline est en échec
Dans la version gratuite, on ne peut pas bloquer une MR si le pipeline est en échec.

rem : Si un job plante dans un stage, le stage suivant n'est pas lancé (sauf exception explicite)

Quand un job échoue, on a un onglet ave le log de sortie.

Il y a aussi un onglet "test"
- gitlab peut faire une analyse des tests (couverture, ...)
- le fait d'avoir des jobs dans test n'est pas suffisant
- il faut les intégrer. 
  - il faut produire un rapport
  - principe : https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html
  - exemples : https://docs.gitlab.com/ee/ci/testing/unit_test_report_examples.html
  - formats : https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html

> Pour jest :  'jest --ci --reporters=default --reporters=jest-junit'
>  - en mode CI, rapport au format junit

Si on ajoute le test avec jest, et qu'on push .
le pipeline se relance
- et comme le rapport est généré et visible l'onglet "test"
Les test échouent.
modification de la fonction sum, pour ajouter `parseint()`