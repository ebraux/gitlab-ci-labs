

---
## diagramme d'activité UML

écrire sur un worflow quels sont es flux, et quels sont les embranchement des workflow

avoir un diagramme d'activité, c'est très utile
Important de le faire, et de l'inclure au projet.

qu'est ce que c'est :
- des étape dans des bulles
- de sflèches qui relient les bulles
- commence par un point
- finissent par un point encerclé
- il y a aussi des transitions conditionelles 
  - si oui ça, si non ça
- séparation et jointures
  - bifurcation de 2 flux
  - 2 flux se rejoingnent
- on peut "lotir", "chapitrer" des actions, pour avoir différents niveau de profondeur

---
## couloirs d'activité
 - pour savoir qui fait quoi
 - on retouve le diagramme, dans des colonnes séparées en général par acteur
  
outil : draw.io
 partir sur u modèle vide
 et construire avec le tab "UML"
 --> ne pas faire quelque chose de précis, mais quelque chose d eclair :
 --> simple à lire, et facile à comprendre
 --> rester simple, fonctionnel, pas trop technique