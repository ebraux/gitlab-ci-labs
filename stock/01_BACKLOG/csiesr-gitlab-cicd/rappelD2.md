
fichier gitlabci

division en tache
- grand chapitre : stage
- plus petits, taches à réaliser : les jobs

Stage par défaut : 
- .pre, .post
- build, test, deploy

Quand on doit avoir dans notre diagrame d'état, deux job qui se rejoingnet, il faut créer un nouveau stage.


Le fichier gitlab-ci.yml est le point d'entrée par défaut (configurable)
il peut êtr décomposé en plusieurs fichiers, en utilisant include.


Un job peut être "surchargé", hériter d'un autre job.
- mot clés extends
- personnalisation :
  - en ajoutant des élèment de configuration (image, scripts, ...)
  - en déclaration des variables (utilisée dans le job parent)
Include peut aussi êtr utilisé pour récupérer des job depuis un dépôt centralisé

Un job dont le nom commence par un '.' ne sera pas executé.

Bonne pratique :
- centraliser des configuration communes dans job parent
- lui donner un nom commençant par un '.'
- et le décliner dans des job avec "extend"

Des jobs de référence peuvent être centralisés dans un dépôt, ou un site WEB, et intégrés avec "include".

Régles : 
- When
- if
- changes
- exists
- allow_failure

Le when :
- on_failure / on success / always / never
- manual
- delayed

On peut utiliser des variables prédéfinies, ou définir ses propres variables.

Ex de variables par défaut : CI_DEFAULT_BRANCH, CI_COMMIT_BRANCH ou CI_COMMIT_REF_NAME.

Créer ses propres variables :
- Dans l'interface de gitlab : Menu Paramètres/CICD/Variables
- directement dans le pipeline 
  - ne pas répéter les informations dans un job
  - personnaliser un job hérité.

Les Runnenr

créer un runner 
 paramètres / CI/CD/ Runners

2 types de runners
 - runner partagés : fourni par le gestionnaire de gitlab
 - runner custom : a installer sur une machine (la votre ou un serveur)

La gestion de l'affectation des jobs aux runners est gérée via des tags
- des tags sont associés aux Runner
- des tags sont associés aux jobs
- les jobs s'executent sur les runner qui portent le même tag qu'eux.

Les runner peuvent être configurés pour accepter les job sans tags.
