

les tests c'est extremement important.

Dépendnet d'un framework.

chaque application a son farmawork 

javascript : getser, moka
php : phpuni
jave junit

et donc sa manière de lancer les tester

Gitlab integre des processus de getsion de rsultats des tests.
- couverture de test
- 


Les tests unitaires : Les tests unitaires consistent à
tester individuellement les composants de l’application.
On pourra ainsi valider la qualité du code et les
performances d'un module.

• Les tests d'intégration : Ces tests sont exécutées pour
valider l'intégration des différents modules entre eux et
dans leur environnement exploitation définitif. Ils
permettront de mettre en évidence des problèmes
d'interfaces entre différents programmes.

tests end to end : simuler le comportement d'un utilisateur
- saisir une données, cliquer sur un bouton, ...
- outils : selenium, panther (php)
- chronométrage des opérations
- c'est des outils ultra lourds
- Dans un pipeline, ca pose problème à cause du timeout
  - par défaut 1h
  - ajustable au niveau job
  - on peut faire des job sans timeout
- pb : un pipeline trop long c'est pas bon
  - les utilisateurs n'attendent pas le résultat ...
  - on retombe sur le problème des tests trop lourds, donc pas utilisés.
- bonne pratique :
  - pas sur les branches de feature
  - uniquement sur dev

tests de non regression:
- on ajoute quelque chose dans le projet, donc on modifie, donc on risque de casser
- bonne pratique : transformer chaque bug relevé en test de non regression

tests de frontend :
- vérification des ihm, de l'apparence
- peu répendu

Test de perf
- temps, ram, cpu, ...
- un nouveau dev fait ralentir le dev, ou appel d'api

Les tests d'installation
- deployer les choses, vérifier si elle fonctionnent
- peuvent prendre du temps.

---
## exemple de test
rédigé à partir d'une librairie de test "jest", et déjà intégré dans le code

test en local 
``` bash
npm jest
> jest
```

implémentation :
mettre en place un job dans le stage de test

pour bloquer le déroulement si le test échoue.