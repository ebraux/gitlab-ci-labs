

---
## qu'est ce que c'est que la CI (intégration continue)

- intégrer les développement au fur et a mesure
- disposer d'une application sans attendre la fin du projet
- tests / validation / compilation automatiques

Prendre en compte les retours utilisateurs, planifier les modif, les codes, et les intégrer au code de l'application

venir livrer au fur et à mesur
éviter effet tunnel
livrer des incréemnt facilement
---
## qu'est ce que c'est que la CI (déploiement continu)

- déployer au fur et à mesure
- déploiment automatique

Prendre en compte le code intégrer, le déployer et l'exploiter
déployer des incréments facilement
---
## D'où ça vient

Méthodologie de travail pour les dev : "eXtrem Programming"

Kent Beck , responsable technique de General Motors
- en 1999 refondre le système de paye, en un an
- au bout d'un an appli pas prête, demande délai, pression sur l'équipe
- mais c'ets en retard.
- il fini par livrer, et ça ne convient pas du tout.
- il faut tout recommencer.
- au bout de 2 ans et demi, c'est bon. mais finalement ils partent sur autre chose

Bilan :
- équipe épuisée : la moitié est en congé maldie, ou a démissione
- 2 ans et demi de travail pour rien
- la façon de travailler (comme sur une chaine de montage) n'est pas bonne 

Changement de pratiques :
- intégration continue :
  - cycles courts
  - fonctionner pas incréments : tous les 2 mois
  - tester le plus tôt possible par les utilisateurs
  - donc une mise en prod tous les 2 mois : c'est compliqué
- promouvoir des design pattern de développement
- rythme soutenable pour les dev et les équipes
- "pair programming" , qui deviendra "pair reviewing"
- tests unitaires : test de chaque module de façon indépendante
  - tester que tous les modules fonctionnet ensemble : tests d'intégration 
  - tester que ca répond au besoin : tests fonctionnel, end to ends

Il est à à l'origine de TDD (TDD Daddy), ...

L'EX a débouché sur l'agilité.

---
## Agilité

Année 2001
crée par des dev.
rapport sur le dev aux US sur les années 1990.
sur la totalité :
- 30% des projet étaient abandonns avent le fin des dev
- 30% livrée, mais abandonnées au bout de 2 ans, et remplacé
- 30% viables.
- 70% ne respectent pas les délais

Lire le manifeste agile. (agile manifesto)
Robert C. Martin : papa des design Pattern (livres très bons sur les bonnes pratiques
Ken Schwaber et Jeff Sutherland : scrum

https://agilemanifesto.org/
https://agilemanifesto.org/iso/fr/manifesto.html

---
##
les outil de l'environnemen tGitlab 
s'inspirent des methodes agile
permettent de les mettre en place.