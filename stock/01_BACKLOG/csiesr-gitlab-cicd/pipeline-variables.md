
---
## pré-définies

Il y a des variables pré-définies

importantes
- CI_COMMIT_BRANCH : la branche qui porte le pipeline
- CI_DEFAULT_BRANCH : le nom de la ranche par défaut
- CI_PROJECT_PATH : interressant si on vient factoriser les scripts de pipeline, pour pouvoir adapter les templates
- GITLAB_USER_NAME : le nom de l'utilisateur qui a déclenché le pipeline

liste https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

---
## Utilisation

commance par "$"
---
## Variables personnalisées