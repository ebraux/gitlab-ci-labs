

The default Git strategy for GitLab CI/CD is 'fetch'. If the same static runner is used for the same pipeline over and over, GitLab won't do a fresh clone but will just fetch changes on subsequent runs.

If you want a fresh environment each time you want the 'clone' strategy. There are two ways you can change this behavior:

Change the project default to 'clone' by going to project Settings -> CI/CD -> General pipelines -> Set 'Git strategy for pipelines' to git clone

Change the strategy via .gitlab-ci.yml:
```yaml
variables:
  GIT_STRATEGY: clone
```
You can learn more about this setting in the GitLab documentation - Git strategy [https://docs.gitlab.com/ee/ci/pipelines/settings.html#git-strategy](https://docs.gitlab.com/ee/ci/pipelines/settings.html#git-strategy)