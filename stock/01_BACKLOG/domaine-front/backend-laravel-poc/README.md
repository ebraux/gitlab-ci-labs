# Laravel API POC 

### Installation
Veuillez consulter le guide d'installation officiel de laravel pour connaître les exigences du serveur avant de commencer. [Documentation officielle](https://laravel.com/docs/8.x/installation)

#### 1 - Cloner le repository
    git clone https://gitlab.imt-atlantique.fr/domaine-front/backend-laravel-poc.git

#### 2 - Passez au dossier du repository
    cd backend-laravel-poc

#### 3 - Apportez les modifications de configuration requises dans le fichier .env
    

#### 4 - Installer toutes les dépendances à l'aide de composer
    composer install

#### 5 - Exécutez les migrations de base de données (**Définissez la connexion à la base de données en .env avant de migrer**) 
    php artisan migrate

#### 6 - Démarrer le serveur de développement local 
    php artisan serve
#### Vous pouvez maintenant accéder au serveur à l'adresse http://localhost:8000


