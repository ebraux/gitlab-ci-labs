<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;
use App\Models\Product;
use Illuminate\Http\Response;


class ProductTest extends TestCase
{

    public function testStoreProduct(){

        $user = User::factory()->create();
        Sanctum::actingAs($user);


        $product = array(
            "name" => "product TEST",
            "slug" => "product TEST",
            "description" => "desc TEST",
            "price" =>"1.00" ,
            "user_id" => 1,
        );

        $this->json('post', 'api/products',$product)
             ->assertStatus(Response::HTTP_CREATED) // ASSURE QUE LE LE HTTP STATUS EST 201
             ->assertJsonFragment($product); //ASSURE LES DONNEES
    }



    public function testIndexReturnsDataInValidFormat() {

        $data = array(
            array(
                "id" => 1,
                "name" => "product TEST",
                "slug" => "product TEST",
                "description" => "desc TEST",
                "price" =>"1.00" ,
                "user_id" => 1,
                "created_at" => now()->format('Y-m-d')."T".now()->format('H:i:s').".000000Z",
                "updated_at" => now()->format('Y-m-d')."T".now()->format('H:i:s').".000000Z"
            )
        );

        $this->json('get', 'api/products')
            ->assertStatus(Response::HTTP_OK) // ASSURE QUE LE LE HTTP STATUS EST 200
            ->assertJsonFragment($data); //ASSURE LES DONNEES
    }

    // public function test(){
    //     $this->assertTrue(false);
    // }


}
?>