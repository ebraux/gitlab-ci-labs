<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use DB;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @description CETTE FONCTION POUR OBTENIR TOUS LES PRODUITS
     */
    public function index()
    {
       return Product::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @description CETTE FONCTION POUR AJOUTER UN PRODUIT
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=> 'required|string',
            'slug'=> 'required|string',
            'price'=> 'required|regex:/^\d+(\.\d{1,2})?$/',
            'user_id'=>'required|integer'
        ]);
        return Product::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @description: CETTE FONCTION PERMET D'OBTENIR UN PRODUIT
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Product::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @description: CETTE FONCTION PERMET DE METTRE METTRE À JOUR LE PRODUIT
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product =  Product::find($id);
        $product->update($request->all());
        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Product::destroy($id);
    }

        /**
     * Search for the specified resource from storage by filtering by name.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     * @description CETTE FONCTION POUR RECHERCHER LE NOM DU PRODUIT
     */
    public function search($name)
    {
        return Product::where('name','LIKE','%'.$name.'%')->get();
    }

    public function search2($name)
    {
        return Product::where('name','LIKE','%'.$name.'%')->get();
    }


}
