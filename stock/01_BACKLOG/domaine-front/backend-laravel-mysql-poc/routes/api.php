<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// PUBLIC ROUTES
Route::get('/products',[ProductsController::class,'index']);
Route::get('/products/{id}',[ProductsController::class,'show']);
Route::get('/products/search/{name}',[ProductsController::class,'search']);

Route::post('/users',[UsersController::class,'register']);
Route::post('/users/login',[UsersController::class,'login']);

// PROTECTED ROUTES
Route::group(['middleware'=>['auth:sanctum']], function(){
    Route::post('/products',[ProductsController::class,'store'])->name('products.store');
    Route::put('/products/{id}',[ProductsController::class,'update'])->name('products.update');
    Route::delete('/products/{id}',[ProductsController::class,'destroy'])->name('products.destroy');
    Route::post('/users/logout',[UsersController::class,'logout']);
});