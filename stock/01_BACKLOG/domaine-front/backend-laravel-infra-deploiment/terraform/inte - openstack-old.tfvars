# ===
# Values
# ===

external_network = "external"
internal_network = "reseau_integration"
external_gateway = "routeur_integration"
dns_ip = [
    "192.44.75.10", 
    "192.108.115.2",
]

http_intances = [
  {
    name = "laravel-master-inte-01"
    floating_ip_address = "10.29.244.36"
  },
  {
    name = "laravel-worker-inte-02"
    floating_ip_address = "10.29.244.86"
  },
  {
    name = "laravel-worker-inte-03"
    floating_ip_address = "10.29.244.103"
  }
]

image_name    = "imta-ubuntu-docker"
flavor_name   = "m1.medium"
key_pair      = "geo-admin"
