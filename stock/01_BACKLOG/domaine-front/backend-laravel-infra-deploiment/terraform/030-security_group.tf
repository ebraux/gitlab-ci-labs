# # Acces group, open input port 80 and ssh port
# resource "openstack_compute_secgroup_v2" "http" {
#   name        = "http"
#   description = "Open input http port"
#   rule {
#     from_port   = 80
#     to_port     = 80
#     ip_protocol = "tcp"
#     cidr        = "0.0.0.0/0"
#   }
# }

# # Open mariadb port
# resource "openstack_compute_secgroup_v2" "db" {
#   name        = "db"
#   description = "Open input db port"
#   rule {
#     from_port   = 3306
#     to_port     = 3306
#     ip_protocol = "tcp"
#     cidr        = "0.0.0.0/0"
#   }
# }

# # Open Apache2 port
# resource "openstack_compute_secgroup_v2" "ssh" {
#   name        = "ssh"
#   description = "Open input ssh port"
#   rule {
#     from_port   = 22
#     to_port     = 22
#     ip_protocol = "tcp"
#     cidr        = "0.0.0.0/0"
#   }
# }


# allow ingress ssh and http tcp protocol 
# resource "openstack_compute_secgroup_v2" "sec_group_http" {
#   name        = "ingress_allow"
#   description = "Open input ports"
  
#   dynamic "rule" {
#     for_each    = {for rule in var.ingress_allow_sec_port:  rule.port => rule.protocol}
#     iterator    = rule
#     content {
#     from_port   = rule.key
#     to_port     = rule.key
#     ip_protocol = rule.value
#     cidr        = "0.0.0.0/0"
#     }
#   }
# }
