#!/bin/bash

set -o errexit
set -o pipefail

DOCKER_HOST=gitlab-registry.imt-atlantique.fr

# ----------------------------------
# 1- CREATING FILES
# ----------------------------------

if [ ! -e deploy-files ]; then
    sudo rm -rf deploy-files
fi

mkdir deploy-files
mkdir deploy-files/docker-files
mkdir deploy-files/docker-files/mysql
mkdir deploy-files/docker-files/nginx
mkdir deploy-files/docker-files/php
mkdir deploy-files/docker-files/nginx/conf.d

sudo tee deploy-files/docker-files/mysql/my.cnf << EOF
[mysqld]
general_log = 1
general_log_file = /var/lib/mysql/general.log
EOF


sudo tee deploy-files/docker-files/nginx/conf.d/app.conf << EOF
server {
    listen 80;
    index index.php index.html;
    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /var/www/public;
    location ~ \.php\$ {
        try_files \$uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)\$;
        fastcgi_pass app:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_param PATH_INFO \$fastcgi_path_info;
    }
    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
        gzip_static on;
    }
}
EOF


sudo tee deploy-files/docker-files/php/local.ini << EOF
upload_max_filesize=40M
post_max_size=40M
EOF


sudo tee deploy-files/docker-compose.yml << EOF
version: '3.2'

services:
    # LARAVEL - Service
    app:
        image: gitlab-registry.imt-atlantique.fr/domaine-front/backend-laravel-mysql-poc/laravel-back:1.0.0
        container_name: app
        restart: unless-stopped
        tty: true
        working_dir: /var/www
        environment:
            LARAVEL_VERSION: 8.*
            WITHOUT_DATABASE_CONFIGURATION: Y
            DB_CONNECTION: mysql
            DB_HOST: db
            DB_PORT: 3306
            DB_DATABASE: laravel
            DB_USERNAME: root
            DB_PASSWORD: 123456
        volumes:
            # - ./:/var/www
            -   shared_volume:/var/www
            - ./docker-files/php/local.ini:/usr/local/etc/php/conf.d/local.ini
        networks:
            - app-network

    # NGINX - Service
    webserver:
        image: nginx:alpine
        container_name: webserver
        restart: unless-stopped
        tty: true
        ports:
            - "80:80"
            - "443:443"
        volumes:
            - shared_volume:/var/www
            - ./docker-files/nginx/conf.d/:/etc/nginx/conf.d/
        networks:
            - app-network

    # MariaDB - Service
    db:
        image: mariadb:10.5.6
        container_name: db
        restart: unless-stopped
        tty: true
        ports:
            - "3306:3306"
        environment:
            MYSQL_DATABASE: laravel
            MYSQL_ROOT_PASSWORD: 123456
        volumes:
            - mariadbdata:/var/lib/mysql
            - ./docker-files/mysql/my.cnf:/etc/mysql/my.cnf
        networks:
            - app-network
            
    # PHPMyAdmin- Service
    phpmyadmin:
        depends_on:
            - db
        image: phpmyadmin/phpmyadmin:latest
        container_name: phpmyadmin-test
        restart: always
        environment:
            PMA_HOST: db
            MYSQL_ROOT_PASSWORD: 123456
        ports:
            - "8080:80"
        networks:
            - app-network

# Volumes
volumes:
    mariadbdata:
        driver: local
    shared_volume:

# Networks
networks:
  app-network:
    driver: bridge
EOF


docker login --username=$DOCKER_USER --password=$DOCKER_PASS $DOCKER_HOST


cd deploy-files

docker-compose up