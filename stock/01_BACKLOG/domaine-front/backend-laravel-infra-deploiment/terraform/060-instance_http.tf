#### INSTANCE HTTP ####

# Create instance
#
# resource "openstack_compute_instance_v2" "http" {
#   for_each    = var.http_instance_names
#   name        = each.key
#   image_name  = var.image
#   flavor_name = var.flavor_http
#   key_pair    = openstack_compute_keypair_v2.user_key.name
#   user_data   = file("scripts/first-boot.sh")
#   network {
#     port = openstack_networking_port_v2.http[each.key].id
#   }
# }

# # Create network port
# resource "openstack_networking_port_v2" "http" {
#   for_each       = var.http_instance_names
#   name           = "port-http-${each.key}"
#   network_id     = openstack_networking_network_v2.generic.id
#   admin_state_up = true
#   security_group_ids = [
#     openstack_compute_secgroup_v2.ssh.id,
#     openstack_compute_secgroup_v2.http.id,
#   ]
#   fixed_ip {
#     subnet_id = openstack_networking_subnet_v2.http.id
#   }
# }

# # Create floating ip
# resource "openstack_networking_floatingip_v2" "http" {
#   for_each = var.http_instance_names
#   pool     = var.external_network
# }

# # Attach floating ip to instance
# resource "openstack_compute_floatingip_associate_v2" "http" {
#   for_each    = var.http_instance_names
#   floating_ip = openstack_networking_floatingip_v2.http[each.key].address
#   instance_id = openstack_compute_instance_v2.http[each.key].id
# }


# resource "openstack_compute_instance_v2" "http" {
#   for_each         = var.http_intances
#   name             = each.value.name
#   image_name       = var.image_name
#   flavor_name      = var.flavor_name
#   key_pair         = var.key_pair
#   security_groups  =  ["ingress_allow","default"]
#   network {
#      name          = openstack_networking_network_v2.internal_network.name
#   }
# }

resource "openstack_compute_instance_v2" "http" {
    count = length(var.http_intances)
    name             = var.http_intances[count.index].name
    image_name       = var.image_name
    flavor_name      = var.flavor_name
    key_pair         = var.key_pair
    security_groups  =  ["ingress_allow","default"] 
    network {
     name          = openstack_networking_network_v2.internal_network.name
  }
  depends_on = [openstack_networking_network_v2.internal_network]
}

resource "openstack_compute_floatingip_associate_v2" "http" {
  count = length(var.http_intances)
  floating_ip = var.http_intances[count.index].floating_ip_address
  instance_id = openstack_compute_instance_v2.http[count.index].id
}

#  OLD CODE 


# resource "openstack_compute_instance_v2" "master" {
#   # name              = var.instance_name
#   name              = "master"
#   image_name        = var.image_name
#   flavor_name       = var.flavor_name
#   key_pair          = "geo-admin"
#   security_groups   = ["ingress_allow","default"]
#   network {
#       name          = openstack_networking_network_v2.internal_network.name
#   }
# }

# resource "openstack_compute_instance_v2" "worker01" {
#   name              = "worker01"
#   image_name        = var.image_name
#   flavor_name       = var.flavor_name
#   key_pair          = "geo-admin"
#   security_groups   = ["ingress_allow","default"]
#   network {
#       name          = openstack_networking_network_v2.internal_network.name
#   }
# }

# resource "openstack_compute_instance_v2" "worker02" {
#   name              = "worker02"
#   image_name        = var.image_name
#   flavor_name       = var.flavor_name
#   key_pair          = "geo-admin"
#   security_groups   = ["ingress_allow","default"]
#   # user_data   = file("scripts/first-boot.sh")
#   network {
#       name          = openstack_networking_network_v2.internal_network.name
#   }
# }


# resource "openstack_compute_instance_v2" "development" {
#   name              = "development"
#   image_name        = var.image_name_docker
#   flavor_name       = var.flavor_name
#   key_pair          = "geo-admin"
#   security_groups   = ["ingress_allow","default"]
#   network {
#       name          = openstack_networking_network_v2.internal_network.name
#   }
# }


# # Attach floating ip to instance
# resource "openstack_compute_floatingip_associate_v2" "master" {
#   # floating_ip = openstack_networking_floatingip_v2.fip_http.address
#   floating_ip = "10.29.244.36"
#   instance_id = openstack_compute_instance_v2.master.id
# }

# resource "openstack_compute_floatingip_associate_v2" "worker01" {
#   # floating_ip = openstack_networking_floatingip_v2.fip_http.address
#   floating_ip = "10.29.244.103"
#   instance_id = openstack_compute_instance_v2.worker01.id
# }


# resource "openstack_compute_floatingip_associate_v2" "worker02" {
#   # floating_ip = openstack_networking_floatingip_v2.fip_http.address
#   floating_ip = "10.29.244.86"
#   instance_id = openstack_compute_instance_v2.worker02.id
# }


# resource "openstack_compute_floatingip_associate_v2" "development" {
#   # floating_ip = openstack_networking_floatingip_v2.fip_http.address
#   floating_ip = "10.29.244.90"
#   instance_id = openstack_compute_instance_v2.development.id
# }