# # Params file for variables

# #### GLANCE
# variable "image" {
#   type    = string
#   # default = "Centos 7"
#   default = "imta-ubuntu-basic"
# }

variable "external_network" {
  type    = string
  default = "external"
}
 # #### NEUTRON
variable "internal_network" {
  type    = string
  default = "internal_network"
}

# # UUID of external gateway
variable "external_gateway" {
  type    = string
  default = "external_router"
}

variable "dns_ip" {
  type    = list(string)
  # default = ["192.44.75.10","192.108.115.2"]
}

# #### VM HTTP parameters ####
# variable "flavor_http" {
#   type    = string
#   default = "m1.small"
# }

variable "network_http" {
  type          = map(string)
  default       = {
    subnet_name = "subnet-http_prod"
    cidr        = "192.168.1.0/24"
  }
}

# variable "ingress_allow_sec_port" {
#   type = list(any)
#   default = [{
#         port = 80
#         protocol = "tcp"
#     },
#     {
#         port = 22
#         protocol = "tcp"
#     },
#     {
#         port = 8080
#         protocol = "tcp"
#     }
#   ]
# }


variable "http_intances" {
  type = list(object({
    name = string
    floating_ip_address = string}))
  default        = []
}

# variable "http_instance_names" {
#   type = set(string)
#   default = ["http-instance-1",
#     "http-instance-2",
#   "http-instance-3"]
# }

# #### VM DB parameters ####
# variable "flavor_db" {
#   type    = string
#   default = "t2.medium"
# }

# variable "network_db" {
#   type = map(string)
#   default = {
#     subnet_name = "subnet-db"
#     cidr        = "192.168.2.0/24"
#   }
# }

# variable "db_instance_names" {
#   type = set(string)
#   default = ["db-instance-1",
#     "db-instance-2",
#   "db-instance-3"]
# }

# variable "db_instance_names" {
#   type = set(string)
#   default = ["db-instance-1",
#     "db-instance-2",
#   "db-instance-3"]
# }

# variable "instance_name" {
#   type = string
#   default = "instance"
# }

// variable "image_name" {
//   type = string
//   default = "imta-ubuntu-docker"
// }

variable "image_name_docker" {
  type    = string
  default = "imta-ubuntu-docker"
}

variable "image_name" {
  type    = string
  default = "imta-ubuntu-docker"
}

variable "flavor_name" {
  type    = string
  default = "m1.medium"
}

variable "key_pair" {
  type    = string
  default = ""
}
