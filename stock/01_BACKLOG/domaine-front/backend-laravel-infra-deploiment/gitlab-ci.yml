---
variables:
  OS_AUTH_URL: ${CI_OS_AUTH_URL}
  OS_PROJECT_ID: ${CI_OS_PROJECT_ID}
  OS_PROJECT_NAME: ${CI_OS_PROJECT_NAME}
  OS_USER_DOMAIN_NAME: ${CI_OS_USER_DOMAIN_NAME}
  OS_USERNAME: ${CI_OS_USERNAME}
  OS_PASSWORD: ${CI_OS_PASSWORD}
  OS_REGION_NAME: ${CI_OS_REGION_NAME}

  # ANSIBLE_TARGET: swarm_cluster_inte
  TF_ROOT: ${CI_PROJECT_DIR}/terraform # TERRAFORM LOCATION
  ENV: "integration"
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${ENV} # TERRAFORM ADDRESS PATH

stages:
  - check-syntax
  - prepare
  - validate
  - build
  - deploy
  - imta-proxy
  - upgrade-system
  - installation-prerequisites
  - deploying-cluster-swarm
  # - configure-infrastructure:validate
  # - configure-infrastructure:plan
  # - configure-infrastructure:deploy
  # - configuring-proxy-imt
  # - updating-system
  # - installation-prerequisites
  # - deploying-cluster-swarm


# ----------------------------------------------------------------------------------
# 0 - CHECK LINT
# ----------------------------------------------------------------------------------

# yamllint:
#   stage: check-syntax
#   image: sdesbure/yamllint
#   script:
#     - yamllint -d relaxed .

ansiblelint:
  stage: check-syntax
  image: willhallonline/ansible:alpine
  tags:
    - lp-disi-docker
  script:
    - cd ansible
    - ansible-galaxy install -r requirements.yml
    - ansible-lint -f codeclimate .


tf-init:
  stage: prepare
  # needs: ['yamllint']
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  before_script:
    - cd ${TF_ROOT}
  script:
    - gitlab-terraform init

tf-validate:
  stage: validate
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  needs: ['tf-init']
  before_script:
    - cd ${TF_ROOT}
  script:
    - gitlab-terraform validate

tf-inte-plan:
  stage: build
  rules:
    - if: '$ENV == "integration"'
      when: always
    - when: never
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  needs: ['tf-validate']
  before_script:
    - cd ${TF_ROOT}
  script:
    - gitlab-terraform plan -var-file="inte.tfvars"
    - gitlab-terraform plan-json -var-file="inte.tfvars"
  artifacts:
    paths:
      - ${TF_ROOT}/plan.cache
    name: integration
    reports:
        terraform: ${TF_ROOT}/integration/plan.json

tf-recette-plan:
  stage: build
  rules:
  - if: '$ENV == "recette"'
    when: always
  - when: never
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  needs: ['tf-validate']
  before_script:
    - cd ${TF_ROOT}
  script:
    - gitlab-terraform plan -var-file="rec.tfvars"
    - gitlab-terraform plan-json -var-file="rec.tfvars"
  artifacts:
    paths:
      - ${TF_ROOT}/plan.cache
    name: recette
    reports:
        terraform: ${TF_ROOT}/recette/plan.json


tf-inte-apply:
  stage: deploy
  rules:
  - if: '$ENV == "integration"'
    when: always
  - when: never
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  needs: ['tf-inte-plan']
  before_script:
    - cd ${TF_ROOT}
  script:
    - gitlab-terraform apply
  # environment:
  #   name: integration
  #   url: "${CI_ENVIRONMENT_INTE_URL}"
  dependencies:
    - tf-inte-plan

tf-rec-apply:
  stage: deploy
  rules:
  - if: '$ENV == "recette"'
    when: always
  - when: never
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  needs: ['tf-recette-plan']
  before_script:
    - cd ${TF_ROOT}
  script:
    - gitlab-terraform apply
  # environment:
  #   name: recette
  #   url: "${CI_ENVIRONMENT_REC_URL}"
  dependencies:
    - tf-recette-plan

# ---------------------------------------------------------------------------------

.ansible-steps:
  image: willhallonline/ansible:alpine
  before_script:
    - chmod 700 ansible
    - mkdir ansible/log
    # - cp ansible/inventory/geo-admin.pem /ansible
    # - chmod 400 /ansible/geo-admin.pem
    - echo ${SSH_PRIVATE_KEY} > temp.txt
    - fold -w 64 temp.txt > /ansible/geo-admin.pem
    - sed -i '1s/^/-----BEGIN RSA PRIVATE KEY-----\n/' /ansible/geo-admin.pem
    - echo "-----END RSA PRIVATE KEY-----" >> /ansible/geo-admin.pem
    - chmod 400 /ansible/geo-admin.pem
    - cd ansible
    - ansible-galaxy install -r requirements.yml


ans-inte-imta-proxy:
  extends: .ansible-steps
  stage: imta-proxy
  tags:
    - lp-disi-docker
  needs: [ 'ansiblelint','tf-inte-apply']
  rules:
    - if: '$ENV == "integration"'
      when: always
    - when: never
  script:
    - ansible-playbook playbooks/imta-proxy.yml --limit swarm_cluster_inte

ans-rec-imta-proxy:
  extends: .ansible-steps
  stage: imta-proxy
  tags:
    - lp-disi-docker
  needs: [ 'ansiblelint','tf-rec-apply']
  rules:
    - if: '$ENV == "recette"'
      when: always
    - when: never
  script:
    - ansible-playbook playbooks/imta-proxy.yml --limit swarm_manager_rec

ans-inte-upgrade-system:
  extends: .ansible-steps
  stage: upgrade-system
  tags:
    - lp-disi-docker
  needs: ['ans-inte-imta-proxy']
  rules:
    - if: '$ENV == "integration"'
      when: always
    - when: never
  script:
    - ansible-playbook playbooks/upgrade-system.yml --limit swarm_cluster_inte

ans-rec-upgrade-system:
  extends: .ansible-steps
  stage: upgrade-system
  tags:
    - lp-disi-docker
  needs: ['ans-rec-imta-proxy']
  rules:
    - if: '$ENV == "recette"'
      when: always
    - when: never
  script:
    - ansible-playbook playbooks/upgrade-system.yml --limit swarm_manager_rec

ans-inte-installation-prerequisites:
  extends: .ansible-steps
  stage: installation-prerequisites
  tags:
    - lp-disi-docker
  rules:
    - if: '$ENV == "integration"'
      when: always
    - when: never
  needs: ['ans-inte-upgrade-system']
  script:
    - ansible-playbook playbooks/install-prerequis.yml --limit swarm_cluster_inte

ans-rec-installation-prerequisites:
  extends: .ansible-steps
  stage: installation-prerequisites
  tags:
    - lp-disi-docker
  rules:
    - if: '$ENV == "recette"'
      when: always
    - when: never
  needs: ['ans-rec-upgrade-system']
  script:
    - ansible-playbook playbooks/install-prerequis.yml --limit swarm_manager_rec

ans-int-deploying-cluster-swarm:
  extends: .ansible-steps
  stage: deploying-cluster-swarm
  tags:
    - lp-disi-docker
  rules:
    - if: '$ENV == "integration"'
      when: always
    - when: never
  needs: ['ans-inte-installation-prerequisites']
  script:
    - ansible-playbook playbooks/swarm-deploy-cluster.yml --limit swarm_cluster_inte

ans-rec-deploying-cluster-swarm:
  extends: .ansible-steps
  stage: deploying-cluster-swarm
  tags:
    - lp-disi-docker
  rules:
    - if: '$ENV == "recette"'
      when: always
    - when: never
  needs: ['ans-rec-installation-prerequisites']
  script:
    - ansible-playbook playbooks/swarm-deploy-cluster.yml --limit swarm_cluster_rec


# ----------------------------------------------------------------------------------
# 1 - CONFIGURE INFRASTRUCTURE
# ----------------------------------------------------------------------------------
.configure-infrastructure:init:
  stage: configure-infrastructure:prepare
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  before_script:
    - cd ${TF_ROOT}
  script:
    - gitlab-terraform init

.configure-infrastructure:validate:
  stage: configure-infrastructure:validate
  needs: ['configure-infrastructure:init']
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  before_script:
    - cd ${TF_ROOT}
  script:
    - gitlab-terraform init
    - gitlab-terraform validate

.tf-fmt:
  stage: configure-infrastructure:validate
  needs: ['configure-infrastructure:init']
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  script:
    - gitlab-terraform fmt -check -recursive ${TF_ROOT}/

.configure-infrastructure:plan:
  stage: configure-infrastructure:plan
  needs: ['configure-infrastructure:validate']
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  before_script:
    - cd ${TF_ROOT}
  script:
    - gitlab-terraform plan -var-file="terraform.tfvars"
    - gitlab-terraform plan-json -var-file="terraform.tfvars"
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json

# Separate apply job for manual launching Terraform as it can be destructive action.
.configure-infrastructure:apply:
  stage: configure-infrastructure:deploy
  needs: ['configure-infrastructure:plan']
  tags:
    - lp-disi-docker
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  environment:
    name: developement
  before_script:
    - cd ${TF_ROOT}
  script:
    - gitlab-terraform apply
  dependencies:
    - configure-infrastructure:plan
  # when: manual
  only:
    - master

# ----------------------------------------------------------------------------------
# 2- DEPLOYING DOCKER SWARM
# ----------------------------------------------------------------------------------
.preperation-nodes-conf-proxy:
  stage: configuring-proxy-imt
  tags:
    - lp-disi-docker
  image: willhallonline/ansible:alpine
  # needs: ['yamllint', 'ansiblelint','configure-infrastructure:apply']
  # before_script:
  #   - chmod 775 setup-ansible.sh
  #   - ./setup-ansible.sh
  script:
    - chmod 700 ansible
    - mkdir ansible/log
    - cp ansible/inventory/geo-admin.pem /ansible
    - chmod 400 /ansible/geo-admin.pem
    - cd ansible
    - ansible-galaxy install -r requirements.yml
    - ansible-playbook playbooks/imta-proxy.yml --limit $ANSIBLE_TARGET

.preperation-nodes-updating-system:
  stage: updating-system
  tags:
    - lp-disi-docker
  image: willhallonline/ansible:alpine
  # needs: ['preperation-nodes-conf-proxy']
  # before_script:
  #   - chmod 775 setup-ansible.sh
  #   - ./setup-ansible.sh
  script:
    - chmod 700 ansible
    - mkdir ansible/log
    - cp ansible/inventory/geo-admin.pem /ansible
    - chmod 400 /ansible/geo-admin.pem
    - cd ansible
    - ansible-galaxy install -r requirements.yml
    - ansible-playbook playbooks/upgrade-system.yml --limit $ANSIBLE_TARGET

.preperation-nodes-installation-prerequisites:
  stage: installation-prerequisites
  tags:
    - lp-disi-docker
  # image : ansible/ansible-runner
  image: willhallonline/ansible:alpine
  # needs: ['preperation-nodes-updating-system']
  # before_script:
  #   - chmod 775 setup-ansible.sh
  #   - ./setup-ansible.sh
  script:
    - chmod 700 ansible
    - mkdir ansible/log
    - cp ansible/inventory/geo-admin.pem /ansible
    - chmod 400 /ansible/geo-admin.pem
    - cd ansible
    - ansible-galaxy install -r requirements.yml
    - ansible-playbook playbooks/install-prerequis.yml --limit $ANSIBLE_TARGET

.preperation-nodes-deploying-cluster-swarm:
  stage: deploying-cluster-swarm
  tags:
    - lp-disi-docker
  # image : ansible/ansible-runner
  image: willhallonline/ansible:alpine
  # needs: ['preperation-nodes-updating-system','preperation-nodes-installation-prerequisites']
  # before_script:
  #   - chmod 775 setup-ansible.sh
  #   - ./setup-ansible.sh
  script:
    - chmod 700 ansible
    - mkdir ansible/log
    - cp ansible/inventory/geo-admin.pem /ansible
    - chmod 400 /ansible/geo-admin.pem
    - cd ansible
    - ansible-galaxy install -r requirements.yml
    - ansible-playbook playbooks/swarm-deploy-cluster.yml --limit $ANSIBLE_TARGET
