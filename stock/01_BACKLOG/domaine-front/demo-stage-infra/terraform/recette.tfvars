# ===
# Values
# ===

external_network = "external"
internal_network = "reseau_recette"
external_gateway = "routeur_recette"
dns_ip = [
    "192.44.75.10", 
    "192.108.115.2",
]


http_intances = [
  {
    name = "master-rec"
    floating_ip_address = "10.29.244.91"
  },
  {
    name = "worker-rec-01"
    floating_ip_address = "10.29.244.26"
  }
]

image_name    = "imta-ubuntu-docker"
flavor_name   = "m1.medium"
key_pair      = "demo-stage"
