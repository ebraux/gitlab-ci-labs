terraform {
    required_version = ">= 0.14.0"
    required_providers {
        openstack = {
            source  = "terraform-provider-openstack/openstack"
            version = "~> 1.35.0"
        }
    }
}

# provider openstack {
#   auth_url         = "https://openstack.imt-atlantique.fr:5000/v3"
#   tenant_id        = "91c861ffa0aa41d2988e22c24e16cd34"
#   tenant_name      = "laravel-poc"
#   user_domain_name = "Default"
#   user_name        = "g20aboua"
#   password         = "changeit"
#   region           = "RegionOne"

# }
provider openstack {}
terraform {
  backend "http" {
    
  }
}
