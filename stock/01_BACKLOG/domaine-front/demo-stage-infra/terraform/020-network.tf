# # #### NETWORK CONFIGURATION ####


## Get external network
data "openstack_networking_network_v2" "ext_network" {
  name = var.external_network
}

# # Router creation
resource "openstack_networking_router_v2" "external_router" {
  name                = var.external_gateway
  external_network_id = data.openstack_networking_network_v2.ext_network.id

  depends_on = [data.openstack_networking_network_v2.ext_network]
}

# Network creation
resource "openstack_networking_network_v2" "internal_network" {
  name           = var.internal_network
  admin_state_up = true
}

# # #### HTTP SUBNET ####

# # Subnet http configuration
resource "openstack_networking_subnet_v2" "application_http_prod" {
  name            = var.network_http["subnet_name"]
  network_id      = openstack_networking_network_v2.internal_network.id
  cidr            = var.network_http["cidr"]
  dns_nameservers = var.dns_ip
  
  depends_on = [openstack_networking_network_v2.internal_network]
}

# Router interface configuration
resource "openstack_networking_router_interface_v2" "application_http_prod" {
  router_id = openstack_networking_router_v2.external_router.id
  subnet_id = openstack_networking_subnet_v2.application_http_prod.id

  depends_on = [openstack_networking_router_v2.external_router,
                openstack_networking_subnet_v2.application_http_prod]
}

# #### DB SUBNET ####

# # Subnet db configuration
# resource "openstack_networking_subnet_v2" "db" {
#   name            = var.network_db["subnet_name"]
#   network_id      = openstack_networking_network_v2.generic.id
#   cidr            = var.network_db["cidr"]
#   dns_nameservers = var.dns_ip
# }

# # Router interface configuration
# resource "openstack_networking_router_interface_v2" "db" {
#   router_id = openstack_networking_router_v2.generic.id
#   subnet_id = openstack_networking_subnet_v2.db.id
# }



# Create floating ip
# resource "openstack_networking_floatingip_v2" "fip_http" {
#   pool = data.openstack_networking_network_v2.ext_network.name
# }
