# ===
# Values
# ===

external_network = "external"
internal_network = "reseau_integration"
external_gateway = "routeur_integration"
dns_ip = [
    "192.44.75.10", 
    "192.108.115.2",
]

http_intances = [
  {
    name = "demo-stage-inte"
    floating_ip_address = "10.29.244.121"
  },
  {
    name = "worker-inte-01"
    floating_ip_address = "10.29.244.103"
  },
  {
    name = "worker-inte-02"
    floating_ip_address = "10.29.244.86"
  }
]

image_name    = "imta-ubuntu-docker"
flavor_name   = "m1.medium"
key_pair      = "demo-stage"
