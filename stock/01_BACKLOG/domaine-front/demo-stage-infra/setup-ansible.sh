#!/bin/bash

set -o errexit
set -o pipefail

chmod 700 ansible
mkdir ansible/log
cp ansible/inventory/geo-admin.pem /ansible
chmod 400 /ansible/geo-admin.pem
cd ansible
ansible-galaxy install -r requirements.yml

echo ""
exec "$@"
