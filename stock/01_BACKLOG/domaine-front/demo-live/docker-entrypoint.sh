#!/bin/bash

set -o errexit
set -o pipefail


# FUNCTIONS
###################################################


########################
# Replace a regex in a file
# Arguments:
#   $1 - filename
#   $2 - match regex
#   $3 - substitute regex
#   $4 - regex modifier
# Returns: none
#########################
replace_in_file() {
    local filename="${1:?filename is required}"
    local match_regex="${2:?match regex is required}"
    local substitute_regex="${3:?substitute regex is required}"
    local regex_modifier="${4:-}"
    local result

    result="$(sed "${regex_modifier}s@${match_regex}@${substitute_regex}@g" "$filename")"
    echo "$result" >"$filename"

}

########################
# Setup the database configuration
# Arguments: none
# Returns: none
#########################
setup_db() {
    echo "------------------------------------------------"
    echo "Configuring the database"
    echo "------------------------------------------------"
    php artisan migrate --force
}


# CONFIGURATION PROXY DE L'IMT
# --------------------------------------------------------
if [ "${IMT_PROXY}" == "true" ];then
    export http_proxy=http://proxy.enst-bretagne.fr:8080
    export https_proxy=http://proxy.enst-bretagne.fr:8080
    export ftp_proxy=http://proxy.enst-bretagne.fr:8080
    export no_proxy="127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr"
    env | grep proxy

else 
        echo "------------------------------------------------"
        echo "Skipping configuring IMT proxy"
        echo "------------------------------------------------"
fi



# ETAPE 0 : INSTALLATION LES FICHIERS LARAVEL
# --------------------------------------------------------
installingLaravel=0
dirTempName=temp_laravel
if [ ! -e artisan ]; then
    
    # 0.1: SUPRESSION DOSSIER GIT
    if [ -e .git ]; then
        rm -rf .git 
    fi

    # 0.2: SUPRESSION DOSSIER TEMPORAIRE S'IL EXISTE
    if [ -e "${dirTempName}" ];then
        rm -rf "${dirTempName}"
    fi
    
    # 0.3: CREATION PROJET LARAVEL DANS UN DOSSIER TEMPORAIRE
    composer create-project --stability stable  laravel/laravel "${dirTempName}" "${LARAVEL_VERSION}"
    cp -r /var/www/"${dirTempName}"/.  /var/www/
    rm -rf "${dirTempName}"
    chmod -R 777 /var/www
    installingLaravel=1
fi


# ETAPE 1 : CREATION DU FICHER .ENV ET INJECTION LES VARIABLES DE LA BASE DE DONNEES
# --------------------------------------------------------
if [ -e .env.example ]; then
    # CREATION DU FICHER .ENV
    if [ ! -e .env ];then
        cp .env.example .env
    fi

    echo "------------------------------------------------"
    echo "Configuring the env file"
    echo "------------------------------------------------"
    # CHECK IF THE VARIABLES ARE POPULATED
    # ------------------------------------
    flagError=0
    errorListMsg=""
    if [ -z "${DB_CONNECTION}" ];then
        errorListMsg+=" + la variable DB_CONNECTION est nulle \n"
        flagError=1  
    else
        replace_in_file ".env" "DB_CONNECTION=mysql" "DB_CONNECTION=${DB_CONNECTION}"
    fi

    if [ -z "${DB_HOST}" ];then
        errorListMsg+=" + la variable DB_HOST est nulle \n"
        flagError=1  
    else
        replace_in_file ".env" "DB_HOST=127.0.0.1" "DB_HOST=${DB_HOST}"
    fi

    if [ -z "${DB_PORT}" ];then
        errorListMsg+=" + la variable DB_PORT est nulle \n"
        flagError=1  
    else
        replace_in_file ".env" "DB_PORT=3306" "DB_PORT=${DB_PORT}"
    fi

    if [ -z "${DB_DATABASE}" ];then
        errorListMsg+=" + la variable DB_DATABASE est nulle \n"
        flagError=1  
    else
        replace_in_file ".env" "DB_DATABASE=laravel" "DB_DATABASE=${DB_DATABASE}"
    fi

    if [ -z "${DB_USERNAME}" ];then
        errorListMsg+=" + la variable DB_USERNAME est nulle \n"
        flagError=1  
    else
        replace_in_file ".env" "DB_USERNAME=root" "DB_USERNAME=${DB_USERNAME}"
    fi

    if [ -z "${DB_PASSWORD}" ];then
        errorListMsg+=" + la variable DB_PASSWORD est nulle \n"
        flagError=1  
    else
        replace_in_file ".env" "DB_PASSWORD=" "DB_PASSWORD=${DB_PASSWORD}"
    fi    


    if [ $flagError == 1 ];then
        echo -e "La liste des erreurs : \n" $errorListMsg
        exit 1
    fi

    echo ".env file is configured"
else
    echo ".env.example does not exist"
    # THROW ERROR AU CAS OU LE FICHER .ENV.EXAMPLE N'EXISTE PAS
    exit 1 
fi


if [ -e .env ]; then
    
    # ETAPE 2: INTALLATION DU LARAVEL 
    # --------------------------------------------------------
    if [ $installingLaravel == 0 ];then
        echo "---------------------------------------------------"
        echo "Installing/Updating Laravel dependencies (composer)"
        echo "---------------------------------------------------"

        if [[ ! -d /vendor ]]; then
            composer install
            echo "Dependencies installed"
        else
            composer update
            echo "Dependencies updated"
        fi
    fi

    # ETAPE 3: GENERATION DE LARAVEL CLEF
    # --------------------------------------------------------
    if [ $installingLaravel == 0 ];then
        echo "------------------------------------------------"
        echo "Regenerating APP_KEY"
        echo "------------------------------------------------"
        php artisan key:generate --ansi
    fi 
  
    # ETAPE 4: ADDING DATABASE UPDATES : MIGRATIONS
    # --------------------------------------------------------
    if [ "${WITHOUT_DATABASE_CONFIGURATION}" == "true" ];then
        setup_db
    else
        echo "------------------------------------------------"
        echo "Skipping configuring database"
        echo "------------------------------------------------"
    fi

    # ETAPE 5: CREATION FICHER .ENV.TESTING 
    # --------------------------------------------------------
    echo "------------------------------------------------"
    echo "Configuring the testing env file"
    echo "------------------------------------------------"
    cp .env .env.testing
    echo ".env file is configured"
else
    echo ".env does not exist"
    # THROW ERROR AU CAS OU LE FICHER .ENV N'EXISTE PAS
    exit 1 
fi


echo "------------------------------------------------"
echo "Finalizing configuration and setup"
echo "------------------------------------------------"

echo ""
exec "$@"
