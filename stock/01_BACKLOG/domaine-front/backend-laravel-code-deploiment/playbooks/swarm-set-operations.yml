---
- name: Code deployment
  hosts: swarm_node
  gather_facts: no
  become: true

  pre_tasks:
    - name: Install prereq
      apt:
        name: "{{ packages }}"
      become: true
      vars:
        packages:
        - acl
        - python3-docker
        - python3-jsondiff
        - python3-yaml
          
  tasks:
    - name : Getting env variables from Linux to Ansible 
      include_vars:
        file: ../variables.yml
        name : parametres

    - debug:
        var: parametres  

    - name: Config private registry and force re-authorization
      docker_login:
        registry_url: "{{parametres.deploiment_registry_url }}"
        username: "{{ parametres.deploiment_registry_user }}"
        password: "{{ parametres.deploiment_registry_password }}"
        reauthorize: yes
      become: true

    - name: (LOG) Get stack info before removing stack
      community.docker.docker_stack_info:
      register: stack_info_before_rm_stack
      when: inventory_hostname in groups['swarm_manager']

    - name: (LOG) Shows stack info results before removing stack
      debug:
        var:  stack_info_before_rm_stack.results
      when: inventory_hostname in groups['swarm_manager']

    - name: Remove "{{parametres.stack_name}}" stack
      docker_stack:
        name: "{{parametres.stack_name}}"
        state: absent
        absent_retries: 50
        absent_retries_interval: 15
      when: inventory_hostname in groups['swarm_manager']

    - name: (LOG) Get stack info after removing stack
      community.docker.docker_stack_info:
      register: stack_info_after_rm_stack
      when: inventory_hostname in groups['swarm_manager']

    - name: (LOG) Shows stack info results after removing stack
      debug:
        var:  stack_info_after_rm_stack.results
      when: inventory_hostname in groups['swarm_manager']

    - name: (LOG) Get infos on {{parametres.stack_name}}_shared_volume volume before delete
      docker_volume_info:
        name: "{{parametres.stack_name}}_shared_volume"
      register: volume_before_rm_result

    - name: (LOG) Does {{parametres.stack_name}}_shared_volume volume exist?
      debug:
        msg: "The {{parametres.stack_name}}_shared_volume volume {{ 'exists' if volume_before_rm_result.exists else 'does not exist' }}"

    - name: Remove a volume
      docker_volume:
        name: "{{parametres.stack_name}}_shared_volume"
        state: absent
        timeout: 300
      when: inventory_hostname in groups['swarm_worker']

    - name: (LOG) Get infos on {{parametres.stack_name}}_shared_volume volume after delete
      docker_volume_info:
        name: "{{parametres.stack_name}}_shared_volume"
      register: volume_after_rm_result

    - name: (LOG) Does {{parametres.stack_name}}_shared_volume volume exist after delete?
      debug:
        msg: "The {{parametres.stack_name}}_shared_volume volume {{ 'exists' if volume_before_rm_result.exists else 'does not exist' }}"

    - name: Copy Files in deploy-files folder
      copy:
        src: ../deploy-files/
        dest: /home/ubuntu/deploy-files

    - name: Deploy stack from a compose file
      docker_stack:
        state: present
        name: "{{parametres.stack_name}}"
        compose:
          - /home/ubuntu/deploy-files/docker-compose.yml
        with_registry_auth: true
      environment:
          IMAGE_NAME: "{{parametres.image_name}}"
          PHP_FPM_PORTS: "{{parametres.php_fpm_ports}}"
          NGINX_APP_PORTS: "{{parametres.nginx_app_ports}}"
          LARAVEL_VERSION: "{{parametres.laravel_version}}"
          WITHOUT_DATABASE_CONFIGURATION: "{{parametres.without_database_configuration}}"
          IMT_PROXY: "{{parametres.imt_proxy}}"
          DB_CONNECTION: "{{parametres.db_connection}}"
          DB_HOST: "{{parametres.db_host}}"
          DB_PORT: "{{parametres.db_port}}"
          DB_DATABASE: "{{parametres.db_database}}"
          DB_USERNAME: "{{parametres.db_username}}"
          DB_PASSWORD: "{{parametres.db_password}}"
      when: inventory_hostname in groups['swarm_manager']
    
    - name: Shows {{parametres.stack_name }} stack task info
      community.docker.docker_stack_task_info:
        name: "{{parametres.stack_name }}"
      register: stack_task_info
      when: inventory_hostname in groups['swarm_manager']

    - name: (LOG)Shows {{parametres.stack_name }} stack tasks info results
      debug:
        var: stack_task_info.results
      when: inventory_hostname in groups['swarm_manager']

    - name: (LOG) Get infos on {{parametres.stack_name}}_shared_volume volume after stack created
      docker_volume_info:
        name: "{{parametres.stack_name}}_shared_volume"
      register: volume_after_rm_result

    - name: (LOG) Does {{parametres.stack_name}}_shared_volume volume exist after stack created?
      debug:
        msg: "The {{parametres.stack_name}}_shared_volume volume {{ 'exists' if volume_after_rm_result.exists else 'does not exist' }}"

    - name: Log out Gitlab Registry
      docker_login:
        registry_url: "{{parametres.deploiment_registry_url }}"
        username: "{{ parametres.deploiment_registry_user }}"
        password: "{{ parametres.deploiment_registry_password }}"
        reauthorize: yes
        state: absent
