# Documentation POC Laravel

La documentation générée est disponible sur le site [https://domaine-front.gitlab-pages.imt-atlantique.fr/backend-laravel-poc-doc](https://domaine-front.gitlab-pages.imt-atlantique.fr/backend-laravel-poc-doc/)

## Tests en local du site

Build de l'image
```bash
docker build -t mkdocs_laravel-poc .
```

Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_laravel-poc mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_laravel-poc mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_laravel-poc rm -rf /work/site
docker image rm mkdocs_laravel-poc
```

