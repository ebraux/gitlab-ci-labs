# Lancer un nouveau projet laravel en local

!!! example ""
    <div style="line-height:2em;vertical-align:middle;color:#651fff;">
        <span style="font-size:1.5em">:material-test-tube:</span>
        <span style="font-size:1.1em">Version expérimentale</span>
    </div>

## Exécuter le projet

Vous aurez besoin des outils suivants :
>[Docker] [Docker Desktop](https://www.docker.com/products/docker-desktop)
>
>[Windows] [Git pour windows](https://gitforwindows.org/)
>
<!-- >[Text Editor] [Notepad++](https://notepad-plus-plus.org/downloads/) -->

## Créer un nouveau projet sur gitlab

1. Aller sur [gitlab de l'IMT  :fontawesome-solid-external-link-alt:](https://gitlab.imt-atlantique.fr).
2. Connectez-vous avec vos identifiants de l'IMT.
3. Cliquer sur le bouton **new project** pour créer un nouveau projet.
4. Choisir **Create blank project** pour créer un projet gitlab vide.
5. Remplir le formulaire:
    - Remplissez le champ **nom du projet**. 
        <p style="text-align:justify">Vous ne pouvez pas utiliser de caractères spéciaux, mais vous pouvez utiliser des espaces, des traits d'union, des traits de soulignement ou même des emoji. Lors de **l'ajout du nom**, le slug du projet se remplit automatiquement. Le slug est ce que l'instance GitLab utilise comme chemin d'URL vers le projet. Si vous voulez un slug différent, saisissez d'abord le nom du projet, puis modifiez le slug après. </p>
        <p style="text-align:justify">Le chemin d'accès à votre projet dans le champ Project slug. Il s'agit du chemin d'URL de votre projet que l'instance GitLab utilise. Si le nom du projet est vide, il se remplit automatiquement lorsque vous remplissez le slug du projet.</p>


    - Remplir le champ de description du projet (c'est facultatif).
    - Choisir le niveau de visibilité.
  
6. Cliquer sur le bouton **create project** pour créer un nouveau projet.

## Créer un projet Laravel à partir de POC

### 1- Cloner le projet Laravel à partir du projet de base

1. Aller au [le projet de base sur gitlab  :fontawesome-solid-external-link-alt:](https://gitlab.imt-atlantique.fr/domaine-front/backend-laravel-mysql-base-projet)
2. Cliquer sur le bouton **Clone v** et copier le lien de clone with HTTPS
3. Ouvrir Phpstorm et cliquer sur le button **Get from VCS**
4. Coller le lien qui est déjà copié dans le champ URL
5. Renomer le dossier du projet avec un nom unique
    En utilisant le champs directory et en cliquant sur le button de dossier on crée un nouveau dossier 

    !!! attention "Attention: **Nommage du dossier du projet**"
        Le nom du dossier du projet est obligatoirement **unique** car ce nom sera utiliser dans l'appellation des containaires ultérieurement. 

6. Cliquer sur le boutton **clone**

7. <p style="text-align:justify">Pour les utilisateurs de WINDOWS</p>

    <p style="text-align:justify">Pour changer la configuration des conversions pour lancer ce projet et au cas ou vous travaillez sur le système d'exploitation Windows, il faut modifier pour que les conversions de fin de ligne soient en format de **LF** du ficher <strong>docker-entrypoint.sh</strong>. En utilisant phpStorm, il faut ouvrir le fichier docker-entrypoint.sh, ensuite, il faut convertir le fichier avec conversion EOL en Linux (LF) en cliquant sur CRLF et choisir LF.

    !!! info 
        <p style="text-align:justify">Pour changer la configuration des conversions de fin de ligne de git, naviguer vers [Configuration du formatage et d'espace de git  :fontawesome-solid-external-link-alt:](/git#configuration-du-formatage-et-despace-de-git).</p>

    ![tous les pipelines](images/intelij_lf.png)

### 2- Configurer les conteneurs

<p style="text-align:justify">Après l'installation du projet on vise vers la configuration des conteneurs et aux variables utilisées.
Le projet contient un ficher de configuartion <strong><code>docker-compose.yml</code></strong>, et toute la configuration s'éffectura dans ce fichier.</p>

#### 2.1 - Changer les volumes utlisés par les conteneurs de chaque projet

<p style="text-align:justify">Après chaque création d'un nouveau projet et pour éviter l'écrasement des données dans la base de données, il est obligatoire de changer les volumes utilisés par les conteneurs. Seulement il faut modifier le nom de chaque volume utilisé par le nom du projet.</p>

<p>Voici toutes les pièces qui sont marquées en jaune doivent être sont changées.</p>

!!! abstract "Code"
    <strong>docker-compose.yml</strong>
    <pre style="background:hsla(0, 0%, 96%, 1);">
        version: '3.8'
        <br>
        services:
            # LARAVEL - Service
            app:
            <i>......</i><br>
            # NGINX - Service
            webserver:
            <i>......</i><br>
            # MariaDB - Service
                db:
                    image: mariadb:10.5.6"
                    environment:
                        MYSQL_DATABASE: laravel
                        MYSQL_ROOT_PASSWORD : 123456
                        MYSQL_USER: test
                        MYSQL_PASSWORD : test
                    volumes:
                        - <mark>mariadbdata</mark>:/var/lib/mysql
                        - ./docker-files/mysql/my.cnf:/etc/mysql/my.cnf
            <i>......</i><br>
            # PHPMyAdmin- Service
            phpmyadmin:
            <i>......</i><br>
        # Volumes
        volumes:
        <mark>mariadbdata</mark>:
            driver: local
    </pre>

#### 2.2 - Changer les variables globales dans le docker compose (facultatif)
<p style="text-align:justify">L'application utilise des variables globales dans le but de configurer la connexion entre l'application laravel et la base de données.</p>
<p style="text-align:justify">Voici le tableau des variables :</p>

| Variable            | Conteneur   | Description   | Valeur par défaut
| :------------------- | :-----------: |-------------- | -----------------:
| `WITHOUT_DATABASE_CONFIGURATION`| app         | <p style="text-align:justify;margin:0">Cette variable permet de sauter l'étape d'éffectuer les migrations sur la base de données</p> | true 
| `IMT_PROXY`           | app         | <p style="text-align:justify;margin:0">Cette variable permet d'appliquer le proxy de l'IMT Atlanqtique.</p> | true |
| `DB_HOST`             | app         | <p style="text-align:justify;margin:0">Cette variable définit ip ou nom du conteneur de la base de données.</p>| db |
| `DB_PORT`             | app         | <p style="text-align:justify;margin:0">Cette variable définit le port utilisé de la base de données.</p> | 3306 |
| `DB_USERNAME`         | app         | <p style="text-align:justify;margin:0">Cette variable définit le compte qui a accès à la base de données.</p>| root |
| `DB_DATABASE`         | app         | <p style="text-align:justify;margin:0">Cette variable définit le nom de la base de données.</p>| laravel |
| `DB_PASSWORD`         | app         | <p style="text-align:justify;margin:0">Cette variable définit mot de passe du compte `DB_USERNAME`.</p>| 123456 |
| `MYSQL_DATABASE`    | db          | <p style="text-align:justify;margin:0">Cette variable permet de spécifier le nom d'une base de données à créer au démarrage de l'image. Si un utilisateur/mot de passe a été fourni, cet utilisateur se verra accorder un accès superutilisateur (correspondant à GRANT ALL) à cette base de données.</p> | laravel |
| `MYSQL_ROOT_PASSWORD`| db          | <p style="text-align:justify;margin:0">Cette variable spécifie le mot de passe qui sera défini pour le compte superutilisateur racine Mariadb.</p> | 123456 |
| `MYSQL_USER` |db| <p style="text-align:justify;margin:0">Cette variable définit le compte qui a accès à la base de données. Les variables utilisateur et mot de passe sont nécessaires pour qu'un utilisateur soit créé. </p> | test|
| `MYSQL_PASSWORD` |db| <p style="text-align:justify;margin:0">Cette variable définit mot de passe du compte `MARIADB_USER`.</p> | test|
| `PMA_HOST`            | phpmyadmin  | <p style="text-align:justify;margin:0">Cette variable définit l'adresse/le nom d'hôte du serveur MySQL.</p> |db |
|`PMA_PORT`             |phpmyadmin|<p style="text-align:justify;margin:0">Cette variable définit la liste des ports séparés par des virgules des serveurs MySQL.</p>|3306|
|`PMA_USER`             |phpmyadmin|<p style="text-align:justify;margin:0">Cette variable définit le compte qui a accès au portail PhpMyAdmin.</p>|test|
|`PMA_PASSWORD`         |phpmyadmin|<p style="text-align:justify;margin:0">Cette variable définit mot de passe du compte `PMA_USER`.</p>|test|
| `MYSQL_ROOT_PASSWORD` | phpmyadmin  | <p style="text-align:justify;margin:0">Cette variable spécifie le mot de passe qui sera défini pour le compte superutilisateur root.</p> |123456 |

Voici toutes les pièces qui sont marquées en jaune doivent être sont changées.

!!! abstract "Code"
    <strong>docker-compose.yml</strong>
    <pre style="background:hsla(0, 0%, 96%, 1);">
    version: '3.8'
    <br>
    services:
        app:
        build:
            context: .
            dockerfile: Dockerfile
        <i>......</i>
        environment:
            LARAVEL_VERSION: <mark>8.*</mark>
            WITHOUT_DATABASE_CONFIGURATION: <mark>"true"</mark>
            IMT_PROXY: <mark>"false"</mark>
            DB_CONNECTION: <mark>mysql</mark>
            DB_HOST: <mark>db</mark>
            DB_PORT: <mark>3306</mark>
            DB_DATABASE: <mark>laravel</mark>
            DB_USERNAME: <mark>root</mark>
            DB_PASSWORD: <mark>123456</mark>
        <i>......</i><br>
        # MariaDB - Service
        db:
            image: mariadb:10.5.6
            container_name: db
            <i>......</i>
            environment:
                MYSQL_DATABASE: <mark>laravel</mark>
                MYSQL_ROOT_PASSWORD: <mark>123456</mark>
                MYSQL_USER: <mark>test</mark>
                MYSQL_PASSWORD : <mark>test</mark>
        <i>......</i><br>
        # PHPMyAdmin - Service
        phpmyadmin:
            image: phpmyadmin/phpmyadmin:latest
            <i>......</i>
            environment:
                PMA_HOST: <mark>db</mark>
                PMA_PORT: <mark>3306</mark>
                PMA_USER: <mark>test</mark>
                PMA_PASSWORD: <mark>test</mark>
                MYSQL_ROOT_PASSWORD: <mark>123456</mark>
        <i>......</i>
        <br>
    </pre>

### 3 - Exécuter l'application

<p style="text-align:justify">Il faut verifier que l'application tourne correctement et à la première éxecution le système supprime le lien du `.git`. Les étapes pour exécuter l'application sont:</p>

<ol>
    <li>Ovrir un terminal</li>
    <li>
        <p style="text-align:justify">Identifier sur la registry privée de l'IMT par cette commande:</p>
```bash
docker login gitlab-registry.imt-atlantique.fr
```
    </li>
    <li>
        <p style="text-align:justify">Lancer les services: (cette commande fige le terminal)</p>
```bash
docker-compose up --build
```
    </li>
</ol>

<p style="text-align:justify">Dans le terminal, le système affiche ci-dessous indiquant que la configuration est terminée. (selon la ressource de la machinem l'éxécution peut pre)</p>

```
app_1         | ------------------------------------------------
app_1         | Finalizing configuration and setup
app_1         | ------------------------------------------------
app_1         |
app_1         | [xxxxxxxxxxxxxxxxxxxx] NOTICE: fpm is running, pid 1
app_1         | [xxxxxxxxxxxxxxxxxxxx] NOTICE: ready to handle connections

```

### 4 - Création un runner pour le projet

#### Prérequis

Avant de configuration des runners, il faut d'installer gitlab tout d'abord, consulter le menu [Gestion des runners :fontawesome-solid-external-link-alt: ](/backend-laravel-poc-doc/runners/).

Dans la but de lancer le pipeline, il faut créer des runners spécifiques:</p>

#### Runner pour éxécuter les jobs d'analyse de code
<p style="text-align:justify">Pour que l'étape <code>code quality</code> lance, elle a besion d'un runner spécifique avec une configuration spécifique.</p>



<ol> 
<li> <p style="text-align:justify">Executer cette commande dans un terminal en mode admin</p> </li>

```
gitlab-runner register
```

<li>
<p style="text-align:justify">Répondre aux questions que l'écran affiche pour configurer le runner. Les réponses sont coloriées en jaune. Pour le token consulter la page de <a href="/backend-laravel-poc-doc/runners/">Gestion des runners</a>.</p>

<div class="admonition attention">
<p class="admonition-title">Attention: Le tag lp-disi-docker</p>
<p>Le tag du runner doit être le même nom du tag des étapes dans <code>gitlab-ci</code></p>
</div>


```sh hl_lines="2 4 6 8 12 14" 
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.imt-atlantique.fr/
Enter the registration token:
xxxxxxxxxxxxxxxxxx
Enter a description for the runner:
[port-adm-028]: lp-disi-docker
Enter tags for the runner (comma-separated):
code-quality
Registering runner... succeeded                     runner=Tom94gNV
Enter an executor: docker-windows, docker-ssh, shell, ssh, docker+machine, docker-ssh+machine, custom, docker, kubernetes,
parallels, virtualbox:
docker
Enter the default Docker image (for example, ruby:2.6):
docker:stable
```
</li>

<li>Ouvrir le ficher de la configuration des runners de Gitlab <code>config.toml</code>. Il se trouve dans la même place que <code>gitlab-runner.exe</code></li>
<li>Trouver le block de code de votre runner déjà créé et effectuer ses modifications:</li>
<ol> 
<li>
    Ajouter cette ligne au dessous de cette ligne <code>executor = "docker"</code>:
```
builds_dir = "/tmp/builds"
```
</li>
<li>
    Changer cette ligne <code>volumes = ["/cache"]</code> par:
```
volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock", "/tmp/builds:/tmp/builds"]
```
</li>
</ol> 



</ol> 

- Voici un exemple de la configuration fianle de ce runner :
  
```
[[runners]]
  name = "lp-disi-docker"
  url = "https://gitlab.imt-atlantique.fr/"
  token = "GhJPaUsqssT8oxcyifmi"
  executor = "docker"
  builds_dir = "/tmp/builds"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock", "/tmp/builds:/tmp/builds"]
    shm_size = 0
```


#### Runner pour éxécuter les jobs normaux
<p style="text-align:justify">Pour que les étapes lance, elles ont besion d'un runner spécifique avec une configuration spécifique.</p>

<ol> 
<li> <p style="text-align:justify">Executer cette cette commande dans un terminal</p> </li>

```
gitlab-runner register
```

<li>
<p style="text-align:justify">Répondre aux questions que l'écran affiche pour configurer le runner. Les réponses sont coloriées en jaune. Pour le token consulter la page de <a href="/backend-laravel-poc-doc/runners/">Gestion des runners</a>.</p>

<div class="admonition attention">
<p class="admonition-title">Attention: Le tag lp-disi-docker</p>
<p>Le tag du runner doit être le même nom du tag des étapes dans <code>gitlab-ci</code></p>
</div>

```sh hl_lines="2 4 6 8 12 14" 
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.imt-atlantique.fr/
Enter the registration token:
xxxxxxxxxxxxxxxxxx
Enter a description for the runner:
[port-adm-028]: lp-disi-docker
Enter tags for the runner (comma-separated):
lp-disi-docker
Registering runner... succeeded                     runner=Tom94gNV
Enter an executor: docker-windows, docker-ssh, shell, ssh, docker+machine, docker-ssh+machine, custom, docker, kubernetes,
parallels, virtualbox:
docker
Enter the default Docker image (for example, ruby:2.6):
docker:stable
```


</li>
<li>Ouvrir le ficher de la configuration des runners de Gitlab <code>config.toml</code>. Il se trouve dans la même place que <code>gitlab-runner.exe</code></li>
<li>Trouver le block de code de votre runner déjà créé et changer cette ligne <code>privileged = false</code> par: 
```
privileged = true
```
</li>
</ol> 

- Voici un exemple de la configuration fianle de ce runner :

```
[[runners]]
  name = "lp-disi-docker"
  url = "https://gitlab.imt-atlantique.fr/"
  token = "-yWF5o4svCysVPk7qxW_"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```
#### Exécuter les runners
<p style="text-align:justify">Dans un terminal tout en se déplaçant ou l'exécutable <code>gitlab-runner.exe</code> se trouve, on éxécute cette ligne de commande:</p>

```
gitlab-runner.exe run
```

### 5 - Connecter à gitlab

<p style="text-align:justify">La dernière étape vise à se connecter au gitlab,on ouvre un deuxième terminal dans phpStorm et en utilisant la ligne de commande, on exécute ces commandes :</p>
    
!!! info "Information"
    <p style="text-align:justify">Pour avoir le lien git du nouveau projet déjà créé dans la première étape :</p>
    <ol>
        <li>Aller au repository git di nouveu projet</li>
        <li>Cliquer sur le bouton **Clone v** et copier le lien de clone with HTTPS</li>
    </ol>

!!! info "Information"
    C'est 3e bloque de code affiché dans la page de projet vierge lorsque nous créons un projet gitlab
    <pre>
    ```
    git init
    git remote add origin https://gitlab.imt-atlantique.fr/xxxxxxxxxx/<name project>.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master
    ```
    </pre>

## Les commandes pour manipuler l'application

<p style="text-align:justify">Voici les commandes de lignes pour lancer, arrêter et executer des services :</p>

#### Lancer les services

<p style="text-align:justify">Avant de lancer les service, il faut se identifier sur la registry privée de l'IMT par cette commande:</p> 

```bash
docker login gitlab-registry.imt-atlantique.fr
```

<p style="text-align:justify">La commande pour lancer les services :</p>

```bash
docker-compose up --build
```

- Pour tester l'application : Connexion via the url: [http://localhost:8100/](http://localhost:8100/)
- Pour accéder à la base de données par PhpMyAdmin: Connexion via the url: [http://localhost:8080/](http://localhost:8080/)
- Pour excuter de differentes commandes `docker-compose exec <service_name> <command>`, exemple:

```bash
docker-compose exec app php artisan migrate
```

### Arret les services

<p style="text-align:justify">La commande pour arrèter les services :</p>

```bash
docker-compose down
```
<p style="text-align:justify">La commande pour arrèter les services avec supprission des volumes :</p>
```bash
docker-compose down -v
```