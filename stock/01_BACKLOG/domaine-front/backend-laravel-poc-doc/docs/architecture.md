# Architechture des images


!!! example ""
    <div style="line-height:2em;vertical-align:middle;color:#651fff;">
        <span style="font-size:1.5em">:material-test-tube:</span>
        <span style="font-size:1.1em">Version expérimentale</span>
    </div>
## Introduction

<p style="text-align:justify">Avec Docker, nous sommes entrés dans l’ère des architectures à base de « conteneur ». Le produit du concept (POC) est constitué par une une image Laravel qui est basé sur une image php-fpm. Le découplage entre les images permet d'éliminer les dépendences entre le moteur PHP et un framework de PHP. Alors il possible d'évolué la version d'une image sans impacter l'autre. De plus une seule image PHP peut être utlisée par plusieurs images de framework de PHP (Laravel, cake, symphony, ...).</p>

![architectiue des images docker](images/architecture-docker.png)

## Image PHP
<p style="text-align:justify">PHP-FPM (de l'anglais FastCGI Process Manager, littéralement "gestionnaire de processus FastCGI") est une interface permettant la communication entre un serveur Web et PHP, basée sur le protocole FastCGI. PHP-FPM constitue ainsi une alternative au serveur PHP avec des options pour les sites subissant de fortes charges. Contrairement au serveur PHP, il est fourni avec son propre daemon.</p>
L'image docker PHP est constitée de 3 étapes:

1. **Image de base** 
    <p style="text-align:justify">
    Partir d'une image de base **php:8.0-fpm** avec le réglage du fuseau horaire de Paris en exposant le port 9000 du conteneur.
    </p>
2. **Installation des extentions de PHP**
    <p style="text-align:justify">
    Avant de lancer le serveur PHP, l'installation des outils et des extentions de PHP s'exécute en installant ces librairies :   
    - <i>build-essential</i>: liste informative des paquets de construction essentiels.
    - <i>curl</i>: récupérer le contenu d'une ressource accessible par un réseau informatique.
    - <i>libmemcached-dev</i>: bibliothèque cliente C et C++ sur le serveur memcached.
    - <i>locales</i>: prise en charge des données de langages régionaux.
    - <i>zip</i>: archiver for .zip files.
    - <i>unzip</i>: de-archiver for .zip files.
    - <i>libz-dev</i>: compression et décompression aux formats gzip et zlib.
    - <i>libpq-dev</i>: fichiers d'en-tête pour libpq5 (bibliothèque PostgreSQL).
    - <i>libjpeg-dev</i>: fichiers de développement pour la bibliothèque JPEG,
    - <i>libpng-dev</i>: bibliothèque PNG.
    - <i>libfreetype6-dev</i>: gestion de polices de caractères FreeType 2.
    - <i>libssl-dev</i>: trousse à outils SSL.
    - <i>libwebp-dev</i>: bibliothèque de compression d'images photographiques numériques.
    - <i>libxpm-dev</i>: bibliothèque X11 pixmap.
    - <i>libmcrypt-dev</i>: bibliothèque de cryptage/décryptage.
    - <i>libonig-dev</i>: fichiers de développement pour libonig2.
    - <i>gd</i>: bibliothèque servant à manipuler des images dynamiquement
    </p>
3. **Installation de composer** 
    <p style="text-align:justify">
    Cette étape permet d'installer **Composer** qui est un outil de gestion des dépendances en PHP. Il permet de déclarer les bibliothèques dont dépend le projet et il les gérera (installer/mettre à jour).
    </p>
    <p style="text-align:justify">
    Ensuite l'initialisation du répertoire de base `/var/www` ou il se trouve les dossiers et les fichiers de l'applicataion PHP à conteneuriser.
    </p>

!!! abstract "Code"
    <strong>docker-compose.yml</strong>
    <pre style="background:hsla(0, 0%, 96%, 1);">

        #
        #--------------------------------------------------------------------------
        # 1 - IAMGE SETUP
        #--------------------------------------------------------------------------
        #
        
        # BASE IMAGE
        FROM php:8.0-fpm

        LABEL maintainer="Abou Ahmad Georges georges.abou-ahmad@imt-atlantique.net"


        #EXPOSING PORT
        EXPOSE 9000

        # SETTING TIMEZONE
        ENV TZ=Europe/Paris
        RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


        #
        #--------------------------------------------------------------------------
        # 2 - SOFTWARE'S INSTALLATION
        #--------------------------------------------------------------------------
        #
        # INSTALLING TOOLS AND PHP EXTENTIONS USING "apt", "docker-php", "pecl",
        #

        # INTSALL "curl", "libmemcached-dev", "libpq-dev", "libjpeg-dev",
        #         "libpng-dev", "libfreetype6-dev", "libssl-dev", "libmcrypt-dev",
        #         "zip", "unzip", "locales", "build-essential"
        RUN set -eux; \
                apt-get update; \
                apt-get upgrade -y; \
                apt-get install -y --no-install-recommends \
                apt-utils \
                build-essential \
                curl \
                libmemcached-dev \
                locales \
                zip \
                unzip \
                libz-dev \
                libpq-dev \
                libjpeg-dev \
                libpng-dev \
                libfreetype6-dev \
                libssl-dev \
                libwebp-dev \
                libxpm-dev \
                libmcrypt-dev \
                libonig-dev

        # CLEAR CACHE
        RUN apt-get clean && rm -rf /var/lib/apt/lists/*

        # INSTALL EXTENTIONS
        RUN set -eux; \
            # INSTALL THE PHP PDO_MYSQL EXTENTION
            docker-php-ext-install pdo_mysql; \
            # INSTALL THE PHP PDO_PGSQL EXTENTION
            docker-php-ext-install pdo_pgsql; \
            # INSTALL THE PHP GD LIBRARY
            docker-php-ext-configure gd \
                    --prefix=/usr \
                    --with-jpeg \
                    --with-webp \
                    --with-xpm \
                    --with-freetype; \
            docker-php-ext-install gd; \
            php -r 'var_dump(gd_info());'


        #
        #--------------------------------------------------------------------------
        # 3 - COMPOSER INSTALLATION
        #--------------------------------------------------------------------------
        #

        # INSTALL COMPOSER
        RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

        # SETTING WORK DIRECTORY
        WORKDIR /var/www
    </pre>

## Image Laravel

![architectiue des images docker](images/laravel-architecture-docker.png){ align=right }

<p style="text-align:justify">
Basé sur notre propre image de PHP-fpm, cette image ajoute une couche de code Laravel permettant d'obtenir une application Laravel fonctionnelle indépendante de la couche PHP.</p> 


Cette image de Laravel est constituée de 4 étapes:

1. **Configuration de l'image**
    <p style="text-align:justify">
    Dans cette étape, l'image docker se base sur une image de php créée précédemment.
    </p>
2. **Installation de l'application**
    <p style="text-align:justify">
    Cette étape va copier les fichiers et les dossiers du projet dans le conteneur. 
    </p>
3. **Exécusion de fichier shell lors du démarage du conteneur**
    <p style="text-align:justify">
    Lors du lancement du conteneur, dans le but d'initialiser un bon environnement, les premières instructions qui se trouve dans le script bash `docker-entrypoint.sh` s'exécutent par cet ordre :
    </p>
    1. Création du ficher `.env` avec injection des variables de la base de données. Le fichier `.env` est responsble de créer une connexion à la base de données et de stocker la clef de l'application Laravel.
    2. Installation et mettre à jour les dépendences de Laravel via composer.
    3. Génération de la clef de l'application de Laravel avec artisant.
    4. Exécution des mises à jour à la base de données avec artisant.
    5. Création du ficher `.env.testing` pour configurer l'environnement de testing.
   
4. **Exécusion du conteneur**
    <p style="text-align:justify">
    Le conteneur se lance par la commande `php-fpm`.
    </p>

!!! abstract "Code"
    <strong>docker-compose.yml</strong>
    <pre style="background:hsla(0, 0%, 96%, 1);">

        #
        #--------------------------------------------------------------------------
        # 1 - IAMGE SETUP
        #--------------------------------------------------------------------------
        #

        # BASE IMAGE
        FROM gitlab-registry.imt-atlantique.fr/domaine-front/php-laravel/php:1.0.0

        LABEL maintainer="Georges Abou Ahmad georges.abou-ahmad@imt-atlantique.net"

        # Add user for laravel
        # RUN groupadd -g 1000 www
        # RUN useradd -u 1000 -ms /bin/bash -g www www

        #
        #--------------------------------------------------------------------------
        # 2 - APPLICATION'S INSTALLATION
        #--------------------------------------------------------------------------
        #

        # COPY APPLICATION FOLDER
        COPY . /var/www

        # Copy existing permissions from folder to docker
        # COPY --chown=www:www . /var/www
        # RUN chown -R www:www /var/www

        #
        #--------------------------------------------------------------------------
        # 3 - RUNNUNG SHELL ON START UP CONTAINER
        #--------------------------------------------------------------------------
        #

        # EXECUTE DOCKER ENTRYPOINT
        RUN chmod +x docker-entrypoint.sh
        ENTRYPOINT [ "./docker-entrypoint.sh" ] 

        # change current user to www
        # USER www


        #
        #--------------------------------------------------------------------------
        # 4 - RUNNUNG CONTAINER
        #--------------------------------------------------------------------------
        #

        # START FPM SERVER
        CMD ["php-fpm"]

    </pre>