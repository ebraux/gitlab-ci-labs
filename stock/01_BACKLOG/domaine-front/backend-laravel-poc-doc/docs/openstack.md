# Gestion d'Openstack

## Prérequis
Il faut demander à l'équipe de l'infrasctrure un projet sur Openstack. Il est recommendé d'avoir un projet Openstack par projet applicatif.

## Introduction

<p style="text-align:justify">Afin d'automatiser le déploiement de l'infrastructure, nous avons mis en place un environnement openstack et un projet basé sur le déploiement de l'infrastructure dans le domaine front.</p>

## Open Stack
OpenStack est un ensemble de logiciels open source permettant de déployer des infrastructures de cloud computing. 

### Avoir les variables de l'api du projet sur Openstack

<p style="text-align:justify">Le projet du déploiement de l'infrastructure est connecté au projet sur la plate-forme Openstack via des apis, nous devons collecter ces variables afin de les utiliser plus tard. Ces variables seront détaillées dans la configuration des variables gitlab.<br>
Pour les avoir, nous suivons ces étapes:</p>

1. Cliquer sur votre **nom d'utilisateur** en haut à droite de la page
2. Choisir l'option **Ficher openstack RC v3** dans la liste déroulante<br>
   Le système téléchargera un fichier shell que nous utiliserons plus tard

![téléchargement des varibales](images/openstack/download-api.png){ loading=lazy }

### Paires de clés

1. Cliquer sur **Paires de clés** dans la section **Vue d'ensemble** dans le menu qui se situe a gauche de la page
2. Clique sur le boutton **Créer une paire de clés**
3. Saisire le nom de la paire de clés dans le champ **Nom de la paire de clés** 
4. Cliquer sur le button **Créer une paire de clés**

<p style="text-align:justify">Le système crée la clef et il télécharge un ficher d'extention <code>.pem</code> contenant la clef privée qu'on utilisera après pour une connexion SSH sur les machines dans Openstack qui sont assignées avec cette clef.</p>

### IP Flottantes
<!-- ![tous les pipelines](images/openstack/menu-IP-flottantes.png) -->
<p style="text-align:justify">Chaque instance a une adresse IP privée et fixe et peut également avoir une adresse IP publique ou flottante. Les adresses IP privées sont utilisées pour la communication entre les instances et les adresses publiques sont utilisées pour la communication avec les réseaux en dehors du cloud, y compris Internet.<br>
</p>

#### Allouer une IP flottante

<p style="text-align:justify">Pour chaque projet, on a besion des ip floattantes afin d'accéder aux instances du projets alors on associe à chaque instance une ip flotantte.<br>
Pour les avoir, nous suivons ces étapes:</p>

1. Cliquer sur **Réseau** dans le menu qui se situe a gauche de la page
2. Choisir l'onglet **IP Flottante**
3. Cliquer sur le button **Allouer une adresse IP au projet**
4. Saisie une **description** pour la nouvelle adresse IP qui se trouve dans la nouvelle fenêtre qui apparaît contenant un formulaire pour allouer une ip flottante
5. Cliquer sur le boutton **Allocation d'IP**

![téléchargement des varibales](images/openstack/allocation-ip.png){ loading=lazy }


### Groupes de sécurité
<p style="text-align:justify">
Le groupe de sécurité est le pare-feu qui va protéger les instances en gérant les autorisations d’accès au réseau.
Le groupe de sécurité - security group - par défaut autorise toutes les communications sortantes mais aucune communication entrante.
Les règles de groupe ne sont pas figées et peuvent donc être modifiées à tout moment : soit en supprimant, soit en ajoutant de nouvelles règles.
Les groupes de securités peuvent être utilisés séparément ou de manière cumulée.
</p>

#### Céer un groupe de sécurité

<!-- <p style="text-align:justify">Les règles </p> -->

1. Cliquer sur **Réseau** dans le menu qui se situe a gauche de la page
2. Choisir l'onglet **Groupes de sécurité**
3. Cliquer sur le boutton **Créer un groupe de sécurité**
4. Saisir **ingress_allow** dans le champ du **Nom**
5. Cliquer sur le boutton **Créer un groupe de sécurité** 

#### Gérer les règles du groupe de sécurité 

1. Cliquer sur **Réseau** dans le menu qui se situe a gauche de la page
2. Choisir l'onglet **Groupes de sécurité**
3. Cliquer sur le boutton **Gérer les règles** pour le groupe de sécurité **ingress_allow**

##### Ajouter une règle SSH

1. Cliquer sur le boutton **Ajouter une règle** et un formulaire apparaît
2. Choisir l'option **SSH** de la liste déroulante du champ **Règle**
3. Cliquer sur Ajouter

##### Ajouter une règle HTTP

1. Cliquer sur le boutton **Ajouter une règle** et un formulaire apparaît
2. Choisir l'option **HTTP** de la liste déroulante du champ **Règle**
3. Cliquer sur Ajouter

##### Ajouter une règle HTTPS

1. Cliquer sur le boutton **Ajouter une règle** et un formulaire apparaît
2. Choisir l'option **HTTPS** de la liste déroulante du champ **Règle**
3. Cliquer sur Ajouter

##### Ajouter une règle de PhpMyAdmin

1. Cliquer sur le boutton **Ajouter une règle** et un formulaire apparaît
2. Choisir l'option **Règle TCP personnalisée** de la liste déroulante du champ **Règle**
3. Choisir l'option **Entrée** de la liste déroulante du champ **Direction**
4. Saisir **8080** dans le champs **Port**
5. Cliquer sur Ajouter