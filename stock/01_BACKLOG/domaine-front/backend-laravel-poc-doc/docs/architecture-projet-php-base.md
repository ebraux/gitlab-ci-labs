# Projet de PHP de base
<p style="text-align:justify">Ce projet aboutit à créer une image docker contenant une interface permettant la communication entre un serveur Web et PHP, basée sur le protocole FastCGI.</p>

Arborescence du projet

```
php-laravel
├──.gitlab-ci.yml
├──Dockerfile
```

`.gitlab-ci.yml`: <p style="text-align:justify">Le pipeline pour publier une image de PHP-FPM base sur le Dockerfile existant dans le projet.</p>

!!! abstract "Code"
    <strong>docker-compose.yml</strong>
    <pre style="background:hsla(0, 0%, 96%, 1);">
        
        docker-build:
        # OFFICIAL DOCKER IMAGE
        image: docker:20.10
        variables:
            DOCKER_DRIVER: overlay2
            DOCKER_TLS_CERTDIR: ""
            APP_CRITIC_VERSION: 1
            APP_MAJOR_VERSION: 0
            APP_MINOR_VERSION: 0
            IMAGE_NAME: "php"
            MY_IMAGE_REGISTRY: ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${APP_CRITIC_VERSION}.${APP_MAJOR_VERSION}.${APP_MINOR_VERSION}
        services:
            - docker:20.10-dind
        stage: build
        tags:
            - build-php
        before_script:
            - echo ${MY_IMAGE_REGISTRY}
            # CONNECT TO REGISTRY
            - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY  
        script:
            # BUILDING IMAGE
            - docker build -t ${MY_IMAGE_REGISTRY}  .
            # PUSHING IMAGE IN THE REGISTRY
            - docker push ${MY_IMAGE_REGISTRY}
            # CLEAN UP
            - docker rm --force ${MY_IMAGE_REGISTRY}
            - docker logout
    </pre>


`Dockerfile`:<p style="text-align:justify">Fichier de docker permettant de construire une image de PHP-FPM.</p>

!!! abstract "Code"
    <strong>docker-compose.yml</strong>
    <pre style="background:hsla(0, 0%, 96%, 1);">

        #
        #--------------------------------------------------------------------------
        # 1 - IAMGE SETUP
        #--------------------------------------------------------------------------
        #
        
        # BASE IMAGE
        FROM php:8.0-fpm

        LABEL maintainer="Abou Ahmad Georges georges.abou-ahmad@imt-atlantique.net"


        #EXPOSING PORT
        EXPOSE 9000

        # SETTING TIMEZONE
        ENV TZ=Europe/Paris
        RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


        #
        #--------------------------------------------------------------------------
        # 2 - SOFTWARE'S INSTALLATION
        #--------------------------------------------------------------------------
        #
        # INSTALLING TOOLS AND PHP EXTENTIONS USING "apt", "docker-php", "pecl",
        #

        # INTSALL "curl", "libmemcached-dev", "libpq-dev", "libjpeg-dev",
        #         "libpng-dev", "libfreetype6-dev", "libssl-dev", "libmcrypt-dev",
        #         "zip", "unzip", "locales", "build-essential"
        RUN set -eux; \
                apt-get update; \
                apt-get upgrade -y; \
                apt-get install -y --no-install-recommends \
                apt-utils \
                build-essential \
                curl \
                libmemcached-dev \
                locales \
                zip \
                unzip \
                libz-dev \
                libpq-dev \
                libjpeg-dev \
                libpng-dev \
                libfreetype6-dev \
                libssl-dev \
                libwebp-dev \
                libxpm-dev \
                libmcrypt-dev \
                libonig-dev

        # CLEAR CACHE
        RUN apt-get clean && rm -rf /var/lib/apt/lists/*

        # INSTALL EXTENTIONS
        RUN set -eux; \
            # INSTALL THE PHP PDO_MYSQL EXTENTION
            docker-php-ext-install pdo_mysql; \
            # INSTALL THE PHP PDO_PGSQL EXTENTION
            docker-php-ext-install pdo_pgsql; \
            # INSTALL THE PHP GD LIBRARY
            docker-php-ext-configure gd \
                    --prefix=/usr \
                    --with-jpeg \
                    --with-webp \
                    --with-xpm \
                    --with-freetype; \
            docker-php-ext-install gd; \
            php -r 'var_dump(gd_info());'


        #
        #--------------------------------------------------------------------------
        # 3 - COMPOSER INSTALLATION
        #--------------------------------------------------------------------------
        #

        # INSTALL COMPOSER
        RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

        # SETTING WORK DIRECTORY
        WORKDIR /var/www
    </pre>