# Gestion du projet de l'infrasctructure
## Prérequis
Il faut demander à l'équipe de l'infrasctrure un projet sur Openstack. Il est recommendé d'avoir un projet Openstack par projet applicatif.
## Introduction
<p style="text-align:justify">
Ce Projet est responsable de déployer les infrasctructures dans openstack dans le but d'heberger des clusters des projet web de Laravel. Maintenant il faut configurer terraform et ansible. Terraform est utilisé
pour créer l'infrastucture et ansible pour lancer des commandes à distance.
</p>

### Cloner le projet

1. Aller au [le projet infrasctucture sur gitlab  :fontawesome-solid-external-link-alt:](https://gitlab.imt-atlantique.fr/domaine-front/backend-laravel-infra-deploiment)
2. Cliquer sur le bouton **Clone v** et copier le lien de clone with HTTPS
3. Ouvrir Phpstorm et cliquer sur le button **Get from VCS**
4. Coller le lien qui est déjà copié dans le champ URL
5. Renomer le dossier du projet avec un nom unique.En utilisant le champs directory et en cliquant sur le button de dossier on crée un nouveau dossier 
6. Cliquer sur le boutton **clone**
7. En utilisant **Exploiteur des fichers** dans windows ou Linux, aller au nouveau répertoire déjà créé.
8. **Supprimer** le dossier `.git`

### Créer un runner pour le projet

#### Prérequis

Avant de configuration des runners, il faut d'installer gitlab tout d'abord, consulter le menu [Gestion des runners :fontawesome-solid-external-link-alt: ](/backend-laravel-poc-doc/runners/).

Dans la but de lancer le pipeline, il faut créer des runners spécifiques:</p>

#### Runner pour éxécuter les jobs d'analyse de code
<p style="text-align:justify">Pour que l'étape <code>code quality</code> lance, elle a besion d'un runner spécifique avec une configuration spécifique.</p>

<ol> 
<li> <p style="text-align:justify">Executer cette commande dans un terminal en mode admin</p> </li>

```
gitlab-runner register
```

<li>
<p style="text-align:justify">Répondre aux questions que l'écran affiche pour configurer le runner. Les réponses sont coloriées en jaune. Pour le token consulter la page de <a href="/backend-laravel-poc-doc/runners/">Gestion des runners</a>.</p>

<div class="admonition attention">
<p class="admonition-title">Attention: Le tag lp-disi-docker</p>
<p>Le tag du runner doit être le même nom du tag des étapes dans <code>gitlab-ci</code></p>
</div>


```sh hl_lines="2 4 6 8 12 14" 
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.imt-atlantique.fr/
Enter the registration token:
xxxxxxxxxxxxxxxxxx
Enter a description for the runner:
[port-adm-028]: lp-disi-docker
Enter tags for the runner (comma-separated):
code-quality
Registering runner... succeeded                     runner=Tom94gNV
Enter an executor: docker-windows, docker-ssh, shell, ssh, docker+machine, docker-ssh+machine, custom, docker, kubernetes,
parallels, virtualbox:
docker
Enter the default Docker image (for example, ruby:2.6):
docker:stable
```
</li>

<li>Ouvrir le ficher de la configuration des runners de Gitlab <code>config.toml</code>. Il se trouve dans la même place que <code>gitlab-runner.exe</code></li>
<li>Trouver le block de code de votre runner déjà créé et effectuer ses modifications:</li>
</ol> 

- Voici un exemple de la configuration fianle de ce runner :
  
```
[[runners]]
  name = "lp-disi-docker"
  url = "https://gitlab.imt-atlantique.fr/"
  token = "GhJPaUsqssT8oxcyifmi"
  executor = "docker"
  builds_dir = "/tmp/builds"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock", "/tmp/builds:/tmp/builds"]
    shm_size = 0
```


### Paramétriser Terraform
<p style="text-align:justify">Pour configurer le déploimenent des infrasctuctures, le projet est paramétrisé par des fichiers de <code>.tfvars</code>. Il existe un fichier de <code>.tfvars</code> correspondant à un environement précis: </p>

- `inte.tfvars` : correspond à l'environement de **developement**.
- `rec.tfvars` : correspond à l'environement de **recette**.

<p style="text-align:justify">Chaque fichier de <code>.tfvars</code> utilisé suit un format précis contenant ces variables dans le but de configurer l'environement. Ces variables sont responsables pour régler l'environement. </p>

- `external_network` : 
    - **type** : text
    - **description** : Le nom du reseau externe (internet).
    - **valeur** : `external`
- `internal_network` :
    - **type** : text
    - **description** : Le nom du reseau interne qui connecte les instances entre eux.
    - **valeur** : `reseau_xxxxxxxxx`
- `external_gateway` :
    - **type** : text
    - **description:** : Le nom du routeur entre le réseau interne et le réseau externe.
    - **valeur** : `routeur_xxxxxxxxx`
- `dns_ip` :
    - **type** : liste de texte
    - **description** : Si vous souhaitez créer un sous réseau, et si vous voulez que les machines connectées à ce sous réseau puissent accèder à des ressources externes, vous devez configurer la résolution de nom, pour utiliser les serveurs DNS d'IMT Atlantique.
    - **valeur** : ``` dns_ip = ["192.44.75.10", "192.108.115.2"]```
- `http_intances` :
    - **type** : liste d'objet
    - **description** : La liste qui va créer les vm dans Openstack. Chaque objet est constituer de deux champs : 
        - `name`: nom de l'instance
        - `floating_ip_address` : ip flottante qu'on a créer avant dans l'étape  [IP flottante  :fontawesome-solid-external-link-alt:](/backend-laravel-poc-doc/openstack#ip-flottantes) 
    - **valeur (exemple)** : `[{ name = "master-inte" floating_ip_address = "10.29.244.36"},{ name = "worker-inte-01" floating_ip_address = "10.29.244.103"},{ name = "worker-inte-02" floating_ip_address = "10.29.244.86"}]`
    ![téléchargement des varibales](images/openstack/instance_liste.png){ loading=lazy }
- `image_name` : 
    - **type** : text
    - **description** : Le nom de l'image souhaitée pour le serveur. Changer cela crée un nouveau serveur.
    -  **valeur** : `imta-ubuntu-docker`
- `flavor_name` : 
    - **type** : text
    - **description** : Le nom de la capacité de mémoire souhaitée pour le serveur. Changer cela redimensionne le serveur existant. 
    - **valeur** : `m1.medium`
- `key_pair` : 
    - **type** : text
    - **description**: Le nom d'une paire de clés à mettre sur le serveur. La paire de clés doit déjà être créée et associée au compte du locataire. Changer cela crée un nouveau serveur. 
    - **valeur (exemple)** : `geo-admin`

### Ajouter les variables d'Openstack dans la Chaine de livraison

#### La façon d'ajouter des varaible dans Gitlab
1. Aller **Paramètre > Intégration et livraison continues**
2. Cliquer sur le button **Étendre** de la section **Variables**
3. Cliquer sur le bouton **Add variable** et le système affiche un formulaire
4. Saisir le nom de la variable dans le champ key
5. Saisir la valeur de la variable dans le champs value
6. *(Seulement pour la variable **CI_OS_PASSWORD**)* Cocher la case Mask variable pour la variable **CI_OS_PASSWORD**
7. Cliquer sur le boutton **Add variable**
#### Les variables à ajouter

<!-- 1. Ouvrir le ficher téléchargé dans l'étape : [avoir les variables du projet sur openstack  :fontawesome-solid-external-link-alt:](#avoir-les-variables-de-lapi-du-projet-sur-openstack) par un IDE ou un éditeur de text. 
2. Suivant la façon d'ajouter des varaibles dans Gitlab ci-dessus, ajouter les varables de la CI avec la correspondance des variables et leurs valeurs qui se trouvent dans le fichier téléchargé
3.  -->

Récupérer les variables du ficher téléchargé dans l'étape : [avoir les variables du projet sur openstack  :fontawesome-solid-external-link-alt:](/backend-laravel-poc-doc/openstack#avoir-les-variables-de-lapi-du-projet-sur-openstack) et ajouter ces variables et leurs valeurs correspondantes dans les variables de la CI

| Variable de la CI       | Variables du fichier Openstack | Valeur (comme exemple)                      | Protected                 | Masked                    |
| :---------------------- | :----------------------------- | :------------------------------------------ | :-----------------------: | :-----------------------: |
| CI_OS_AUTH_URL          | OS_AUTH_URL                    | https://openstack.imt-atlantique.fr:5000/v3 | :fontawesome-solid-times: | :fontawesome-solid-times: |
| CI_OS_PROJECT_ID        | OS_PROJECT_ID                  | 91c861ffa0aa41d2988e22c24e16cd34            | :fontawesome-solid-times: | :fontawesome-solid-check: |
| CI_OS_PROJECT_NAME      | OS_PROJECT_NAME                | laravel-poc                                 | :fontawesome-solid-times: | :fontawesome-solid-times: |
| CI_OS_USER_DOMAIN_NAME  | OS_USER_DOMAIN_NAME            | Default                                     | :fontawesome-solid-times: | :fontawesome-solid-times: |
| CI_OS_USERNAME          | OS_USERNAME                    | g20aboua                                    | :fontawesome-solid-times: | :fontawesome-solid-times: |
| CI_OS_PASSWORD          | OS_PASSWORD                    | xxxxxxxxxxxx                                | :fontawesome-solid-times: | :fontawesome-solid-times: |
| CI_OS_REGION_NAME       | OS_REGION_NAME                 | RegionOne                                   | :fontawesome-solid-times: | :fontawesome-solid-times: |

### Configuration d'Ansible pour les hosts et déploiement d'un cluster
#### Configuration du fichier hosts

1. Connecter au **Openstack** et aller à la page **compute > Instances** et on le laisse en arrière plan
2. Dans un IDE (PhpStrorm) ou un éditeur de texte, ouvrir le fichier **hosts** qui se trouve dans **ansible > inventory**. Ce fichier contient la liste des hotes pour les accéder via ansible.<br>
   Voici quelque explications du fichier
    Groupe(s) Ansible pour la getsion des cluster Swarm :
    - des groupes globaux
        - swarm_node : toutes les machine appartenant à un cluster swarm
        - swarm_manager : toutes les machines ayant le rôle "Manager" dans un des cluster Swarm
    - des groupes spécifiques à chaque cluster
        - swarm_manager_XXX : les machines ayant le rôle "Manager" dans le cluster Swarm XXX
        - swarm_worker_XXX : les machines ayant le rôle "Worker" dans le cluster Swarm XXX
        - swarm_cluster_XXX : toutes les machines ayant appartenant au cluster Swarm XXX

3. Correspondre chaque valeur de la page **Instances** d'Openstack avec le fichier **Hosts**.
    - Créer les groupes "swarm_manager_XXX"  et "swarm_worker_XXX" Utilisée par le rôle "jtyr.ansible_docker_swarm" pour déployer les clusters
    - Créer le groupe  "swarm_cluster_XXX", qui rassemble ces 2 groupes
    - Déclarer des Hosts avec un alias lisible, et définir leur adresse IP

    Exemple de déclaration
    
```sh hl_lines="2 3 6  9 10" 
    [swarm_cluster_prod:children]
    swarm_manager_XXX
    swarm_worker_XXX

    [swarm_manager_prod]
    laravel-master-prod-01 ansible_host=10.29.XXX.XXX

    [swarm_worker_prod]
    laravel-worker-prod-02 ansible_host=10.29.XXX.XXX
    laravel-worker-prod-03 ansible_host=10.29.XXX.XXX
```

- Associer les groupes créés au groupes de gestion globale "swarm_node" et "swarm_manager" Comme la photo ci-dessous: 

```sh hl_lines="2 5" 
    [swarm_node:children]
    swarm_cluster_XXX

    [swarm_manager:children]
    swarm_manager_XXX
```
    
![téléchargement des varibales](images/openstack/ans-hosts.png){ loading=lazy }

#### Configuration du cluster

1. Aller à **ansible > inventory > group_vars**
2. Créer le fichier de groupe de variables spécifique au cluster "swarm_cluster_XXX.yml", et y définir la configuration spécifique du cluster Swarm

Voici un exemple du fichier **swarm_cluster_inte.yml** 

```bash
---
# -------------------------------------
#  _____      ____ _ _ __ _ __ ___
# / __\ \ /\ / / _` | '__| '_ ` _ \
# \__ \\ V  V / (_| | |  | | | | | |
# |___/ \_/\_/ \__,_|_|  |_| |_| |_|
# -------------------------------------
#
# Group name for all nodes in the cluster (managers + workers)
docker_swarm_group: swarm_cluster_inte

# Group name for manager nodes
docker_swarm_group_managers: swarm_manager_inte

# Group name for worker nodes
docker_swarm_group_workers: swarm_worker_inte

# Default node network interface through which the nodes in the cluster communicate
docker_swarm_node_iface: ens3

docker_swarm_node_labels:
    cluster: swarm_cluster_inte
```

### Connecter à gitlab

<p style="text-align:justify">La dernière étape vise à se connecter au gitlab,on ouvre un terminal dans phpStorm et en utilisant la ligne de commande, on exécute ces commandes :</p>
<p style="text-align:justify">Pour avoir le lien git du nouveau projet déjà créé dans l'étape de cloner le projet :</p>
<ol>
    <li>Aller au repository git di nouveu projet</li>
    <li>Cliquer sur le bouton **Clone v** et copier le lien de clone with HTTPS</li>
</ol>

!!! info "Information"
    C'est 3e bloque de code affiché dans la page de projet vierge lorsque nous créons un projet gitlab
    <pre>
    ```
    git init
    git remote add origin https://gitlab.imt-atlantique.fr/xxxxxxxxxx/<name project>.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master
    ```
    </pre>

