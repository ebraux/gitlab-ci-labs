# Architechture des projets


!!! example ""
    <div style="line-height:2em;vertical-align:middle;color:#651fff;">
        <span style="font-size:1.5em">:material-test-tube:</span>
        <span style="font-size:1.1em">Version expérimentale</span>
    </div>


<p style="text-align:justify">
L'architecture des microservices est conçue sur mesure pour DevOps avec son approche basée sur les services qui permet aux organisations de décomposer l'application en services plus petits. Cela permet aux équipes de livraison d'aborder les services individuels en tant qu'entités distinctes, simplifiant ainsi le développement, les tests et le déploiement.</p>
<p style="text-align:justify">Voici la vue globale de tous les projets (microservices) et de leurs interactions les uns avec les autres:</p>

![architectiue de la solution](images/architecture-projets.png)

<p style="text-align:justify">Pour chaque projet, le chef doit clonner les projets pour appliquer l'automatisation prédéfinie. Les projets à clonner sont:</p>

<ul>
    <li>Projet de base Laravel</li>
    <li>Projet de déploiement d'infrasctructure</li>
    <li>Projet de déploiement applicatif</li>
</ul>

!!! info "Pour plus d'information comment lancer un nouveau projet"
    Naviguer à la rubrique lancer un nouveau projet [lancer un nouveau projet](/nouveau-projet)