# Intégration gitlab avec le POC

!!! example ""
    <div style="line-height:2em;vertical-align:middle;color:#651fff;">
        <span style="font-size:1.5em">:material-test-tube:</span>
        <span style="font-size:1.1em">Version expérimentale</span>
    </div>

## Configuration du formatage et d'espace de git 
<p style="text-align:justify">Les problèmes de formatage et d'espaces sont parmi les problèmes les plus frustrants et les plus subtils que de nombreux développeurs rencontrent lorsqu'ils collaborent, en particulier sur plusieurs plates-formes. Il est très facile pour les correctifs ou d'autres travaux en collaboration d'introduire des modifications subtiles des espaces blancs, car les éditeurs les introduisent silencieusement, et si vos fichiers touchent un système Windows, leurs fins de ligne peuvent être remplacées. Git propose quelques options de configuration pour vous aider à résoudre ces problèmes.</p>
`core.autocrlf`

<p style="text-align:justify">Si vous programmez sous Windows et travaillez avec des personnes qui ne le sont pas (ou vice-versa), vous rencontrerez probablement des problèmes de fin de ligne à un moment donné. En effet, Windows utilise à la fois un caractère de retour chariot et un caractère de saut de ligne pour les nouvelles lignes dans ses fichiers, alors que les systèmes macOS et Linux n'utilisent que le caractère de saut de ligne. C'est un fait subtil mais incroyablement ennuyeux du travail multiplateforme ; de nombreux éditeurs sur Windows remplacent silencieusement les fins de ligne de style LF existantes par CRLF, ou insèrent les deux caractères de fin de ligne lorsque l'utilisateur appuie sur la touche Entrée.</p>

<p style="text-align:justify">Git peut gérer cela en convertissant automatiquement les fins de ligne CRLF en LF lorsque vous ajoutez un fichier à l'index, et vice versa lorsqu'il extrait le code de votre système de fichiers. Vous pouvez activer cette fonctionnalité avec le paramètre core.autocrlf. Si vous êtes sur une machine Windows, définissez-le sur true — cela convertit les terminaisons LF en CRLF lorsque vous extrayez le code :</p>

```
git config --global core.autocrlf true
```

<p style="text-align:justify">Si vous utilisez un système Linux ou macOS qui utilise des fins de ligne LF, vous ne voulez pas que Git les convertisse automatiquement lorsque vous extrayez des fichiers ; Cependant, si un fichier avec des terminaisons CRLF est introduit accidentellement, vous voudrez peut-être que Git le corrige. Vous pouvez demander à Git de convertir CRLF en LF lors de la validation, mais pas l'inverse en définissant core.autocrlf sur input :</p>

```
git config --global core.autocrlf input
```
<p style="text-align:justify">Cette configuration devrait vous laisser des terminaisons CRLF dans les extractions Windows, mais des terminaisons LF sur les systèmes macOS et Linux et dans le référentiel.</p>

<p style="text-align:justify">Si vous êtes un programmeur Windows réalisant un projet Windows uniquement, vous pouvez désactiver cette fonctionnalité, en enregistrant les retours chariot dans le référentiel en définissant la valeur de configuration sur false</p>

```
git config --global core.autocrlf false
```
## Workflow du git 
<p style="text-align:justify">Voici un schema qui résume les commandes de git: </p>

![workflow git](images/workflow-git.png)

## Integration les branches git au projet

<p style="text-align:justify">L'architeture du project consiste a avoir 2 branche principale et ces 2 branches sont protégées <strong>(Protected)</strong>:</p>

- <p style="text-align:justify"><strong>master</strong> : cette branche contient un code de production</p>
- <p style="text-align:justify"><strong>développement</strong> : cette branche contient l'ensemble de part de code qui est en cours de développement.</p>

<p style="text-align:justify">Pour developper de nouvelles fonctionnalités, vous devez créer une nouvelle sous-branche a partir de la branche de développement. Lorsque vous avez fini de développer une nouvelle fonctionnalité et à la fin du sprint, vous fusionnez cette sous-branche avec la branche de développement.
Pour fixer un bug qui se trouve sur le serveur de production, vous devez créer une nouvelle sous-branche a partir de la branche de master. Lorsque vous avez fini de corriger le bogue, vous fusionnez cette sous-branche avec la branche principale et n'oubliez pas de fusionner la nouvelle branche principale avec la branche de développement afin d'avoir ce code corrigé dans tous les environnements.</p>

<p style="text-align:justify">Voici l'arborescence de git :</p>

![arborescence](images/arborescence_git.png)

## Création une nouvelle branche

Il existe plusieurs façons de créer une branche à partir de l'interface Web GitLab.

### Création une nouvelle branche à partir du tableau de bord d'un projet

1. <p style="text-align:justify">Sur la page des fichiers d'un projet, choisissez **New Branche** dans la liste déroulante.</p> ![ajouter une nouvelle branche](images/web_editor_new_branch_dropdown.png)
2. <p style="text-align:justify">Remplissez le formulaire</p>
   
   1. <p style="text-align:justify">Remplissez le champ <strong>nom de la branche</strong></p>
    
    !!! info "nommage des branches"
        Selon la convention pour nommer une sous-branche en utilisant ce format : <br>
        `<numero ticket>-<type de fonctionnalité>/<nom de la fonctionalité>`

   2. <p style="text-align:justify">Modifier le champs  <strong>Create from</strong> pour choisir de quelle branche, balise ou commit SHA cette nouvelle branche provient. Ce champ se complète automatiquement si vous commencez à saisir une branche ou une balise existante.</p>
3. <p style="text-align:justify">Cliquez sur  <strong>Create branch</strong> pour revenir au navigateur de fichiers sur cette nouvelle branche.</p>
![Le formulaire pour ajouter une branche](images/web_editor_new_branch_page.png)

## Configurer une branche protégée

Prérequis: 

- Il faut avoir au moins le rôle de mainteneur.


Pour protéger une branche :

1. <p style="text-align:justify">Accédez à votre projet et sélectionnez <strong>Settings > Repository</strong>.</p  >
2. <p style="text-align:justify">Développez <strong>Branches protégées</strong>.</p >
3. <p style="text-align:justify">Dans le menu <strong>déroulant Branche</strong>, sélectionnez la branche que vous souhaitez protéger.</p   >
4. <p style="text-align:justify">Dans la <strong>liste Autorisé à fusionner</strong>, sélectionnez un rôle ou un groupe qui peut fusionner dans cette branche.</p   >
5. <p style="text-align:justify">Dans la <strong>liste Autorisé à envoyer</strong>, sélectionnez un rôle, un groupe ou un utilisateur qui peut envoyer un envoi à cette branche. </p    >
6. <p style="text-align:justify">Sélectionnez Protéger.</p  >

