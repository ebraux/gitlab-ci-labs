# Projet laravel de base

## Introduction
<p style="text-align:justify">Tous les futurs projets laravel seront basés sur ce modèle de projet laravel. Il contient tous les fichiers requis pour démarrer un projet laravel à partir de zéro et se connecter à un pipeline CI/CD prédéfini qui testera et déploiera l'application automatiquement.</p>

## Arborescence du projet

```
php-laravel
├── docker-files
│   │   └── mysql
│   │       └── my.cnf
│   ├── nginx
│   │   └── conf.d
│   │       └──app.conf
│   ├── php
│   │   └── php.ini
├── .dockerignore
├── Dockerfile
├──docker-compose.yml
├──docker-entrypoint.sh
```

!!! abstract "Code"
    <p style="text-align:justify"><strong>my.cnf</strong>: Ficher de configuration de Mariab DB.</p>
        
            [mysqld]
            general_log = 1
            general_log_file = /var/lib/mysql/general.log
        

    <p style="text-align:justify"><strong>app.conf</strong>: Ficher de configuration de Nginx.</p>
    <pre style="background:hsla(0, 0%, 96%, 1);">
    
        server {
            listen 80;
            index index.php index.html;
            error_log  /var/log/nginx/error.log;
            access_log /var/log/nginx/access.log;
            root /var/www/public;
            location ~ \.php$ {
                try_files $uri =404;
                fastcgi_split_path_info ^(.+\.php)(/.+)$;
                fastcgi_pass app:9000;
                fastcgi_index index.php;
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_param PATH_INFO $fastcgi_path_info;
            }
            location / {
                try_files $uri $uri/ /index.php?$query_string;
                gzip_static on;
            }
        }
    
    </pre>

    <p style="text-align:justify"><strong>php.ini</strong>: Ficher de configuration de PHP.</p>
    <pre style="background:hsla(0, 0%, 96%, 1);">
        
        upload_max_filesize=40M
        post_max_size=40M
    
    </pre>
    <p style="text-align:justify"><strong>.dockerignore</strong>: Ficher exclut les fichiers et les répertoires qui correspondent aux modèles qu'il contient. Cela permet d'éviter d'envoyer inutilement des fichiers et des répertoires volumineux ou sensibles au démon et éventuellement de les ajouter aux images à l'aide de `ADD` ou `COPY`.</p>
    <pre style="background:hsla(0, 0%, 96%, 1);">

        docker-files
        docker-compose.yml
        vendor
    </pre>


<!-- !!! abstract "Code"
    <strong>docker-compose.yml</strong>
    <pre style="background:hsla(0, 0%, 96%, 1);">

    </pre> -->

`Dockerfile`: <p style="text-align:justify">Contient toutes les commandes pour construire une image de Laravel</p>

<!-- !!! abstract "Code"
    <strong>docker-compose.yml</strong>
    <pre style="background:hsla(0, 0%, 96%, 1);">

    </pre> -->

`docker-compose.yml`:<p style="text-align:justify"> </p>

<!-- !!! abstract "Code"
    <strong>docker-compose.yml</strong>
    <pre style="background:hsla(0, 0%, 96%, 1);">

    </pre> -->


`docker-entrypoint.sh`: <p style="text-align:justify"></p>

<!-- !!! abstract "Code"
    <strong>docker-compose.yml</strong>
    <pre style="background:hsla(0, 0%, 96%, 1);">

    </pre> -->