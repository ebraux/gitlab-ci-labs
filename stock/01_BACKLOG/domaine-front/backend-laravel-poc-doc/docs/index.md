# POC Application Laravel

POC mise en place chaine d'intégration Continue pour une application de Laravel

## Prérequis
 
- Avoir une connaissance en **Docker** et comment il fonctionne.
- Avoir une connaissance en **git** et comment il fonctionne.
- Avoir une connaissance en **Ansible** et comment il fonctionne.
- Avoir une connaissance en **Terraform** et comment il fonctionne.
- Avoir une connaissance en la méthodologie d'**agile** et **scrum**.
- Avoir une connaissance en la méthodologie de **devOps**.