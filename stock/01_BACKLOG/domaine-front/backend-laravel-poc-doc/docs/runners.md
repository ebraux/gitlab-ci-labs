# Les runners

!!! example ""
    <div style="line-height:2em;vertical-align:middle;color:#651fff;">
        <span style="font-size:1.5em">:material-test-tube:</span>
        <span style="font-size:1.1em">Version expérimentale</span>
    </div>

Gitlab runner, qui exécutera les commandes dans le fichier `.gitlab-ci.yml` que nous avons préparé, nous devons l'installer sur une machine, toutes les opérations seront exécutées par gitlab-runner, et les commandes qu'il exécutera seront écrites au fichier `.gitlab-ci.yml`

## Inventaire

[Installer Docker pour Windows 10](https://docs.docker.com/docker-for-windows/install/)

[Installer Git](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git)
## Installation (pour windows)

1. Créez un dossier quelque part dans votre système, ex. : C:\GitLab-Runner.
2. Téléchargez le binaire pour [64 bits](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe) ou [32 bits](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-386.exe) et placez-le dans le dossier que vous avez créé. Ce qui suit suppose que vous avez renommé le binaire en gitlab-runner.exe (facultatif).
3. Exécuter une invite de commande avec une règle d'administration

```bash
cd C:\GitLab-Runner
.\gitlab-runner.exe install
.\gitlab-runner.exe start
```

## Inscription des coureurs

L'enregistrement d'un runner est le processus qui lie le runner à une ou plusieurs instances GitLab.

Vous pouvez enregistrer plusieurs runners sur la même machine hôte, chacun avec une configuration différente, en répétant la commande **register**.

```bash
cd C:\GitLab-Runner
.\gitlab-runner.exe register
```

## Configuration
<ol>
  <li>Aller dans <strong>Paramètres > CI/CD</strong> et développez la section <strong>Coureurs</strong>, c'est ou il se trouve le URL et le token.</li>
  <li>Cliquer sur le boutton <strong>Reset register token</strong></li>
  <li>Accepter la rechargement de la page</li>
  <li>Utiliser le nouveau token</li>
</ol>

![configaration runner](images/runner_token_configuration.png)




### Exemple de la configuration token 



* Modifier le fichier "config.toml" :

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "gitlab-pc-runner"
  url = "https://gitlab.imt-atlantique.fr/"
  token = "4B3M_1qEM1ZmTfy1GG_-"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```

Pour que le concurant etre capable d'exécuter docker in docker
```
privileged = true
```

## Liste des coureurs 
On se place dans le dossier du runner
```
.\gitlab-runner.exe list
```

## Exécution

> [Windows]
En mode administrateur, on ouvre un powershell

On se place dans le dossier du runner

```
.\gitlab-runner.exe run
```

## Désinscrire un coureurs

<ol>
  <li>On se place dans le dossier du runner.</li>
  <li>On excecute <code>.\gitlab-runner.exe list</code> dans le but de voir la liste des coureurs.</li>
  <li>On choisit un coureur et on exécute la commande suivante .</li>
</ol>

```
.\gitlab-runner.exe unregister --name xxxxxxxxx
```

## Arrêt

On se place dans le dossier du runner
```
.\gitlab-runner.exe stop
```

## Log

On se place dans le dossier du runner
```
Get-WinEvent -ProviderName gitlab-runner
```

## Désinstallation
On se place dans le dossier du runner
```
.\gitlab-runner.exe uninstall

```


