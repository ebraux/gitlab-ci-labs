# Workflow d'intégration et de déploiement continu

Dans cette section, nous utiliserons Gitlab CI/CD pour créer notre code, puis le pousserons vers notre registre Docker.

GitLab CI/CD est un outil intégré à GitLab pour le développement de logiciels via les méthodologies continues :

- **Intégration continue (CI)** - *dans une future version*
- **Livraison continue (CD)**
- **Déploiement continu (CD)**  - *dans une future version*

## CI/CD pipeline

Les pipelines sont le composant de niveau supérieur de l'intégration, de la livraison et du déploiement continu.
Le pipeline comprend :

- **Jobs** qui définissent ce qu'il faut faire. Par exemple, des tâches qui compilent ou testent du code.
- **Stages** qui définissent quand exécuter les travaux. Par exemple, les étapes qui exécutent des tests après les étapes qui compilent le code.

Les jobs sont exécutées par les runners. Plusieurs tâches de la même étape sont exécutées en parallèle, s'il y a suffisamment d'exécuteurs simultanés.

Si tous les travaux d'une étape réussissent, le pipeline passe à l'étape suivante.

Si une tâche d'une étape échoue, l'étape suivante n'est pas (généralement) exécutée et le pipeline se termine plus tôt.

En général, les pipelines sont exécutés automatiquement et ne nécessitent aucune intervention une fois créés. Cependant, il y a aussi des moments où vous pouvez interagir manuellement avec un pipeline.

**Pour utiliser GitLab CI/CD :**

1. Assurer avoir des runnners disponibles pour exécuter vos tâches. S'il n'y a pas de runner, installer GitLab Runner et enregistrer un runner pour votre instance, projet ou groupe.
2. Créer un fichier `.gitlab-ci.yml` à la racine de votre référentiel. Ce fichier est l'endroit où vous définissez vos travaux CI/CD.

Lorsque le fichier est validé dans votre référentiel, le programme d'exécution exécute les tâches. Les résultats du travail sont affichés dans un pipeline.

## Voir les pipelines

Vous pouvez trouver les exécutions de pipeline actuelles et historiques sous la page **CI/CD >** Pipelines de votre projet. Vous pouvez également accéder aux pipelines pour une demande de fusion en accédant à son onglet **Pipelines**.
![tous les pipelines](images/pipelines_index_v13_0.png)

Cliquez sur un pipeline pour ouvrir la page **Détails du pipeline** et afficher les tâches qui ont été exécutées pour ce pipeline. À partir de là, vous pouvez annuler un pipeline en cours d'exécution, réessayer des tâches sur un pipeline en échec ou supprimer un pipeline.

## Gitlab runner

GitLab Runner est une application qui fonctionne avec GitLab CI/CD pour exécuter des tâches dans un pipeline.
Une fois qu'un runner est configuré et disponible pour votre projet, les travaux CI/CD peuvent utiliser le runneur.
Préciser le nom du runner ou ses tags dans votre fichier `.gitlab-ci.yml`. Ensuite, lorsque vous pousser du code dans votre référentiel, le pipeline s'exécute et l'exécuteur du programme d'exécution (runner) traite les commandes.

## Caches et artifacts

### Jobs Artifacts

Les travaux peuvent générer une archive de fichiers et de répertoires. Cette sortie est connue sous le nom d'artefact de travail.
Vous pouvez télécharger des artefacts de tâche à l'aide de l'interface utilisateur GitLab.

### Caches

Une cache est un ou plusieurs fichiers qu'un travail télécharge et enregistre. Les tâches suivantes qui utilisent le même cache n'ont pas besoin de télécharger à nouveau les fichiers, elles s'exécutent donc plus rapidement.

## Les stages

Jusqu'à présent, ce pipeline contient 4 étapes:

- **préparation**
- **configuration**
- **testing**
- **building**
![pipeline stages](images/pipeline_jobs.png)

```yaml
stages:
  - preparation
  - configuration
  - testing
  - building
```

### Environement d'execution

Ce pipeline est éxécuté avec un runneur docker en se basant à une image : `edbizarro/gitlab-ci-pipeline-php:fpm`.

### Préparation

Dans cette étape, le pipeline install les dépendences de Laravel ensuite il génère les fichiers nécéssaires `.env` et `.env.testing` tout en generant la clef pour l'application Laravel.
Comme artifacts cette étape publie le dossier `vendor/`, `.env` et `.env.testing`

```yaml
composer:
  stage: preparation
  tags:
    - ci
  script:
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts --no-suggest
    - cp .env.example .env
    - php artisan key:generate
    - cp .env .env.testing
  artifacts:
    paths:
      - vendor/
      - .env
      - .env.testing
    expire_in: 1 days
    when: always
  cache:
    paths:
      - vendor/
```

### Configuration

Dans cette étape, le pipeline modifie les fichiers `.env` et `env.testing` en injectant les variables globales de la CI dans le but de configurer la connexion entre la base de données et l'application Laravel.

```yaml
config:
  stage: configuration
  dependencies:
  - composer
  needs: ['composer']
  script:
    - chmod +x ci/ci-build.sh
    - ci/ci-build.sh
  artifacts:
    paths:
      - .env
      - .env.testing
```

### Testing

Cette étape est responsable de lancer les tests unitaires automatiquement en utilisant la librairie phpunit.

```yaml
test:
  stage: testing
  services:
    - mysql:5.7 
  dependencies:
    - composer
  needs: ['composer','config']
  tags:
    - ci
  variables:
    # MYSQL_DATABASE: ${CI_DB_TESTING_NAME}
    # MYSQL_ROOT_PASSWORD: ${CI_DB_TESTING_ROOT_PASSWORD}
    MYSQL_ALLOW_EMPTY_PASSWORD: 'yes'
    MYSQL_DATABASE: laravel
    # MYSQL_PASSWORD: ${CI_DB_TESTING_USER_PASSWORD}
    # MYSQL_USER: ${CI_DB_TESTING_USER}
    SERVICE_TAGS: dev
    SERVICE_NAME: mysql
  script:
    # - ./vendor/bin/security-checker security:check
    # - ./vendor/bin/phpcs --report=summary
    - env
    - php artisan migrate
    - php artisan migrate:refresh
    - ./vendor/phpunit/phpunit/phpunit --no-coverage
    # - php artisan code:analyse
  cache:
    paths:
      - vendor/
```

### Building

Cette étape est la dernière étape et si tous les tests sont passés, le pipeline declance le processus de la conteneurisation de l'application Laravel.
Ensuite il la publie dans le registre de conteneurs propre au projet.

```yaml
.build_docker_image:
  image: docker:20.10
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:20.10-dind
  before_script:

    # check variables
    - echo "CI_REGISTRY_IMAGE = $CI_REGISTRY_IMAGE"
    - echo "IMAGE_NAME = $IMAGE_NAME"
    # Create IMAGE Tag
    - export MY_IMAGE_REGISTRY=${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:latest
      # Connect to registry 
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    # check variable
    - echo "IMAGE_NAME = $IMAGE_NAME"
    - echo "MY_IMAGE_REGISTRY = $MY_IMAGE_REGISTRY"
    # build de l'image
    #   - copie du dockerfile et des ressources à la racine du projet
    #   - build de l'image
    - mv ./* .
    - docker build -t ${MY_IMAGE_REGISTRY}  .
    # push dans la registry 
    - docker push ${MY_IMAGE_REGISTRY}
        # Menage
    - docker rm --force $CI_PROJECT_NAME
    - docker logout


# ---

build:
  extends: .build_docker_image
  stage: building
  needs: ['test']
  tags:
    - ci
```

## Consulter les images générer

Avec le Docker Container Registry intégré à GitLab, chaque projet GitLab peut avoir son propre espace pour stocker ses images Docker.

### Afficher le registre de conteneurs

Pour afficher le Container Registry d'un projet ou d'un groupe:

1. Accédez à votre projet ou groupe.
2. Accédez à **Packages et registres > Container Registry**.

Vous pouvez rechercher, trier, filtrer et supprimer des conteneurs sur cette page. Vous pouvez partager une vue filtrée en copiant l'URL depuis votre navigateur.

Seuls les membres du projet ou du groupe peuvent accéder au registre de conteneurs d'un projet privé.

Si un projet est public, le Container Registry l'est aussi.

![conteneur images](images/container_images.png)
