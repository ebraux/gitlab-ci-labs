


You cannot delete a project runner that is assigned to more than one project. Before you can delete the runner, you must disable it in all projects where it is enabled.

