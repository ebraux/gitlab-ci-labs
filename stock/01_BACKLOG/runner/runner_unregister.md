

- Get your runner token and id
First, go to the GitLab settings page and find the token (e.g. 250cff81 in the image below) and the id (e.g. 354472 in the image below) of the GitLab runner which you wish to delete.


- Use the gitlab-runner CLI to unregister the runner
If you have access to the machine which was used to register the GitLab runner, you can unregister the runner using the following command, where you replace {TOKEN} with the token of your GitLab runner (e.g. 250cff81 in the example above).

gitlab-runner unregister --url https://gitlab.org/ --token {TOKEN}



