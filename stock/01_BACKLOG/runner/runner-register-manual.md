


Saisir les données :

- Enter your GitLab instance URL (also known as the gitlab-ci coordinator URL) : `https://gitlab.com/`
- Enter the token you obtained to register the runner : `J6AzHSAyr-o39sZsUFoz`
- Enter a description for the runner. You can change this value later in the GitLab user interface: `Dedicated to Lab-ci`.
- Enter the tags associated with the runner, separated by commas. You can change this value later in the GitLab user interface : `lab-ci`
- Provide the runner executor. For most use cases, enter docker : `docker`
- Enter the default Docker image (for example, ruby:2.6): `alpine:latest`

Vérifier dans la liste des runner dans l'interface de Gitlab :

![](img/specific-runner.png){: style="height:500px"}

- [https://docs.gitlab.com/runner/install/docker.html](https://docs.gitlab.com/runner/install/docker.html)
- [https://docs.gitlab.com/runner/register/index.html#docker](https://docs.gitlab.com/runner/register/index.html#docker)
