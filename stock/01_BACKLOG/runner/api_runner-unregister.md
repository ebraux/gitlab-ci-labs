
Use the GitLab API to unregister the runner

If you no longer have access to the machine which was used to register the runner, or if the runner is associated with multiple projects, you can use the following Python script.

Set RUNNER_ID to the id of your runner (e.g. 354472 in the example above) and GITLAB_AUTH_TOKEN to a GitLab token which you can generate from your profile page.

``` python
import os
import requests

GITLAB_AUTH_TOKEN = ...
RUNNER_ID = ...

headers = {"PRIVATE-TOKEN": GITLAB_AUTH_TOKEN}

r = requests.get(f"https://gitlab.com/api/v4/runners/{RUNNER_ID}", headers=headers)
runner_data = r.json()

for project in runner_data.get("projects", []):
    r = requests.delete(
        f"https://gitlab.com/api/v4/projects/{project['id']}/runners/{RUNNER_ID}",
        headers=headers,
    )
    if not r.ok:
        print("Encountered an error deleting runner from project:", r.json())

r = requests.delete(f"https://gitlab.com/api/v4/runners/{RUNNER_ID}", headers=headers)
if not r.ok:
    print("Encountered an error deleting runner:", r.json())
```



