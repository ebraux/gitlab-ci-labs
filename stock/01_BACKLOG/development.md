
## Tests en local du site

Build de l'image
```bash
docker build -t mkdocs_gitlab-ci-labs .
```

Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_gitlab-ci-labs mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_gitlab-ci-labs mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_gitlab-ci-labs rm -rf /work/site
docker image rm mkdocs_gitlab-ci-labs
```
Rem : Creation du site
```bash
docker run  -v ${PWD}/..:/work mkdocs_gitlab-ci-labs mkdocs new gitlab-ci-labs
sudo chown -R $(id -u -n):$(id -g -n)  *
```
