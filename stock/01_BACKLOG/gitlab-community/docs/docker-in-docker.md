# Utilisation de Docker In Docker (dind)


Docker in Docker permet d'utiliser des commandes Docker avec un runner Docker.

Ce mécanisme  permet d'éviter de données les droits d'accès au moteur Docker qui tourne sur la machine Hôte

Docker in Docker lance :

 - un container Docker "client",
 - un autre container en mode Daemon, avec lequel le client va interragir via l'API

Tout est géré sur ce container Daemon. Et une fois les traitements terminés, il est tout simplement détruit.



## Version de DinD et configuration de TLS : "Cannot connect to the Docker daemon"

A partir de la version 18.09+, docker:dind génére automatique des certificats TLS, et impose leur utilisation pour les connexions distantes. Si ces certificats ne sont pas utilisé, la connexion est impossible, et la chaine d CI renvoie le message `docker: Cannot connect to the Docker daemon at tcp://docker:2375. Is the docker daemon running?.`

Les certificats sont généré dans le dossier spécifié par la variable d'environnement `DOCKER_TLS_CERTDIR`.

Utiliser Docker Dinid, il faut donc soit :

- utiliser une version antérieur à la version 18.09, 
- utiliser une version >18.09, et desactiver la configuration TLS au niveau du container DinD
- utiliser une version >18.09, et configurer l'utilisation de TLS entre le client Docker et le container DinD

### Utilisation d'une version antérieure à "18.09"

```yaml
testdind:
  image: docker:18.09.7
  
  services:
  - docker:18.09.7-dind

  script:
  - docker info
```


### Utilisation de DinD sans TLS

Cette solution est utilisée par Gitlab dans ces template, et c'est celle quon retouve dans le exemples d'utilisation de DinD avec la CI de Gitlab.

Elle necessite de configurer des variables : 

- DOCKER_TLS_CERTDIR: ""
- DOCKER_DRIVER: overlay2 (optionel)
- DOCKER_HOST='tcp://docker:2375'  (optionel)

```yaml
testdind:
  image: docker:19.03.12
  
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  
  services:
  - docker:19.03.12-dind

  script:
  - docker info
```


Rem : Pour DOCKER_HOST,  'tcp://docker:2375' est la valeur par défaut. Elle peut-être définie soit au niveau de variables dans le fichier de CI, soit dans la configuration du runner  

```ini
[runners.docker]
  host = "tcp://docker:2375"
```

DOCKER_TLS_CERTDIR, doit être définie dans le fichier CI. La placer dans les variables "environment = ["DOCKER_TLS_CERTDIR="]" dans la configuration du runner ne fonctionne pas.



### Utilisation de DinD avec configuration de TLS

Modifier la configuration du runner, pour créer un volume partagé pour les certifictas :
```ini
volumes = ["/certs/client", "/cache"]
```

Et configurer le chemin des certificats dnas le fichier de CI

```yaml
DOCKER_TLS_CERTDIR: "/certs"
```

[https://about.gitlab.com/blog/2019/07/31/docker-in-docker-with-docker-19-dot-03/](https://about.gitlab.com/blog/2019/07/31/docker-in-docker-with-docker-19-dot-03/)


--- 

# Ressources 

Documentations :

- [https://about.gitlab.com/blog/2019/07/31/docker-in-docker-with-docker-19-dot-03/](https://about.gitlab.com/blog/2019/07/31/docker-in-docker-with-docker-19-dot-03/)
- [https://www.testcontainers.org/supported_docker_environment/continuous_integration/gitlab_ci/](https://www.testcontainers.org/supported_docker_environment/continuous_integration/gitlab_ci/)
- [https://hub.docker.com/_/docker/](https://hub.docker.com/_/docker/), notament la section "TLS"
- [https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1986](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1986)
- [https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-the-docker-image-docker-in-docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-the-docker-image-docker-in-docker)
- [https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4501](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4501)


Exemples :

- [https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml)

