
# De la bonne gestion de la rétention des artifacts

 

C'est important de fixer des dates d'expiration au niveau des artifacts, pour éviter d esur-carger le problème.

## Fixer des dates d'expiration

Permet configrer la durée de rétention des artifacts.

- Ne pas recréer les artifacts à chaque pipeline.
- Mais éviter de conserver des données non utilisées, sur des pipeline qui ne sont plus à jour.


## Possibilité de conserver le dernier artifact.

Interêt : pouvoir relancer des job partiellement.

- https://about.gitlab.com/releases/2020/09/22/gitlab-13-4-released/#lock-the-latest-job-artifact-to-prevent-deletion

