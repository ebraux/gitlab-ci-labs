

``` bash
$ eval $(ssh-agent -s)
Agent pid 2193
```

## sans clé

``` bash
$ ssh-add -l 
The agent has no identities.

$ ssh -T git@gitlab.imt-atlantique.fr
git@gitlab.imt-atlantique.fr: Permission denied (publickey).
```

## Avec la clé :

``` bash
$ ssh-add testgitlab 
Identity added: testgitlab (commentaire)

$ ssh-add -l 
256 SHA256:0h6G0Y3jvS/wcUWsOWzFKk9pj+eiVOq6BCyOIAsc/VQ commentaire (ED25519)

$ ssh -T git@gitlab.imt-atlantique.fr
Welcome to GitLab, @ebraux!

$ git clone git@gitlab.imt-atlantique.fr:contrats-poc-v2/app-ci-test.git
Clonage dans 'app-ci-test'...
remote: Enumerating objects: 1187, done.
remote: Total 1187 (delta 0), reused 0 (delta 0), pack-reused 1187
Réception d'objets: 100% (1187/1187), 144.46 Kio | 11.11 Mio/s, fait.
Résolution des deltas: 100% (685/685), fait.
```

* [https://docs.gitlab.com/ee/ssh/](https://docs.gitlab.com/ee/ssh/)