
# Portail d'information sur la plateforme Gitlab IMT Atlantique

La documentation générée est disponible sur le site [https://gitlab-community.gitlab-pages.imt-atlantique.fr](https://gitlab-community.gitlab-pages.imt-atlantique.fr)


## Tests en local

Build de l'image
```bash
docker build -t mkdocs_gitlab-community .
```

Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_gitlab-community mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_gitlab-community mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_gitlab-community rm -rf /work/site
```


