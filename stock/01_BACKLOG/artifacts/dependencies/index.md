
### Entre jobs dans un stage 

Reprendre le fichier de CI "Utilisation des artifacts avec plusieurs stages", et utiliser le mot clé `dependencies` *[https://docs.gitlab.com/ee/ci/yaml/#dependencies](https://docs.gitlab.com/ee/ci/yaml/#dependencies)*, pour ne charger que les artifacts du job du stage de build.


Observer le résultat dans le pipeline.


- Solution : [artifacts-dependencies_gitlab-ci.yml](artifacts-dependencies_gitlab-ci.yml)
