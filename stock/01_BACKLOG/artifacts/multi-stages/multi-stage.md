---
## Utilisation des artifacts avec plusieurs stages

- Créer des fichiers et les placer dans des artifacts, pour des stages `build` et `test`
- Créer un job qui affiche le contenu des fichiers d'Artifacts du stage de `build` pendant le stage de `test`
- Créer un job qui affiche le contenu des fichiers d'Artifacts des stages de `build` et `test` dans un stage `deploy`


- Solution : [artifacts-multiples_gitlab-ci.yml](artifacts-multiples_gitlab-ci.yml)


---

## Gestion des artifacts


### Entre jobs dans un stage 

Reprendre le fichier de CI "Utilisation des artifacts avec plusieurs stages", et dans le job qui affiche le contenu des fichiers d'Artifacts du stage de `build` pendant le stage de `test`, afficher le contenu des fichiers d'Artifacts du stage de `test`.

Observer le résultat dans le pipeline.

- Solution : [artifacts-jobs_gitlab-ci.yml](artifacts-jobs_gitlab-ci.yml)  
  
