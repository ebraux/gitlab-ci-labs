Gitlab permet de faire des miroir de dépôt mais :

- Gitlab.com : uniquement en mode push vers un autre dépôt, pas de pull (limite du free plan)
- Gitlab.com : ne permet pas de faire des access token de projet ou de groupe, mais uniquement personnel (limite du free plan)
- Gitlab : les deploy token permette uniquement la lecture, pas l'écriture d'un dépôt

La seule solution est donc d'utiliser un personal token, et de mettre en place un mirroir...

- https://@gitlab.com/odyssey-edito/turbiditymapping-4dvarnet/tutorial.git

