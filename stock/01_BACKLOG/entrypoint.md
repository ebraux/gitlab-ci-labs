

# config image


## Modifier le "entrypoint"

* [https://docs.gitlab.com/ce/ci/docker/using_docker_images.html#overriding-the-entrypoint-of-an-image](https://docs.gitlab.com/ce/ci/docker/using_docker_images.html#overriding-the-entrypoint-of-an-image)

### Exemple image phpunit :

Test d'affichage d'un message avec la commande "echo"
```bash
docker run phpunit/phpunit echo 'phpunit container can execute command' 
---
Cannot open file "echo.php".
```

Pourquoi ?
* dockerhub : [https://hub.docker.com/r/phpunit/phpunit](https://hub.docker.com/r/phpunit/phpunit)
* github : []

```bash
ENTRYPOINT ["/usr/local/bin/phpunit"]
CMD ["--help"]
```

Il faut donc changer l'entrypoint (ou utiliser une autre image)
```bash
docker run --entrypoint='/bin/sh' phpunit/phpunit echo 'phpunit container can execute command' 
---
Cannot open file "echo.php".
```


## Ressources

- [https://docs.gitlab.com/ee/ci/docker/using_docker_images.html](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)