# Factorisation

Utilisation de fonctions, d'héritage

## avec des "anchor" 

* [https://medium.com/@ju5t_/how-were-using-gitlab-to-test-and-deploy-our-application-s-to-docker-swarm-e3f556dbf8fe](https://medium.com/@ju5t_/how-were-using-gitlab-to-test-and-deploy-our-application-s-to-docker-swarm-e3f556dbf8fe)

Attention, les "anchor" sont limités. ils ne supportent pas par exemle l'utiisation avec de sfichiers d'include.
il vaut mieux utiliser des "extends"

## avec extends
https://docs.gitlab.com/ee/ci/yaml/#extends


## Utilisation d'extends dans des fichiers fichier d'include
https://docs.gitlab.com/ee/ci/yaml/#extends

