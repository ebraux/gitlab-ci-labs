# Utiliser des variables:

## Variables dans des variables

On peut utiliser des variables dans des variables, uniquement si ce sont des constantes

```
  APP_ENVIRONMENT: 'inte'
  APP_MAJOR_VERSION: 1
  APP_MINOR_VERSION: 1
  STACK_NAME: contrats_${CI_PROJECT_NAME}
  SWARM_MANIFEST: ${DEPLOIEMENT_LOCAL_PATH}/contrats_${CI_PROJECT_NAME}.yml
  #SWARM_MANIFEST: ${DEPLOIEMENT_LOCAL_PATH}/${STACK_NAME}

```

## Concaténation de variables

Attention, la concaténation des variables ne marche pas avec des "-", il faut utiliser des "_"

si `CI_PROJECT_NAME` contient `app-infra`

```bash
STACK_NAME: contrats-${CI_PROJECT_NAME}
```
donne le contenu `contrats-${CI_PROJECT_NAME}`

```bash
STACK_NAME: contrats_${CI_PROJECT_NAME}
```
donne le contenu `contrats_app-infra`

