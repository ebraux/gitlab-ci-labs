# Utilisation des Docker-secrets


# Principe

- Ne peuvent être utilisés que sur un cluster Swarm
- une fois créé, on ne peut pas voir le contenu
- on ne peut pas modifier un secret existant, mais on peu fiare une rotation
- on peut associer des labels à un secret

## Test effectués sur serveur d'inte :

1. Récupération des fichiers de config dans /usr/local/tomcat/conf : config.xml, server.xml et tomcat-users.xml
2. Ajout d'une entête d'idnetification à chacune des fichier, indiquant le secret , et l'environnement. Ex :
```xml
<!--
    secret: contrats-tomcat-tomcat-users.xml
    environnement:  inte
-->
```
3. Création des secret
```bash
docker secret create contrats-tomcat-context.xml context.xml-inte
docker secret create contrats-tomcat-server.xml server.xml-inte
docker secret create contrats-tomcat-tomcat-users.xml tomcat-users.xml.inte
```
4. Modification du manifest pour intégrer les secrets
```yaml
services:
...
app:
    image: gitlab-registry.imt-atlantique.fr/contrats-poc-v2/deploiement/app-infratest:1.1.inte
    ...
    secrets:
    - source: contrats-tomcat-context.xml
        target: /usr/local/tomcat/conf/context.xml
        mode: 0400
    ...
secrets:
    contrats-tomcat-context.xml:
        external: true
    ...
```
5. Relance de la stack
```bash
docker stack deploy -c contrats_app-infratest.yml  --with-registry-auth contrats_app-infratest
```
6. Vérification : entête des fichiers déployés
```bash
docker stack ps contrats_app-infratest
ssh cloud-user@192.168.1.17
docker ps
docker exec <CONTAINER ID> cat /usr/local/tomcat/conf/context.xml
```

Autres commandes utilisées :

- Génération des secrets, et lancement de la stack
```bash
docker secret create contrats-tomcat-context.xml context.xml-inte
docker secret create contrats-tomcat-server.xml server.xml-inte
docker secret create contrats-tomcat-tomcat-users.xml tomcat-users.xml.inte

docker stack deploy -c contrats_app-infratest.yml  --with-registry-auth contrats_app-infratest
```
- Suppression de la stack et des secrets
```bash
docker stack rm  contrats_app-infratest

docker secret rm contrats-tomcat-context.xml
docker secret rm contrats-tomcat-server.xml
docker secret rm contrats-tomcat-tomcat-users.xml
```

## Exemple donné par Warren

```yaml
version: '3.5'
services:

  server:
    image: nginx:1.13.7
    secrets:
      - source: nginx-config
        target: /etc/nginx/conf.d/default.conf
        mode: 0400

secrets:
  nginx-config:
    external: true
```


# Doc sur la gestion des secrets 

* Création
```bash
printf super-secret | docker secret create my_secret -
---
9l7q9vayxyez4m8ks8ewh4zls
```

* List
```bash
docker secret ls
---
ID                          NAME        DRIVER    CREATED          UPDATED
9l7q9vayxyez4m8ks8ewh4zls   my_secret             51 seconds ago   51 seconds ago
```

* Visualisation : pas le contenu
```bash
docker secret inspect 9l7q9vayxyez4m8ks8ewh4zls
---
[
    {
        "ID": "9l7q9vayxyez4m8ks8ewh4zls",
        "Version": {
            "Index": 2815669
        },
        "CreatedAt": "2021-02-16T15:27:28.261317284Z",
        "UpdatedAt": "2021-02-16T15:27:28.261317284Z",
        "Spec": {
            "Name": "my_secret",
            "Labels": {}
        }
    }
]

```
* Modification : Impossible
```bash
printf super-secret2 | docker secret create my_secret -
Error response from daemon: rpc error: code = AlreadyExists desc = secret my_secret already exists
```
* suppression 
```bash
docker secret rm  my_secret
```

* Rotation des secrets : https://docs.docker.com/engine/swarm/secrets/#example-rotate-a-secret
```bash
$ docker service update \
     --secret-rm mysql_password mysql

$ docker service update \
     --secret-add source=mysql_password,target=old_mysql_password \
     --secret-add source=mysql_password_v2,target=mysql_password \
     mysql
```

# Différents modes de création

A partir d'une variable

A partir d'un fichier
```bash
docker secret create my_secret ./secret.json
```

---
ref :

- https://docs.docker.com/engine/swarm/secrets/
- https://docs.docker.com/engine/reference/commandline/secret/
- https://docs.docker.com/engine/reference/commandline/secret_create/
  