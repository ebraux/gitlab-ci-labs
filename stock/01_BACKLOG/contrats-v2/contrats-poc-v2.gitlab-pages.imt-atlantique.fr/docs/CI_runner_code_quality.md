# Runner spécifique pour éxécuter les jobs d'analyse de code

## Description

Actuellement "gitlab.imt-atlantique.fr" n'a pas accés au serveur sonar qui a une IP privée 10.29.244.102 avec un port à 9000.
Afin de pouvoir exécuter les jobs code_quality et sonar_test, j'ai dû installer un runner sur ma machine avec le VPN IMT activé.

## Inventaire

Docker desktop for windows

Git

Gitlab Runner 10

## Installation

En mode administrateur :

* [Installer Docker pour Windows 10](https://docs.docker.com/docker-for-windows/install/)

* [Installer Git](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git)

## Configuration

> [Gitlab]

* Créer un nouveau runner 

![img_runner_specifique.png](images/img_runner_specifique.png)


> [Windows]

* [Intaller "Gitlab Runner" pour windows](https://docs.gitlab.com/runner/install/windows.html)

* Modifier le fichier "config.toml" :

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "codequality"
  url = "https://gitlab.imt-atlantique.fr/"
  token = "KbmEJ3S85LPgH_sMEEhy"
  executor = "docker"
  builds_dir = "/tmp/builds"
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/.m2", "/var/run/docker.sock:/var/run/docker.sock", "/tmp/builds:/tmp/builds"]
    shm_size = 0
```

name : nom du tag sur gitlab

token : générer sur gitlab à la création


## Exécution

En mode administrateur, on ouvre un powershell

> [Windows]

On se place dans le dossier du runner

```
.\gitlab-runner.exe start
```

## Arrêt

```
.\gitlab-runner.exe stop
```

## Log

```
Get-WinEvent -ProviderName gitlab-runner
```

## Désinstallation

```
.\gitlab-runner.exe uninstall

```