# Workflow d'intégration et de déploiement continu 

###<a id="inventaire">Inventaire</a>

L'IDE utilisé par le SI contrat est Intellij IDEA

Le logiciel de gestion de versions utilisé est GIT

Les branches principales sont "develop" et "master", elles doivent être considérées comme protégées

Le workflow de développement utilisé se base sur Gitflow (réf : [Gitflow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow))

Les applications du SI contrat se base sur des technos java/javascript/html-Css/maven/tomcat

Dans chaque projet des fichiers de configurations liés à l'intégration continue sont injectés via le projet ci-templates-contrats. 
Il s'agit de templates utilisés par les différents jobs de gitlab-ci, ils permettent une meilleure lisibilité du fichier de configuration gitlab-ci.yml.
Ils sont également réutilisables par les différents projets du SI contrats.

Arborescence du projet ci-templates-contrats :

```
ci-templates-contrats
build_docker_image.yml
build_maven.yml
compile_test.yml
merge_request.yml
sonar_maven.yml
swarm_deployment.yml
unit_test.yml
README.md
```

Arborescence d'un projet type du SI contrat :

```
project-name
├── conf
│   ├── inte
│   │   └── swarm.env
│   ├── prod
│   │   └── swarm.env
│   ├── rec
│   │   └── swarm.env
├── src
├── web
.codeclimate.yml
merge_request.md
.gitlab-ci.yml
.gitignore
pom.xml
docker-compose.yml
Dockerfile
MakeFile
README.md
```
###<a id="diag-ci1">Processus workflow CI</a>

Les processus en bleu ciel s'exécutent automatiquement (le coeur de la CI) 

![img_diag_workflow1.png](images/img_diag_workflow1.png)

###<a id="diag-ci2">Processus workflow CI hotfix</a>

Les processus en bleu ciel s'exécutent automatiquement (le coeur de la CI)

![img_diag_workflow2.png](images/img_diag_workflow2.png)

###<a id="gitlab-ci">gitlab-ci.yml</a>

```yml
cache:
  paths:
    - ./.m2/repository

variables:
  MAVEN_OPTS: "-Dmaven.repo.local=./.m2/repository -Dhttp.proxyHost=$CI_PROXY_HOST -Dhttp.proxyPort=$CI_PROXY_PORT -Dhttps.proxyHost=$CI_PROXY_HOST -Dhttps.proxyPort=$CI_PROXY_PORT -Dhttp.nonProxyHosts=CI_PROXY_NO"
  STACK_NAME: contrats-${CI_PROJECT_NAME}
  SWARM_MANIFEST: ${DEPLOIEMENT_LOCAL_PATH}/contrats_${CI_PROJECT_NAME}.yml
  SWARM_CONFIG_PATH: ${DEPLOIEMENT_LOCAL_PATH}/contrats_${CI_PROJECT_NAME}
  TIMEOUT_SECONDS: 1

include:
  - project: '$CI_PROJECT_NAMESPACE/ci-templates-contrats'
    ref: 'master'
    file: 'build_docker_image.yml'
  - project: '$CI_PROJECT_NAMESPACE/ci-templates-contrats'
    ref: 'master'
    file: 'swarm_deployment.yml'
  - project: '$CI_PROJECT_NAMESPACE/ci-templates-contrats'
    ref: 'master'
    file: 'unit_test.yml'
  - project: '$CI_PROJECT_NAMESPACE/ci-templates-contrats'
    ref: 'master'
    file: 'compile_test.yml'
  - project: '$CI_PROJECT_NAMESPACE/ci-templates-contrats'
    ref: 'master'
    file: 'build_maven.yml'
  - project: '$CI_PROJECT_NAMESPACE/ci-templates-contrats'
    ref: 'master'
    file: 'merge_request.yml'
  - project: '$CI_PROJECT_NAMESPACE/ci-templates-contrats'
    ref: 'master'
    file: 'sonar_maven.yml'
  - project: '$CI_PROJECT_NAMESPACE/ci-templates-contrats'
    ref: 'master'
    file: 'check_tag_master.yml'
  - template: Code-Quality.gitlab-ci.yml

stages:
  - merge-request
  - test
  - junit
  - sonar
  - report
  - compile
  - check-tag
  - build
  - image
  - majbdd
  - deploy

create:merge-request:
  extends: .create:merge-request
  tags:
    - contrats
    - build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME =~ /feature/'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME =~ /release/'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME =~ /hotfix/'
      when: on_success
    - when: never
  before_script:
    - (echo "${CI_COMMIT_REF_NAME}" | grep -Eq  ^.*feature.*$) && export TARGET_BRANCH="develop" || export TARGET_BRANCH="master"

code_quality:
  tags:
    - codequality
  variables:
    REPORT_FORMAT: html
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"'
      when: on_success
    - when: never
  allow_failure: true
  artifacts:
    expire_in: 1 week
    expose_as: 'Code Quality Report'
    paths: [ gl-code-quality-report.html ]

unit_tests:
  extends: .unit_test
  stage: junit
  tags:
    - contrats
    - build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /release/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /hotfix/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: on_success
    - when: never

sonar_tests:
  extends: .sonar_maven
  stage: sonar
  tags:
    - codequality
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"'
      when: on_success
    - when: never
  allow_failure: true

pages:
  stage: report
  dependencies:
    - unit_tests
    - code_quality
  tags:
    - contrats
    - build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"'
      when: always
    - when: never
  allow_failure: true
  script:
    - mkdir public
    - mv target/surefire-reports/* public/
  artifacts:
    paths:
      - public

compile:
  extends: .compile_test
  stage: compile
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /release/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /hotfix/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: on_success
    - when: never
  tags:
    - contrats
    - build

check_tag_master:
  extends: .check_tag_master
  stage: check-tag
  tags:
    - contrats
    - build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^rec-[vV]/'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^hot-[vV]/'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^prod-[vV]\d.\d.\d/'
      when: on_success
    - when: never

build_maven:
  extends: .build_maven
  stage: build
  tags:
    - contrats
    - build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == "develop"'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^rec-[vV]/'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^hot-[vV]/'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^prod-[vV]\d.\d.\d/'
      when: on_success
    - when: never

image_docker:
  extends: .build_docker_image
  stage: image
  tags:
    - contrats
    - build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == "develop"'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^rec-[vV]/'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^hot-[vV]/'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^prod-[vV]\d.\d.\d/'
      when: on_success
    - when: never
  variables:
    IMAGE_TAG: ${DEPLOIEMENT_REGISTRY}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME}

maj_bdd:
  stage: majbdd
  tags:
    - contrats
    - build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^rec-[vV]/'
      when: manual
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^prod-[vV]\d.\d.\d/'
      when: manual
    - when: never
  script:
    - echo "update bdd contrat ?"

deploy_inte:
  extends: .swarm_deployment
  stage: deploy
  tags:
    - contrats
    - deploy
    - inte
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == "develop"'
      when: on_success
    - when: never
  dependencies:
    - image_docker
  variables:
    APP_ENVIRONMENT: 'inte'
    APP_IMAGE: ${DEPLOIEMENT_REGISTRY}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME}

deploy_rec:
  extends: .swarm_deployment
  stage: deploy
  tags:
    - contrats
    - deploy
    - rec
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^rec-[vV]/'
      when: on_success
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^hot-[vV]/'
      when: on_success
    - when: never
  dependencies:
    - maj_bdd
    - image_docker
  variables:
    APP_ENVIRONMENT: 'rec'
    APP_IMAGE: ${DEPLOIEMENT_REGISTRY}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME}

deploy_prod:
  extends: .swarm_deployment
  stage: deploy
  tags:
    - contrats
    - deploy
    - prod
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^prod-[vV]\d.\d.\d/'
      when: manual
    - when: never
  dependencies:
    - maj_bdd
    - image_docker
  variables:
    APP_ENVIRONMENT: 'prod'
    APP_IMAGE: ${DEPLOIEMENT_REGISTRY}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME}
```


###<a id="merge_request">Merge request</a>

Une merge-request est une proposition de modification de code. 
Un ou plusieurs développeurs peuvent alors décider d'accepter ou de refuser cette modification.

> [Intellij]

Le fichier merge_request.md est à modifier avant chaque création de branches, il permet de :

  * Saisir une description
  * Choisir le type de branche
  * Saisir le n° de l'issue

> [Gitlab]

Des tests se lancent :

  * test -> code_quality
  * unit_tests
  * sonar_tests
  * reports  
  * compile

![img_merge_request.png](images/img_merge_request.png)

Avant la fermeture d'une merge request, plusieurs options sont proposées:

  * Faire une revue de code avec un tiers
  * Attendre l'approbation du manager pour merger
  * Merger seulement quand tous les jobs sont ok

###<a id="version">Versioning</a>

Le format de version appliqué : {env}-[vV]x.x.x 

env :

  * rec
  * prod

x.x.x : {major}.{minor}.{patch}

Les livraisons sur les environnements de recette et production ne se feront que par la création de **tags** à partir de master.

Exemple : rec-v1.0.5

###<a id="tags">Tag</a>

Exemple de création d'un tag à partir de la branche master pour une livraison sur l'environnement de recette

```git
git checkout master
git tag -a rec-v1.0.5 -m "livraison issue<n°issue> sur la recette ddmmyyyhhmm"
git push origin rec-v1.0.5
```

###<a id="feature">Création d'une fonctionnalité</a>

> [Gitlab]

Créer une issue sur gitlab (liée au projet).

> [Intellij]

A partir de la branche develop on créé une branche de feature (**initialement develop provient de master**)

```git
git checkout develop
git checkout -b <n°issue>-feature/<intitule feature>
```
  * Modifier le fichier merge_request.md (lié à l'issue dans gitlab)
  * Coder
  * Réaliser des tests en local
  * Si les tests sont concluants on commit sur la branche "n°issue-feature/intitule feature" et on pousse sur le repo distant

```git
git commit <n°issue>-feature/<intitule feature>
git push origin <n°issue>-feature/<intitule feature>
```

> [Gitlab]

Une merge request de "n°issue-feature/intitule feature" into develop est automatiquement créée :

![img.png](images/img.png)

  * Des tâches sont lancées pour :
    * Tester la qualité du code
    * Tester unitairement des fonctionnalités
    * Tester la compilation et la non régression sur la branche develop
    * Générer des rapports

![img_merge_request.png](images/img_merge_request.png)
  
 > [IDE Intellij]

  * Les tests ne sont **pas concluants** :
    * Faire évoluer le code sur la branche "n°issue-feature/intitule feature"
    * Commiter les changements
    * Pousser sur le repo distant
    * Les tests autos de la merge request sont relancés

> [Gitlab]    

  * Les tests sont concluants :
    * Fermer la merge request
    * Le code est mergé sur la branche develop

###<a id="depfeature">Déploiement d'une fonctionnalité</a>

> [Gitlab]

Un pipeline d'intégration se lance

![img_pipeline_inte.png](images/img_pipeline_inte.png)

Il est à présent possible de tester l'application sur l'environnement d'intégration

 > [IDE Intellij]

  * Les tests ne sont **pas concluants** :
    * on reprend la phase **création d'une feature à partir de develop**

```git
git chekout develop
git checkout -b <n°issue>-feature/<intitule feature>
``` 
> [Gitlab]

  * les tests sont concluants:
    * une release va pouvoir être créée.

###<a id="release">Création d'une release</a>

> [IDE Intellij]

A partir de la branche develop on créé une branche de release

```git
git checkout develop
git checkout -b <n°issue>-release/<intitulé de la release>
```

  * On a toujours la possibilité de modifier le code si on constate des bugs ou si le besoin métier a évolué.
  * Si le code a bougé, il faudra bien sûr reporter ces changements sur la branche develop.
  * Une fois le code terminé:
    * on commit les changements
    * on pousse sur le repo distant

```git
git commit <n°issue>-release/<intitulé de la release>
git push origin <n°issue>-release/<intitulé de la release>
```

> [Gitlab]

Une merge request de "n°issue-release/intitulé de la release" into master est automatiquement créée :

![img.png](images/img.png)

  * Des tâches sont lancées pour
    * Tester la qualité du code
    * Tester unitairement des fonctionnalités
    * Tester la compilation et la non régression sur la branche develop
    * Générer des rapports

> [IDE Intellij]

  * Les tests ne sont **pas concluants** :
    * Faire évoluer le code sur la branche "n°issue-release/intitulé de la release"
    * Commiter les changements
    * Pousser sur le repo distant
    * Les tests autos de la merge request sont relancés

> [GITLAB]

  * Les tests sont concluants :
    * Fermer la merge request
    * Le code est automatiquement mergé sur la branche master

###<a id="deprelease">Déploiement d'une release</a>

> [IDE Intellij]

A partir de master on crée un tag pour livrer sur l'environnement de recette et on le pousse sur le repo distant

```git
git checkout master
git tag -a rec-vx.x.x -m "livraion issue<n°issue> sur la recette ddmmyyyhhmm"
git push origin rec-vx.x.x
```

> [GITLAB]

Un pipeline de recette se lance

![img_pipeline_recette.png](images/img_pipeline_recette.png)

_**l'étape maj_bdd nécessite une action manuelle**_

Il est à présent possible pour le métier de tester l'application sur l'environnement de recette

> [IDE Intellij]

  * Les tests ne sont **pas concluants** :
    * on reprend la phase **création d'une release à partir du tag rec-vx.x.x**

```git
git chekout rec-vx.x.x
git checkout -b <n°issue>-release/<intitulé de la release>
```

> [GITLAB]

  * Les tests sont **concluants** :
    * une **MEP** va pouvoir être programmée.

###<a id="depproduction">Mise en production</a>

> [IDE Intellij]

A partir de master on crée un tag pour livrer sur l'environnement de production et on le pousse sur le repo distant

```git
git chekout master
git tag -a prod-vx.x.x -m "MEP vx.x.x ddmmyyyhhmm "
git push origin prod-vx.x.x
```

> [GITLAB]

Un pipeline de production se lance

![img_pipeline_prod.png](images/img_pipeline_prod.png)

_**les étapes maj_bdd et deploy_prod nécessitent des actions manuelles**_

###<a id="hotfix">Création d'un hotfix</a>

> [Gitlab]

Créer une issue sur gitlab (liée au projet).

> [IDE Intellij]

A partir du dernier tag prod-vx.x.x on créé une branche de hotfix.

```git
git checkout prod-vx.x.x
git checkout -b <n°issue>-hotfix/<intitulé hotfix>
```
  * Corriger le bug
  * Modifier le fichier merge_request.md  
  * une fois le code terminé:
    * Commiter les changements
    * Pousser sur le repo distant

```git
git commit <n°issue>-hotfix/<intitulé hotfix>
git push origin <n°issue>-hotfix/<intitulé hotfix>
```

> [GITLAB]

Une merge request de "n°issue-hotfix/intitulé hotfix" into master est automatiquement créée :

  ![img.png](images/img.png)

  * Des tâches sont lancées pour
    * Tester la qualité du code
    * Tester unitairement des fonctionnalités
    * Tester la compilation et la non régression sur la branche develop
    * Générer des rapports
  
> [IDE Intellij]

  * Les tests ne sont **pas concluants** :
    * Faire évoluer le code sur la branche "n°issue-hotfix/intitulé hotfix"
    * Commiter les changements
    * Pousser sur le repo distant
    * Les tests auto de la merge request sont relancés

> [GITLAB]

  * Les tests sont concluants :
    * Fermer la merge request
    * Le code est mergé sur la branche master
    * Merger sur la branche develop également

###<a id="dephotfix">Déploiement d'un hotfix</a>

> [IDE Intellij]

A partir de master on crée un tag avec incrémentation mineure de la version pour livrer sur l'environnement de production et on le pousse sur le repo distant

```git
git chekout master
git tag -a prod-vx.x.x -m "MEP vx.x.x ddmmyyyhhmm "
git push origin prod-vx.x.x
```

> [GITLAB]

Un pipeline de production se lance

![img_pipeline_prod.png](images/img_pipeline_prod.png)

_**les étapes maj_bdd et deploy_prod nécessitent des actions manuelles**_
