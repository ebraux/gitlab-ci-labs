# Sonarqube

[Fonctionnalités](https://fr.wikipedia.org/wiki/SonarQube#Fonctionnalit%C3%A9s)

## Serveurs

### Machine

Machine ubuntu 18.04 disponible, avec Docker pré-installé : IP publique (pour acces SSH) : 10.29.244.102 IP privée
(pour accès entre les machines) : 192.168.1.13 

commande pour accès ssh : ssh ubuntu@10.29.244.102

### Base de données

PostgreSQL 10.15

### Sonarqube

SonarQube 8.6.1.40680

## Client

SonarScanner for Maven 3.8  2021-01-13

dans le pom.xml :

```maven
<dependency>
    <groupId>org.sonarsource.scanner.maven</groupId>
    <artifactId>sonar-maven-plugin</artifactId>
    <version>3.8.0.2131</version>
</dependency>
```

## Installation

[PostgreSQL](https://www.postgresql.org/docs/10/index.html)

[Sonarqube](https://docs.sonarqube.org/latest/setup/install-server/)

**_TODO_** [Configuration apache / Proxy sonarqube](https://docs.sonarqube.org/latest/setup/operate-server/)

## Gitlab

Configuration par projet : 

[Documentation pour integrer Gitlab](https://docs.sonarqube.org/latest/analysis/gitlab-integration/)

> [Gitlab]

[Ajouter une connexion à Sonarqube avec 1 token OAuth provider](https://gitlab.imt-atlantique.fr/-/profile/applications)

> [Sonarqube]

[Administrer l'intégration de Gitlab](http://10.29.244.102:9000/admin/settings?category=almintegration)

## Utilisation

> [Intellij]

Lancer la commande:

La clé "login" est générée via le profil du user gitlab -> settings -> Applications ->  Add

```shell
mvn package sonar:sonar -Dsonar.projectKey=$CI_PROJECT_NAME -Dsonar.host.url=$SONAR_URL -Dsonar.login=$SONAR_LOGIN_TOKEN
```

> [Gitlab]

Lancement de l'analyse lors d'une merge request

Ajout d'un job :

```yml
sonar_tests:
extends: .sonar_maven
stage: sonar-test
tags:
- contrats
- build
rules:
- if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
when: on_success
- when: never
allow_failure: true
```
