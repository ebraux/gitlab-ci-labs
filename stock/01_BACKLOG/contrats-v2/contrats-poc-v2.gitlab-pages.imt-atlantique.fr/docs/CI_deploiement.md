# Mise en place du déploiement multi-environnements


## Principe 

Le build de l'image est indépendant de l'environnement. Il n'y a donc qu'une seule phase de build.

Le deployement lui est spécifique à chaque environnement

L'image est personnalisée par rapport à l'environnement dans lequel elle s'execute en utilisant le secrets Docker pour définir les fichiers de confiuration, propres àa chaque environnement.

La personnalisation de l'image consiste à adapter :

- l'environnement d'execution de Tomcat
    - fichier : /usr/local/tomcat/conf/server.xml
    - secret mis en place : contrats-tomcat-server.xml
- le contexte d'execution applicatif global (gestion de sessions, datasource, ...) : 
    - fichier : /usr/local/tomcat/conf/context.xml 
    - secret mis en place : contrats-tomcat-context.xml
- la configuration de l'acces aux outils d'admin de tomcat
    - fichier : /usr/local/tomcat/conf/tomcat-users.xml
    - secret mis en place : contrats-tomcat-tomcat-users.xml
 
Le choix de l'environnement, don du cluster swarm dans lequel déployer l'application est fait en fonction des "tags", qui permettent de selectionner le runner qui executera les jobs. Il y a un gitlab-runner shell par environnement.


## Job "Build"

Besoin : à partir de l'image tomcat image reférence, builder une image incorporant l'application à déployer.

Mode opératoire : 

1. chargement des variables contenues dans le fichier `ci/conf/app.env`
   1. BASE_IMAGE : image de base à utiliser, par exemple `gitlab-registry.imt-atlantique.fr/contrats-poc-v2/deploiement/tomcat-contrats:1.2`
2. création d'un Dockerfile qui réalise les actions suivantes :
   1. chargement de l'image de base tomcat définie pour cet environnement
   2. intégration dans la configuration de tomcat des buildés précedement (fichiers *.war) et stockés dans l'artifact "target"
3. build de l'image à patir du Dockerfile, des war, et des fichiers de configuration spécifiques

Les images sont taguées avec une version:  date du build, version majeur/mineure, ...

```yaml
variable:
  - IMAGE_TAG: ${DEPLOIEMENT_REGISTRY}/${CI_PROJECT_NAME}:${APP_MAJOR_VERSION}.${APP_MINOR_VERSION}

include:
  - 'ci/templates/build_docker_image.yml'
  
build-image:
  extends: .build_docker_image
  stage: build
  tags:
    - contrats
    - build
```

## Job "Déploiement swarm"

1. chargement des variables contenues dans le fichier `ci/conf/${APP_ENVIRONMENT}/swarm.env`
    - APP_PORT : port de publication d el'application
    - APP_REPLICATS et APP_MAX_REPLICAT_PER_NODE : définition des réplicats à déployer
    - APP_MEMORY_RESERVATION : gestion de la mémoire pour l'application
2. création des fichiers pour le déploiement swarm : les fichiers créés sont stockés sur le serveur swarm manager au cas où il faille intervenir manuellement sur la stack ( dossier ${DEPLOIEMENT_LOCAL_PATH}/${CI_PROJECT_NAME} )
    - configuration du stack : docker-compose.yml, à partir de la template contrat-app-swarm-docker-compose.yml
    - de gestion du stack : Makefile, à partir de la template contrat-app-swarm-makefile
3. Déploiement de la stack dans l'environemment


Pour utiliser le deploiement swarm :

```yaml
include:
  - 'ci/templates/swarm_deployment.yml'

deploy:inte:
  extends: .swarm_deployment
  stage: deploy
  tags:
    - contrats
    - deploy
    - inte
  variables:
    APP_ENVIRONMENT: 'inte'
    APP_IMAGE: ${DEPLOIEMENT_REGISTRY}/${CI_PROJECT_NAME}:${APP_MAJOR_VERSION}.${APP_MINOR_VERSION}
```

## Mise en oeuvre

Création d'un dossier "ci", contennat 2 sous dossiers "conf" et "templates"

Dans conf, un fichier `app.env` contenant les variables de l'application : 
```
vim  ci/conf/app.env
---
BASE_IMAGE=gitlab-registry.imt-atlantique.fr/contrats-poc-v2/deploiement/tomcat-contrats:1.2
```
Ansi qu'un dossier pas environnement contenant dles infos spécifques à chaque environnement. Pour l'instant uniquement les cacartéristiques de déploiment du stack dans swarm
 par environnement, 

```
vim  ci/conf/inte/swarm.env
---
APP_PORT=8081
APP_REPLICATS=2
APP_MAX_REPLICAT_PER_NODE=2
APP_MEMORY_RESERVATION=512M
```

Dans template 

- les fichiers de template de CI
- les fihciers de template de configuration


Arobrescence :

```
├── ci
│   ├── conf
│   │   ├── app.env
│   │   ├── inte
│   │   │   └── swarm.env
│   │   ├── prod
│   │   │   └── swarm.env
│   │   └── rec
│   │       └── swarm.env
│   └── templates
│       ├── build_docker_image.yml
│       ├── contrat-app-Dockerfile
│       ├── contrat-app-swarm-docker-compose.yml
│       ├── contrat-app-swarm-makefile
│       └── swarm_deployment.yml
```

