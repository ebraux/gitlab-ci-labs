
# Les environnements

## récapitulatif des environnements

- Production (PROD),
- préprod (REC)
- environnement d'intégration (INTE).

Des environements de dev pour chaque développeur sur leurs postes.


## Accès à une application déployée

Les clusters Swarm sont configurés pour accepter des requétes sur les ports entre 8000 et 9000, sur les IP suivantes :

- Prod : 10.29.244.180
- rec : 10.29.244.148
- inte : 10.29.244.6

Donc par exemple pour l'appli "app-infratest", déclarée sur le port 8081 ans le fichier "app.env", qui déploie le

- Prod : http://10.29.244.180:8081/infratest
- rec :  http://10.29.244.148:8081/infratest
- inte : http://10.29.244.6:8081/appinfratest


## accès SSH 

Connection : il faut passer par une machine "Bastion" :

``` bash
ssh -A cloud-user@10.29.244.104
ssh cloud-user@192.168.1.15
```

Liste des IP

- Bastion
    - 10.29.244.104   bastion
- Swarm manager
    - 192.168.1.15    Swarm manager INTE
    - 192.168.1.30    Swarm manager REC
    - 192.168.1.26    Swarm manager PROD
- Swarm worker 
    - 192.168.1.17    Swarm worker INTE #1
    - 192.168.1.12    Swarm worker INTE #2
    - 192.168.1.6     Swarm worker REC #1
    - 192.168.1.7     Swarm worker REC #2
    - 192.168.1.5     Swarm worker PROD #1
    - 192.168.1.22    Swarm worker PROD #2

- Runner de Build
    - 192.168.1.8     Runner de Build
- Service IP
    - 10.29.244.6     Service IP INTE
    - 10.29.244.148   Service IP REC
    - 10.29.244.180   Service IP PROD


