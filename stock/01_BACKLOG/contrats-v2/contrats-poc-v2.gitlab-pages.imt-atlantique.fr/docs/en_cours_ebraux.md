





# -----------
Chargement des variables:

```bash
export $(cat .env) > /dev/null 2>&1;
```

Lancement du stack

```bash
docker stack deploy --with-registry-auth -c docker-compose.yml contrats-app-infratest
```

---

* [https://medium.com/@ju5t_/how-were-using-gitlab-to-test-and-deploy-our-application-s-to-docker-swarm-e3f556dbf8fe]

* https://dockerswarm.rocks/gitlab-ci/
* https://clouddocs.web.cern.ch/containers/tutorials/swarmgitlab.html


Utilisation de variables :
https://stackoverflow.com/questions/44694640/docker-swarm-with-image-versions-externalized-to-env-file

https://github.com/moby/moby/issues/29133


exemple K8s
script:
    - mv ${HELM_VALUES_FILE} ${HELM_VALUES_FILE}-tmp
    - envsubst '${CI_COMMIT_REF_SLUG}' < ${HELM_VALUES_FILE}-tmp > ${HELM_VALUES_FILE}
    - helm upgrade -i ${HELM_RELEASE_NAME} ${HELM_CHART_PATH} -f ${HELM_VALUES_FILE} --namespace ${HELM_NAMESPACE} --set-strimage.tag=${HELM_IMAGE_TAG}


deploy k8s prod:
  extends: .deploy k8s
  before_script:
    - export HELM_VALUES_FILE="<helm_chart_path>/values.prod.yaml"
    - export HELM_RELEASE_NAME="<app_name>-production"
    - export HELM_CHART_PATH="<helm_chart_path>"
    - export HELM_NAMESPACE="<app_name>-production"
    - export HELM_IMAGE_TAG="${CI_COMMIT_SHORT_SHA}"

COPY target/*.war $CATALINA_HOME/webapps/
COPY conf/*.xml $CATALINA_HOME/conf/


https://www.padok.fr/en/blog/test-environments-gitlab-kubernetes


erreur avec USER $NB_UID
https://stackoverflow.com/questions/10683349/forcing-bash-to-expand-variables-in-a-string-loaded-from-a-file
https://www.xspdf.com/help/52616270.html
https://github.com/docker-library/docs/issues/496



Erreur docker registry : https://gitlab.com/gitlab-org/gitlab-runner/-/issues/2247

docker image prune --force
