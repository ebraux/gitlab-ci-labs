# Git pour intellij et en lignes de commandes

## Installation

> [Windows]

[Git for windows](https://gitforwindows.org/)


## Nouveau projet sur gitlab

> [Gitlab]

Créer un nouveau project dans gitlab disi sous le groupe contrats

## Intellij

### Initialiser

* Ouvrir le projet à mettre sous git
* Onglet VCS -> VCS operations popup -> Enable version control integration
* Choisir Git + Ok
* Si message « Project configurations files can be added to git » -> Always add
* Sélectionner les fichiers à ajouter au contrôle de version

### Commiter en local

* Ouvrir la fenêtre "Commit"
* Sélectionner les fichiers à ajouter
* Saisir le message de commit  
* Cliquer sur commit

### Push sur gitlab

* cliquer sur la branche (en bas à droite de l'écran de l'IDE)
* choisir la branche et cliquer sur "push"
* si pas d'url de remote définie, cliquer sur "origin" et choisir "define remote"

### Checkout

* cliquer sur la branche (en bas à droite de l'écran de l'IDE)
* choisir la branche et cliquer sur "checkout"

### Merger

* faire un checkout de la branche sur la laquelle on veut merger
* onglet git -> choisir merge

### Updater un projet depuis une branche distante

* onglet git -> choisir update project


## Lignes de commandes

### Initialiser

```
git config --global user.name -> "John Doe"
git config --global user.email -> jd@example.com
```

### Nouveau projet

```
git clone https://gitlab-disi.imt-atlantique.fr/contrats/<new project>.git
cd <new project>
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### Projet existant non versionné sur gitlab disi

```
cd existing_folder
git init
git remote add origin https://gitlab-disi.imt-atlantique.fr/contrats/<name project>.git
git add .
git commit -m "Initial commit"
git push -u origin master
```


### Projet existant déjà versionné sous git en local sur gitlab disi

```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab-disi.imt-atlantique.fr/contrats/<name project>.git
git push -u origin --all
git push -u origin --tags
```