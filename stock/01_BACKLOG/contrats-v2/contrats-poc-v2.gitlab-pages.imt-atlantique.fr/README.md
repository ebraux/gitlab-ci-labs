
# Documentation POC Contrats

La documentation générée est disponible sur le site [https://contrats-poc-v2.gitlab-pages.imt-atlantique.fr/](https://contrats-poc-v2.gitlab-pages.imt-atlantique.fr/)

## Tests en local du site

Build de l'image
```bash
docker build -t mkdocs_contrats-poc .
```

Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_contrats-poc mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_contrats-poc mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_contrats-poc rm -rf /work/site
docker image rm mkdocs_contrats-poc
```

