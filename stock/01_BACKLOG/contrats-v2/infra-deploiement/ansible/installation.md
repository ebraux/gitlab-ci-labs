# Installation


## Installer les rôles

``` bash
ansible-galaxy install -r requirements.yml
```

## Config de la machine bastion

``` bash
ansible-playbook playbooks/user-imta-clean.yml --user ubuntu --limit bastion
ansible-playbook playbooks/user-cloud.yml --user ubuntu --limit bastion
ansible-playbook playbooks/secure-bastion.yml --limit bastion
```

