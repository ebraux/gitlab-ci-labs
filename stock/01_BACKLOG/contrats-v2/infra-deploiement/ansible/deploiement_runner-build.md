# Déploiement d'un runner de build


Après avoir initialisé et configuré les noeuds concernés

Vérification du nom du cluster à deployer/configurer  dans l'inventory :
``` bash
ansible-inventory --graph
```

Création de la variable d'environnement pour la cible
``` bash
export ANSIBLE_TARGET=<GROUPE>
```

Déploiment du runner de build
``` bash
ansible-playbook playbooks/gitlab-runner-docker-build.yml --limit $ANSIBLE_TARGET
```

Configuration des operations (ménage des images des docker runner, ...)
``` bash
ansible-playbook playbooks/gitlab-set-operations.yml --limit $ANSIBLE_TARGET
```

