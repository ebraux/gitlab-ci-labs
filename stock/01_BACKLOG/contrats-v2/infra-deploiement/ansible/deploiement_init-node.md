
# Préparation d'un noeud

## Objectif 

- Configuration de la VM  (création de l'utilisateur de service, ...)
- Ménage des installation automatisées des systèmes

> Le playbook de ménage peut poser problème si l'id de l'utilisateur de service corresond à un id déjà présent sur le système. Il est donc préférable d ela lancer aavnt le playbook de compte d eservice, et donc avec un compte système pré-existant.

--- 

## Définition des ou de la cible

Affichage des déclarations dans l'inventory
``` bash
ansible-inventory --graph
```

Création de la variable d'environnement pour la cible
``` bash
export ANSIBLE_TARGET=<HOST ou GROUPE>
```


## 
## Dans Vsphere 

- Utlisateur par défaut : root
- authentification : par mot de passe

``` bash
ansible-playbook playbooks/user-imta-clean.yml --user root -k --limit $ANSIBLE_TARGET
ansible-playbook playbooks/user-service.yml    --user root -k --limit $ANSIBLE_TARGET
```
---

## Dans Openstack

- Utlisateur par défaut : ubuntu
- authentification : par clé, définie lors du déploiement

``` bash
ansible-playbook playbooks/user-imta-clean.yml --user ubuntu --limit $ANSIBLE_TARGET
ansible-playbook playbooks/user-service.yml    --user ubuntu --limit $ANSIBLE_TARGET
```

##  Vérification

test de connexion avec l'utilisateur défini pour Ansible, sans mot de passe :
``` bash
ansible $ANSIBLE_TARGET -m shell -a 'free -h'
```

