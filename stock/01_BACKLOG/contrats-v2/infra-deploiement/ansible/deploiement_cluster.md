

# Déploiement / configuration d'un cluster


## Principe

- Préparation des neouds du cluster
- Déclaration dans l'inventory Ansible
- Déploiment/Configuration des noeuds


## Préparation des noeuds du cluster

Voir [deploiment.md](deploiment.md)


---

## Déclaration dans l'inventory Ansible

### Déclaration des machines du cluster dans l'inventor (inventory/hosts)

- Créer les groupes "swarm_manager_XXX"  et "swarm_worker_XXX" Utilisée par le rôle "jtyr.ansible_docker_swarm" pour déployer les clusters
- Créer le groupe  "swarm_cluster_XXX", qui rassemble ces 2 groupes
- Déclarer des Hosts avec un alias lisible, et définir leur adresse IP

Exemple de déclaration
```ini
[swarm_cluster_prod:children]
swarm_manager_XXX
swarm_worker_XXX

[swarm_manager_prod]
contrats-tomcat-01 ansible_host=10.29.XXX.XXX

[swarm_worker_prod]
contrats-tomcat-02 ansible_host=10.29.XXX.XXX
contrats-tomcat-03 ansible_host=10.29.XXX.XXX
```

- Associer les groupes créés au groupes de gestion globale "swarm_node" et "swarm_manager"


```ini
[swarm_node:children]
swarm_cluster_XXX

[swarm_manager:children]
swarm_manager_XXX
```

### Configuration du cluster (inventory/group_vars)

Créer le fichier de groupe de variables spécifique au cluster "swarm_cluster_XXX.yml", et y définir :

- la configuration spécifique du cluster Swarm
- la configuration du runner Gitlab de DEPLOY

Exemple :  

```yaml
# -------------------------------------
#  _____      ____ _ _ __ _ __ ___  
# / __\ \ /\ / / _` | '__| '_ ` _ \ 
# \__ \\ V  V / (_| | |  | | | | | |
# |___/ \_/\_/ \__,_|_|  |_| |_| |_|
# -------------------------------------
#
# Group name for all nodes in the cluster (managers + workers)
docker_swarm_group: swarm_cluster_prod

# Group name for manager nodes
docker_swarm_group_managers: swarm_manager_XXX

# Group name for worker nodes
docker_swarm_group_workers: swarm_worker_XXX

# Default node network interface through which the nodes in the cluster communicate
docker_swarm_node_iface: ens160

docker_swarm_node_labels:
    # Sets label cluster=test_cluster to all nodes
    cluster: contrats_cluster_XXX


#--------------------------------------
#  ____ _ _   _       _       ____  _____ ____  _     _____   __
# / ___(_) |_| | __ _| |__   |  _ \| ____|  _ \| |   / _ \ \ / /
#| |  _| | __| |/ _` | '_ \  | | | |  _| | |_) | |  | | | \ V / 
#| |_| | | |_| | (_| | |_) | | |_| | |___|  __/| |__| |_| || |  
# \____|_|\__|_|\__,_|_.__/  |____/|_____|_|   |_____\___/ |_|  
# ____                              
#|  _ \ _   _ _ __  _ __   ___ _ __ 
#| |_) | | | | '_ \| '_ \ / _ \ '__|
#|  _ <| |_| | | | | | | |  __/ |   
#|_| \_\\__,_|_| |_|_| |_|\___|_|   
#--------------------------------------
gitlab_runner_concurrent: 1
gitlab_runner_runners:
  - name: 'Shell Runner PROD swarm Management'
    # token is an optional override to the global gitlab_runner_registration_token
    #token: 'abcd'
    # url is an optional override to the global gitlab_runner_coordinator_url
    #url: 'https://my-own-gitlab.mydomain.com'
    executor: shell
    tags:
      - deploy
      - shell
      - contrats
      - XXX
    run_untagged: false
```

---

## Déploiment/Configuration des noeuds 

### Intitialsation du déploiement 

Après avoir initialisé et configuré les noeuds concernés

Vérification du nom du cluster à deployer/configurer  dans l'inventory :
``` bash
ansible-inventory --graph
```

Création de la variable d'environnement pour la cible
``` bash
export ANSIBLE_TARGET=<GROUPE>
```

###  Préparation des noeuds

Mise en place du proxy IMT Atlantique
``` bash
ansible-playbook playbooks/imta-proxy.yml --limit $ANSIBLE_TARGET
``` 

Mise à jour du système
``` bash
ansible-playbook playbooks/upgrade-system.yml --limit $ANSIBLE_TARGET
``` 

Installation des prérequis
``` bash
ansible-playbook playbooks/install-prerequis.yml --limit $ANSIBLE_TARGET
``` 

###  Déploiement du cluster Swarm

Déploiement du cluster Swarm
``` bash
ansible-playbook playbooks/swarm-deploy-cluster.yml --limit $ANSIBLE_TARGET
```

Configuration de l'authentification sur la registry de deploiement
``` bash
ansible-playbook playbooks/swarm-private-registry-auth.yml --limit $ANSIBLE_TARGET
```

Configuration des operations (retention de container par Swarm, ménage des images, ...)
``` bash
ansible-playbook playbooks/swarm-set-operations.yml  --limit $ANSIBLE_TARGET
```

### Déploiement du runner de deploiement

``` bash
ansible-playbook playbooks/gitlab-runner-shell-deploy.yml --limit $ANSIBLE_TARGET
```
