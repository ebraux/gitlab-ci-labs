
# Maintenance

## voir le status des clusters

``` bash
ansible-playbook playbooks/swarm-status.yml
```


## Modifier la configuration des operations

Des clsuets Swarm
``` bash
ansible-playbook playbooks/swarm-set-operations.yml  --limit $ANSIBLE_TARGET
```

Des runner gitlab
``` bash
ansible-playbook playbooks/gitlab-set-operations.yml --limit $ANSIBLE_TARGET
```


## Opérations manuelles

Connexion à "bastion", avec suivi de l'agent SSH
``` bash
ssh -A cloud-user@10.29.244.xxx
``` 

Connexion à un des noeuds un fois sur le "bastion"
``` bash
ssh  cloud-user@xxx.xxx.xxx.xxx
``` 

visualiser la liste des noeuds gérés
``` bash
ansible-inventory --graph
```

Obtenir des infortions sur un noued
``` bash
ansible-inventory --host contrats-poc-inte-2 
```

Vérifier l'espace disque sur tous les noeuds
``` bash
ansible poc_node -m shell -a 'df -h'
```

