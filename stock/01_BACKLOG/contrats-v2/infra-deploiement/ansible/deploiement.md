

# Déploiement / configuration

## Principe

Pour modifier la configuration, on lance à nouveau le déploiement


### Déploiement d'un cluster

Groupe(s) Ansible pour la getsion des cluster Swarm :

- des groupes globaux 
  - swarm_node : toutes les machine appartenant à un cluster swarm
  - swarm_manager : toutes les machines ayant le rôle "Manager" dans un des cluster Swarm
- des groupes spécifiques à chaque cluster
  - swarm_manager_XXX : les machines ayant le rôle "Manager" dans le cluster Swarm XXX
  - swarm_worker_XXX : les machines ayant le rôle "Worker" dans le cluster Swarm XXX
  - swarm_cluster_XXX : toutes les machines ayant appartenant au cluster Swarm XXX

Fichier(s) de variables :

``` bash
├── group_vars
│   ├── all.yaml : Les variables communes à tous les cluster
│   └── swarm_cluster_XXX.yml : Les variables spécifiques au cluster, donc la config du DEPLOY runner
└── host_vars
    └── Aucune
```

Après préparation des noeud [deploiement_init-node.md](deploiement_init-node.yml), suivre la procédure [deploiement_cluster.md](deploiement_cluster.md)

### Deploiment d'un Runner de deploiment

Les runner de déploiment sont déployés uniquemant sur les "Manager Swarm". Leur configuration est donc intégrée à celle des "Manager Swarm".

Après préparation des noeud [deploiement_init-node.md](deploiement_init-node.yml), suivre la procédure [deploiement_cluster.md](deploiement_cluster.md)

### Deploiment d'un Runner de Build

- Groupe Ansible pour la gestion des runner de build :
  - groupe global: gitlab_runner_build

Fichier(s) de variables :

``` bash
├── group_vars
│   ├── all.yaml : Les informations communes aux runners (build et deploy) : connection à Gitlab, ...
│   ├── gitlab_runner_build.yml : configuration des runner de build
└── host_vars
    └── Aucune
```

Après préparation des noeud [deploiement_init-node.md](deploiement_init-node.yml), suivre la procédure [deploiement_runner-build.md](deploiement_runner-build.md)


