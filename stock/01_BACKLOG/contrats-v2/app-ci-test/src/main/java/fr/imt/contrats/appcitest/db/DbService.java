package fr.imt.contrats.appcitest.db;

import javax.naming.NamingException;

public class DbService {

    public static DBAccess getDbAccess(boolean junit) throws NamingException {
        DBAccess db = new DBAccess();
        if(junit){
            db.getConnectionTest();
        } else {
            db.getConnection();
        }
        return db;
    }
}
