package fr.imt.contrats.appcitest.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * classe filtre resources css, images, js ....
 */
public class ResourcesFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        if (response instanceof HttpServletResponse) {
            HttpServletResponse alteredResponse = ((HttpServletResponse) response);
            alteredResponse.addHeader("Cache-Control", "max-age=604800");
            alteredResponse.addHeader("X-XSS-Protection", "1; mode=block");
            alteredResponse.addHeader("X-Content-Type-Options", "nosniff");
            alteredResponse.addHeader("X-Frame-Options", "SAMEORIGIN");
        }

        filterChain.doFilter(request, response);
    }
}
