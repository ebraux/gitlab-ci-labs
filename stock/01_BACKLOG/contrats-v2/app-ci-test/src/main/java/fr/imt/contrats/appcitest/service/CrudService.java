package fr.imt.contrats.appcitest.service;

import fr.imt.contrats.appcitest.beans.Person;

import java.util.List;

public interface CrudService {

    public void addPerson(boolean junit, String name, String lastName, Integer age, String city) throws Exception;
    public void deletePerson(boolean junit, Integer id) throws Exception;
    public Person getPerson(boolean junit, Integer id) throws Exception;
    public List<Person> getPersons(boolean junit) throws Exception;
}
