package fr.imt.contrats.appcitest.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;

public class DBAccess implements Serializable {

    // Récupération de notre logger.
    private static final Logger LOGGER =  LogManager.getLogger( DBAccess.class );

    private DataSource dataSource = null;
    public Connection con = null;
    private Statement st = null;
    private boolean bErreur=false;
    private String sErreur=null;


    public void getConnection(String strCon)
    {
        try
        {
            dataSource=DataSourceContrat.getInstance(strCon);
            if (dataSource==null)LOGGER.error("DataSource non trouvé : "+strCon);
            con=dataSource.getConnection();
            if (con==null)LOGGER.error("Connexion non établi : "+strCon);
        }
        catch (Exception e)
        {
            LOGGER.error("dba access : " + e);
        }//catch
    }

    /**
     * datasource pour test junit
     */
    public void getConnectionTest()  {
        try{
            dataSource = DataSourceContrat.getInstanceTest();
            con=dataSource.getConnection();
            if (con==null)LOGGER.error("Connexion test non établi ");

        }
        catch (Exception e)
        {
            LOGGER.error("dba access : " + e);
        }//catch
    }

    /**
     * methode utilisé pour se connecter à la base
     */
    public void getConnection(){

        try
        {
            getConnection("DBTest");
        }
        catch (Exception e)
        {
            LOGGER.error("dba access : " + e);
            //   e.printStackTrace();

        }//catch
    }//getConnection




    public void closeConnection()
    {
        try{
  /*
             // Tentative de fermeture du Resulset
         if (resultSet != null){
            resultSet.close();
         }
*/
            // Tentative de fermeture du Statement
            if (st != null){
                st.close();
            }
/*
         // Tentative de fermeture du CallableStatement
         if (procedure != null){
            procedure.close();
         }
*/
            // Tentative de fermeture de la connexion
            // (on la rend au pool)
            if (con != null){
                con.close();
            }

            //con.close();
        }
        catch (Exception e)
        {
            LOGGER.error("dba access : " + e);
            //   e.printStackTrace();

        }//catch
    }

    /**
     * identique à closeconnection
     */
    public void releaseConnection()
    {
        this.closeConnection();
    }
}
