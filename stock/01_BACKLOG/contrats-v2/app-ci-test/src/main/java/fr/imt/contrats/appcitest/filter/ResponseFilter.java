package fr.imt.contrats.appcitest.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * classe filtre servlet et jsp
 */
public class ResponseFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        if (response instanceof HttpServletResponse) {
            HttpServletResponse alteredResponse = ((HttpServletResponse) response);
            alteredResponse.addHeader("Cache-Control", "no-cache,no-store,must-revalidate");
            alteredResponse.addHeader("Pragma", "no-cache");
            alteredResponse.setDateHeader("Expires", 0);
            alteredResponse.addHeader("X-XSS-Protection", "1; mode=block");
            alteredResponse.addHeader("X-Content-Type-Options", "nosniff");
            alteredResponse.addHeader("X-Frame-Options", "SAMEORIGIN");
            alteredResponse.addHeader("Access-Control-Max-Age", "1");
        }

        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
