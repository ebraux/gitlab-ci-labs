package fr.imt.contrats.appcitest.servlet;

import fr.imt.contrats.appcitest.beans.Person;
import fr.imt.contrats.appcitest.service.CrudService;
import fr.imt.contrats.appcitest.service.CrudServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/insert","/list","/get","/delete"})
public class CrudServlet extends HttpServlet {
    // Récupération de notre logger.
    private static final Logger LOGGER =  LogManager.getLogger( CrudServlet.class );

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String operation = req.getRequestURI().substring(req.getRequestURI().lastIndexOf("/") + 1);
        String name =  req.getParameter("name");
        String lastname = (String) req.getParameter("lastname");
        Integer age = null;
        if(StringUtils.isNotBlank(req.getParameter("age"))){
            age = Integer.parseInt(req.getParameter("age"));
        }
        String city = req.getParameter("city");
        Integer id = null;
        if(StringUtils.isNotBlank(req.getParameter("id"))){
            id = Integer.parseInt(req.getParameter("id"));
        }
        CrudService service = new CrudServiceImpl();
        List<Person> persons = null;
        Person person = null;
        try{
        switch(operation){
            case "insert" :
                service.addPerson(false, name, lastname, age, city);
                persons = service.getPersons(false);
                req.setAttribute("persons", persons);
                req.setAttribute("id", id);
                break;
            case "list" :
                persons = service.getPersons(false);
                req.setAttribute("persons", persons);
            default:
                break;
            case "get" :
                person = service.getPerson(false, id);
                persons = service.getPersons(false);
                req.setAttribute("person", person);
                req.setAttribute("persons", persons);
                break;
            case "delete" :
                service.deletePerson(false, id);
                persons = service.getPersons(false);
                req.setAttribute("persons", persons);
                req.setAttribute("id", id);
                break;
        }
        this.getServletContext().getRequestDispatcher("/test.jsp").forward(req, resp);
        } catch (Exception e) {
            LOGGER.error(e);
            resp.sendRedirect("/error.jsp " + e.getMessage());
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        doPost(req, resp);
    }
}
