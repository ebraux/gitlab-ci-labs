package fr.imt.contrats.appcitest.db;

import fr.imt.contrats.appcitest.util.PropertyLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sqlite.SQLiteDataSource;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.util.Properties;

public class DataSourceContrat {

    // Récupération de notre logger.
    private static final Logger LOGGER =  LogManager.getLogger( DataSourceContrat.class );

    private static SQLiteDataSource dataSourceTest;

    private static DataSource dataSource;

    private DataSourceContrat(){

    }

    public static DataSource getInstance(String strCon){
        if(dataSource == null){
            dataSource = initDataSource(strCon);
        }
        return dataSource;
    }

    public static DataSource initDataSource(String strCon){
        Context ctx;
        try {
            ctx = new InitialContext();
            dataSource=(DataSource) ctx.lookup("java:comp/env/jdbc/"+strCon);
        } catch (Exception e) {
            LOGGER.error("initDataSource " + e.getMessage());
        }
       return dataSource;
    }

    public static SQLiteDataSource getInstanceTest(){
        if(dataSourceTest == null){
            dataSourceTest = initDataSourceTest();
        }
        return dataSourceTest;
    }

    public static SQLiteDataSource initDataSourceTest(){
        SQLiteDataSource dataSource = null;
        try {
            Properties props = PropertyLoader.load("src/test/resources/bdd.properties");
            dataSource = new SQLiteDataSource();
            dataSource.setUrl(props.getProperty("url"));
        } catch (Exception e) {
            LOGGER.error("initDataSource test " + e.getMessage());
        }
        return dataSource;
    }


}