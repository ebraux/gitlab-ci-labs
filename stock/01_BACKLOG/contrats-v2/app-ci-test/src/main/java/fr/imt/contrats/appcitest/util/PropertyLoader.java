package fr.imt.contrats.appcitest.util;

import fr.imt.contrats.appcitest.db.DataSourceContrat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {

    private static final Logger LOGGER =  LogManager.getLogger( PropertyLoader.class );
    /**
     * Charge la liste des propriétés contenu dans le fichier spécifié
     *
     * @param filename le fichier contenant les propriétés
     * @return un objet Properties contenant les propriétés du fichier
     */
    public static Properties load(String filename)  {
        Properties properties = new Properties();
        FileInputStream input = null;
        try {
            input = new FileInputStream(filename);
            properties.load(input);
        } catch (Exception e) {
            LOGGER.error("properties " + e.getMessage());
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                LOGGER.error("file properties " + e.getMessage());
            }
        }
        return properties;
    }
}
