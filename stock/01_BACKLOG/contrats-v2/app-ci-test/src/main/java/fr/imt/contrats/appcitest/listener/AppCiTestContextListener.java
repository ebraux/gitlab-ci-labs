package fr.imt.contrats.appcitest.listener;

import fr.imt.contrats.appcitest.db.DBAccess;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class AppCiTestContextListener implements ServletContextListener {
    /**
     * {@inheritDoc}
     *
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed(final ServletContextEvent event) {
        // Rien de spécifique à faire, les objets etant désaloués automatiquement
    }

    /**
     * {@inheritDoc}
     *
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextInitialized(final ServletContextEvent event) {
        String sql = "CREATE TABLE IF NOT EXISTS person (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text,\n"
                + "	lastname text,\n"
                + "	age integer, \n"
                + "	city text\n"
                + ");";

        DBAccess db = new DBAccess();
        db.getConnection();
        try {
            DatabaseMetaData meta = db.con.getMetaData();
            System.out.println("The driver name is " + meta.getDriverName());
            System.out.println("A new database has been created.");
            Statement stmt = db.con.createStatement();
            stmt.execute(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            db.releaseConnection();
        }
    }
}
