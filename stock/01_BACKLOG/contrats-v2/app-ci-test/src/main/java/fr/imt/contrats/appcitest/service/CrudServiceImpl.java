package fr.imt.contrats.appcitest.service;

import fr.imt.contrats.appcitest.beans.Person;
import fr.imt.contrats.appcitest.db.DBAccess;
import fr.imt.contrats.appcitest.db.DbService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CrudServiceImpl implements CrudService{
    @Override
    public void addPerson(boolean junit, String name, String lastName, Integer age, String city) throws Exception {
        String sqlPerson = "INSERT INTO person(name, lastname, age, city) VALUES(?,?,?,?)";
        ResultSet rs = null;
        Connection conn = null;
        PreparedStatement pstmt1 = null;
        DBAccess db = null;
        Integer id = null;
        try{
            db = DbService.getDbAccess(junit);
            conn = db.con;
            conn.setAutoCommit(false);
            pstmt1 = conn.prepareStatement(sqlPerson);
            pstmt1.setString(1, name);
            pstmt1.setString(2, lastName);
            pstmt1.setInt(3, age);
            pstmt1.setString(4,city);
            pstmt1.executeUpdate();
            conn.commit();
        } catch (SQLException e1) {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException e2) {
                System.out.println(e2.getMessage());
            }
            System.out.println(e1.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt1 != null) {
                    pstmt1.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e3) {
                System.out.println(e3.getMessage());
            } finally {
                db.releaseConnection();
            }
        }
    }

    @Override
    public void deletePerson(boolean junit, Integer id) throws Exception {
        String sqlPerson = "DELETE FROM person where id = " + id;
        ResultSet rs = null;
        Connection conn = null;
        Statement stmt1 = null;
        DBAccess db = null;
        try{
            db = DbService.getDbAccess(junit);
            conn = db.con;
            conn.setAutoCommit(false);
            stmt1 = conn.createStatement();
            stmt1.executeUpdate(sqlPerson);
            conn.commit();
        } catch (SQLException e1) {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException e2) {
                System.out.println(e2.getMessage());
            }
            System.out.println(e1.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt1 != null) {
                    stmt1.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e3) {
                System.out.println(e3.getMessage());
            } finally {
                db.releaseConnection();
            }
        }


    }

    @Override
    public Person getPerson(boolean junit, Integer id) throws Exception {
        String sqlPerson = "SELECT * FROM person where id =" + id;
        ResultSet rs = null;
        Connection conn = null;
        Statement stmt1 = null;
        DBAccess db = null;
        Person person = new Person();
        try{
            db = DbService.getDbAccess(junit);
            conn = db.con;
            stmt1 = conn.createStatement();
            rs = stmt1.executeQuery(sqlPerson);
            // loop through the result set
            while (rs.next()) {
                person.setIdPerson(rs.getInt("id"));
                person.setName(rs.getString("name"));
                person.setLastName(rs.getString("lastname"));
                person.setAge(rs.getInt("age"));
                person.setCity(rs.getString("city"));
            }

        } catch (SQLException e1) {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException e2) {
                System.out.println(e2.getMessage());
            }
            System.out.println(e1.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt1 != null) {
                    stmt1.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e3) {
                System.out.println(e3.getMessage());
            } finally {
                db.releaseConnection();
            }
        }
        return person;
    }

    @Override
    public List<Person> getPersons(boolean junit) throws Exception {
        String sqlPerson = "SELECT * FROM person";
        ResultSet rs = null;
        Connection conn = null;
        Statement stmt1 = null;
        DBAccess db = null;

        List<Person> persons = new ArrayList<>();
        try{
            db = DbService.getDbAccess(junit);
            conn = db.con;
            stmt1 = conn.createStatement();
            rs = stmt1.executeQuery(sqlPerson);
            // loop through the result set
            while (rs.next()) {
                Person person = new Person();
                person.setIdPerson(rs.getInt("id"));
                person.setName(rs.getString("name"));
                person.setLastName(rs.getString("lastname"));
                person.setAge(rs.getInt("age"));
                person.setCity(rs.getString("city"));
                persons.add(person);
            }

        } catch (SQLException e1) {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException e2) {
                System.out.println(e2.getMessage());
            }
            System.out.println(e1.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt1 != null) {
                    stmt1.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e3) {
                System.out.println(e3.getMessage());
            } finally {
                db.releaseConnection();
            }
        }
        return persons;
    }
}
