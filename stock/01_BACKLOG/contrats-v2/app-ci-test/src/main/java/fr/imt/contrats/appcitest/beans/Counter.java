package fr.imt.contrats.appcitest.beans;

import java.io.Serializable;

public class Counter implements Serializable {

        private static final long serialVersionUID = -2225907659592871329L;
        private int value;
        public Counter()       { value = 1;    }
        public int  getValue() { return value; }
        public void plusplus() { value++;      }


}
