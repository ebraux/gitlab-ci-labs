package fr.imt.contrats.appcitest.service;

import fr.imt.contrats.appcitest.beans.Person;
import fr.imt.contrats.appcitest.db.DBAccess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.File;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CrudServiceImplTest {

    private static CrudService service;
    private static Integer id;
    private static String sql = "CREATE TABLE IF NOT EXISTS person (\n"
            + "	id integer PRIMARY KEY,\n"
            + "	name text,\n"
            + "	lastname text,\n"
            + "	age integer, \n"
            + "	city text\n"
            + ");";

    @BeforeClass
    public static void setUp() {

        DBAccess db = new DBAccess();
        db.getConnectionTest();
        try {
            DatabaseMetaData meta = db.con.getMetaData();
            System.out.println("The driver name is " + meta.getDriverName());
            System.out.println("A new database has been created.");
            Statement stmt = db.con.createStatement();
            stmt.execute(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            db.releaseConnection();
        }

        service = new CrudServiceImpl();
    }

    @Test
    public void addPerson() throws Exception {
        String name= "testName";
        String lastname = "testLastName";
        Integer age = 25;
        String city = "Brest";
        service.addPerson(true, name, lastname, age, city);
        List<Person> persons = service.getPersons(true);
        id = persons.get(0).getIdPerson();
        Assert.assertTrue(persons.size()>=1);
    }

    @Test
    public void bgetPerson() throws Exception {
        Person person = service.getPerson(true, id);
        Assert.assertTrue(person.getIdPerson() > 0);
    }

    @Test
    public void cgetPersons() throws Exception {
        List<Person> persons = service.getPersons(true);
        Assert.assertTrue(persons.size()>=1);
    }

    @Test
    public void deletePerson() throws Exception {
        service.deletePerson(true, id);
        boolean trouve = false;
        List<Person> persons = service.getPersons(true);
        for(Person pers : persons){
            if(pers.getIdPerson() == id){
                trouve = true;
                break;
            }
        }
        Assert.assertFalse(trouve);

    }
    @Test
    public void edeleteBDD() throws Exception {
        File file = new File("app-ci-test-dev.db");
        if(file.delete()){
            System.out.println(" Le fichier de BDD test junit" + file.getName() + " est supprimé");
            Assert.assertTrue(true);
        }else{
            System.out.println("suppression fichier BDD test junit SQLite echouée");
            Assert.assertTrue(false);
        }
    }
}