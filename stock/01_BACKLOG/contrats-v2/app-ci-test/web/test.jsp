<%@ page import="fr.imt.contrats.appcitest.beans.Person" %>
<%@ page import="java.util.List" %>
<%@ page import="fr.imt.contrats.appcitest.db.DBAccess" %>
<%@ page import="java.sql.DatabaseMetaData" %>
<%@ page import="fr.imt.contrats.appcitest.util.PropertiesPomReader" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    PropertiesPomReader reader = null;
    try{
        reader = new PropertiesPomReader("app.properties");
    } catch(Exception e){
        e.getMessage();
    }

    String name="";
    String version="";
    if(reader!=null) {
        name = reader.getProperty("app.name");
        version = reader.getProperty("app.version");
    }
    List<Person> persons = (List<Person>) request.getAttribute("persons");
    DBAccess db = new DBAccess();
    db.getConnection();
    DatabaseMetaData meta = db.con.getMetaData();
    Integer id = (Integer) request.getAttribute("id");
    Person person = (Person) request.getAttribute("person");
    if(person==null){
        person = new Person();
        person.setName("");
        person.setLastName("");
        person.setAge(0);
        person.setCity("");
    }
%>

<!DOCTYPE html>
<html class="no-js" lang="fr" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>app ci test</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

</head>
<body>


<div class="card text-center">
    <div class="card-header">
        Application de test CI/CD avec BDD SQLite embarquée
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Actions</th>
            <th scope="col">Nom</th>
            <th scope="col">Prénom</th>
            <th scope="col">Age</th>
            <th scope="col">Ville</th>
        </tr>
        </thead>
        <tbody>

        <%if(persons != null){
            for (Person pers : persons){%>
        <tr>
            <td><a href="/app_ci_test/delete?id=<%=pers.getIdPerson()%>">Supprimer</a><br/>
                <a href="/app_ci_test/get?id=<%=pers.getIdPerson()%>">Editer</a>
            </td>
            <td><%=pers.getName()%></td>
            <td><%=pers.getLastName()%></td>
            <td><%=pers.getAge()%></td>
            <td><%=pers.getCity()%></td>
        </tr>
        <%}
        }%>
        </tbody>
    </table>


    <br/>
    <br/>

    <div class="card-body">
        <form name="formul" id="formul" method="post" action="/app_ci_test/insert">
            <div class="form-row">
                <div class="col-auto">
                    <label for="name">Nom</label>
                    <input type="text" name="name" id="name" autocomplete="off" value="<%=person.getName()%>">
                </div>
                <div class="col-auto">
                    <label for="lastname">Prénom</label>
                    <input type="text" name="lastname" id="lastname" autocomplete="off" value="<%=person.getLastName()%>">
                </div>
            </div>
            <div class="form-row">
                <div class="col-auto">
                    <label for="age">Age</label>
                    <input type="number" name="age" id="age" autocomplete="off" value="<%=person.getAge()%>">
                </div>
                <div class="col-auto">
                    <label for="city">Ville</label>
                    <input type="text" name="city" id="city" autocomplete="off" value="<%=person.getCity()%>">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-auto">
                    <button type="submit" id="ajouter" name="ajouter" class="btn btn-primary">AJouter</button>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer text-muted">
        Driver BDD : <%=meta.getDriverName()%>
        <br/>
        Environnement : <%=meta.getURL()%>
        <br/>
        <%=StringUtils.isNotBlank(name)?name + " - " +version:""%>
    </div>

</div>
