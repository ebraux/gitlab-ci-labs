# app-infratest

Application pour tester l'infrastructure : affiche les infos sur l'hote qui gére les requettes, getsion des sessions, test les accès fichiers, BDD,  ....


``` bash
docker run --rm -u $(echo $UID) --name maven-cli -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3-openjdk-11 mvn package
```

``` bash
docker run -d  -p 8000:8080 --rm --name infratest -v "$(pwd)"/target/app-infratest.war:/usr/local/tomcat/webapps/app-infratest.war  app-infratest

docker stop infratest
```

