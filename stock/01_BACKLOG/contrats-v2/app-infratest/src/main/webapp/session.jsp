<%--
    Document   : index
    Created on : 18 June, 2020
    Author     : ebraux
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.net.InetAddress,
                java.util.Date,
                java.util.Enumeration,
                test.Counter"%>

<%      
  Counter counter = (Counter)session.getAttribute("counter");
	if (counter == null) {
	  counter = new Counter();
	} else {
	  counter.plusplus();
	}
	session.setAttribute("counter", counter);
%>

<%
  InetAddress ia = InetAddress.getLocalHost();
  String nodeHostName = ia.getHostName();
  String nodeAddress = ia.getHostAddress();

  session.setMaxInactiveInterval(3600);
%>


<!DOCTYPE html>
<html>
  <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>session</title>
  </head>
  <body>
    <h2>Updated 07/10/2020 18h50 </h2>

    <p><font size = 5 COLOR="#CC0000">
            Compteur en session serialisee:  <%=counter.getValue()%></br>
            HOST:  <%=nodeHostName%></br>
    </font></p>

    <hr/>

    <h2>Session Infos</h2>
    Session Id : <b><%=session.getId()%></b> <br/>
    Is it New Session : <b><%=session.isNew()%></b><br/>
    Session Creation Date : <b><%=new Date(session.getCreationTime())%></b><br/>
    Session Access Date : <b><%=new Date(session.getLastAccessedTime())%></b><br/>

    <hr/>
    <h2>Code  Infos</h2>
    Class : <b><%=counter.getClass().getName()%></b><br/>

  </body>
</html>


</body>
</html>
