<%--
    Document   : index
    Created on : 18 June, 2020
    Author     : ebraux
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.net.InetAddress,
                java.util.Date,
                java.util.Enumeration,
                test.Counter"%>

<%      
  Counter counter = (Counter)session.getAttribute("counter");
	if (counter == null) {
	  counter = new Counter();
	} else {
	  counter.plusplus();
	}
	session.setAttribute("counter", counter);
%>

<%
  InetAddress ia = InetAddress.getLocalHost();
  String nodeHostName = ia.getHostName();
  String nodeAddress = ia.getHostAddress();
%>

<%
  Integer simpleCounter = (Integer)session.getAttribute("simpleCounter");
  String heading = null;
  if (simpleCounter == null) {
      simpleCounter = new Integer(1);
  } else {
      simpleCounter = new Integer(simpleCounter.intValue() + 1);
  }
  session.setAttribute("simpleCounter", simpleCounter);
%>


<!DOCTYPE html>
<html>
  <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>session</title>
  </head>
  <body>

    <p><font size = 5 COLOR="#CC0000">
            Compteur en session serialisee:  <%=counter.getValue()%></br>
            Compteur en session simple:  <%=simpleCounter%></br>
            HOST:  <%=nodeHostName%></br>
    </font></p>

    <hr/>

    <h2>Session Infos</h4>
    Session Id : <b><%=session.getId()%></b> <br/>
    Is it New Session : <b><%=session.isNew()%></b><br/>
    Session Creation Date : <b><%=new Date(session.getCreationTime())%></b><br/>
    Session Access Date : <b><%=new Date(session.getLastAccessedTime())%></b><br/>

    <hr/>
    <h2>Code  Infos</h4>
    Class : <b><%=counter.getClass().getName()%></b><br/>

  </body>
</html>


</body>
</html>
