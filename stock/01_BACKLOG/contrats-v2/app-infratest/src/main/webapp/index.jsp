<%--
    Document   : index
    Created on : 18 June, 2020
    Author     : ebraux
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>app-infratest</title>
    </head>

<body>
  <ul>
    <li> <a href="info.jsp">Informations sur le contexte</a></li>
    <li> <a href="session.jsp">Test de session serialize</a></li>
    <li> <a href="session-dual.jsp">Test de session misxte : serialize ET simple</a></li>
  </ul>

</body>
</html>
