<%--
    Document   : index
    Created on : 18 June, 2020
    Author     : ebraux
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.net.InetAddress,
                java.util.Date,
		java.util.Enumeration"%>

<%
  InetAddress ia = InetAddress.getLocalHost();
  String nodeHostName = ia.getHostName();
  String nodeAddress = ia.getHostAddress();
%>

<!DOCTYPE html>
<html>
  <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>infos</title>
  </head>
  <body>

    <h2>Host Infos </h2>	    
    <h4>local</h4>
    Hostname : <b><%=nodeHostName%></b> <br/>
    Address : <b><%=nodeAddress%></b><br/>

    <h4>Remote</h4>
    Hostname : <b><%=request.getRemoteHost()%></b><br/>
    Address : <b><%=request.getRemoteAddr()%></b><br/>
    Scheme :  <b><%=request.getScheme()%></b><br/>
    
  </body>
</html>


</body>
</html>
