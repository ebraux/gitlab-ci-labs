
PROXY_SETTINGS=""
if [ "${http_proxy}" != "" ]; then   PROXY_SETTINGS="$PROXY_SETTINGS --build-arg http_proxy=${http_proxy}"; fi
if [ "${https_proxy}" != "" ]; then   PROXY_SETTINGS="$PROXY_SETTINGS --build-arg https_proxy=${https_proxy}"; fi
if [ "${ftp_proxy}" != "" ]; then   PROXY_SETTINGS="$PROXY_SETTINGS --build-arg ftp_proxy=${ftp_proxy}"; fi
if [ "${no_proxy}" != "" ]; then   PROXY_SETTINGS="$PROXY_SETTINGS --build-arg no_proxy=${no_proxy}"; fi
if [ "$PROXY_SETTINGS" != "" ]; then   echo "Proxy settings were found and will be used during the build."; fi

docker build  $PROXY_SETTINGS -t local/tomcat-imta .

echo "docker tag  local/tomcat-master local-registry:5000/tomcat-master"
echo "docker push local-registry:5000/tomcat-master"


