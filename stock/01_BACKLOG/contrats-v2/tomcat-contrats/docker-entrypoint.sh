#!/bin/bash
#
# docker-entrypoint for tomcat-imta

set -e

# si il faut modifier la gestion des droits d'accès admin tomcat
if [[ ! -z "$TOMCAT_USERS_CONFIG" ]]; then
    # copie des template et personalisation
    cp $CATALINA_HOME/config-template/tomcat-users.xml $CATALINA_HOME/conf/tomcat-users.xml
    cp $CATALINA_HOME/config-template/manager-context.xml $CATALINA_HOME/webapps/manager/META-INF/context.xml
    cp $CATALINA_HOME/config-template/host-manager-context.xml $CATALINA_HOME/webapps/host-manager/META-INF/context.xml
    envsubst \$TOMCAT_USERS_CONFIG < "${CATALINA_HOME}/conf/tomcat-users.xml" | sponge "${CATALINA_HOME}/conf/tomcat-users.xml"
fi

# si la customisation du fichier de context est demandée
if [[ ! -z "$TOMCAT_CONTEXT_CONFIG" ]]; then
    # si on fourni un fichier complet, on le met en place
    if [[ ! -z "$CONTEXT_FILE_CONTENT" ]]; then
        printf "%s" "$CONTEXT_FILE_CONTENT" > "${CATALINA_HOME}/conf/context.xml"
    else
    # sinon, copie du template et  personalisation
    cp $CATALINA_HOME/config-template/context.xml $CATALINA_HOME/conf/context.xml
    if [ ! -n "$CONTEXT_MEMCACHED" ]; then 
        CONTEXT_MEMCACHED='<!-- No memcached configuration -->'
    fi

    if [[ ! -n "$CONTEXT_DATASOURCE_CONFIG" ]]; then
            CONTEXT_DATASOURCE_CONFIG='<!-- No datasource configuration -->'
    fi

    if [ ! -n  "$CONTEXT_CUSTOM" ]; then
            CONTEXT_CUSTOM='<-- No custom context configuration -->'
    fi

    envsubst \$CONTEXT_MEMCACHED_CONFIG,\$CONTEXT_DATASOURCE_CONFIG,\$CONTEXT_CUSTOM < "${CATALINA_HOME}/conf/context.xml" | sponge "${CATALINA_HOME}/conf/context.xml"
    fi
fi


# execute command passed in as arguments.
exec "$@"

