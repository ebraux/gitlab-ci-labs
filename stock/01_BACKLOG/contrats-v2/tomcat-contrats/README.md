## Image de base

Basé sur image "tomcat:9.0-jre11" :

* site de l'image : [https://hub.docker.com/_/tomcat]
* depot github : [https://github.com/docker-library/tomcat/tree/master/9.0/jdk12]
* détails de l'installation de tomcat dans cette image :
  * depuis le code source, en provenance du site apache.org
  * dans le dossier '/usr/local/tomcat'
  * écoute sur le port 8080

## Personnalisation

Ajout des outils : vim, zsh, tcpdump, dnsutils, sysstat, procps, net-tools, tzdata

Configuration des outils de management:

* accès web distant à "/manager", "/manager/status" et "/host-manager"
* utilisateur pour les applications web :  username="human", password="human"
* utilisateur pour les scripts, api et jmx : username="nothuman", password="nothuman"

Configuration pour accès à des bases oracle :

* ajout de la librairie "ojdbc8.jar"
* configuration de la timezone "Europe/Paris", utilisée pour la configuration du client à la connexion.
* Aucune Datasource déclarée par défaut. La configuration de l'accès à la base Oracle est définie au niveau de l'application.

Monitoring/suivi :

* activation  metrics tomcat, et jmx
* ajout de la configuration pour activer la collecte de données par prometheus
* ajout de l'applcation `probe-sci`


## Détail des configuration

### Fichier impactés

* fichier  /usr/local/tomcat/conf/server.xml
* fichier /usr/local/tomcat/conf/context.xml
* fichier /usr/local/tomcat/conf/catalina.properties.xml

### Pour l'accès aux interfaces d'administration

* fichier conf/tomcat-users.xml

```xml
  <role rolename="manager-gui"/>
  <role rolename="manager-script"/>
  <role rolename="manager-jmx"/>
  <role rolename="manager-status"/>
  <role rolename="admin-gui"/>
  <role rolename="admin-script"/>
  <user username="xxxxxxxx" password="xxxxxxxx" roles="manager-gui,admin-gui"/>
  <user username="xxxxxxxx" password="xxxxxxxx" roles="manager-script,manager-jmx,admin-script"/>
```

### Pour la gestion des session

* fichier  /usr/local/tomcat/conf/server.xml
* fichier /usr/local/tomcat/conf/context.xml

Le mécanisme de partage de session mis en place impose une valeur de "vmroute" commune pour tous les serveurs tomcat. Cette valeur n'a pas d'impact si on n'ets pas en mode cluster, elle peut donc être configurée quel que soit l'usage. Comme la session est partagée il n'y a pas de serveur privilégié, et donc pas besoin de sticky session. Modification de server.xml :
```xml
<!-- You should set jvmRoute to support load-balancing via AJP ie :
    <Engine name="Catalina" defaultHost="localhost" jvmRoute="jvm1">
    -->
    <Engine name="Catalina" defaultHost="localhost" jvmRoute="shared">
```

En cas d'utilisation en mode cluster, il faut également activer l'accées à memcached. Un exemple de configuration en commentaire est ajouté au fichier context.xml
```xml
    <!-- Uncomment this to enable session persistence memcache storage --> 
    <!--
    <Manager className='de.javakaffee.web.msm.MemcachedBackupSessionManager'
	    memcachedNodes='n1:memcached:11211'
	    sticky="false"
            requestUriIgnorePattern='.*\.(ico|png|gif|jpg|css|js)$' />
    -->
```

### Pour la gestion de la timezone avec Oracle

A ajouter dans le Dockerfile

```bash
RUN apt install tzdata

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
```


### Pour la gestion de version de XMLParser

* fichier /usr/local/tomcat/conf/catalina.properties.xml

Modification catalina.properties.xml, ajout à la fin du fichier :
```bash
javax.xml.parsers.DocumentBuilderFactory=com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl
javax.xml.transform.TransformerFactory=com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl
javax.xml.parsers.SAXParserFactory=com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl
javax.xml.datatype.DatatypeFactory=com.sun.org.apache.xerces.internal.jaxp.datatype.DatatypeFactoryImpl
```


* dossier /usr/local/tomcat/scripts

Le dossier script permet de customiser l'image lors de son initialisation, via des scripts.
Ce mécanisme peut être utilsié par exemple pour initialiser les application à lancer, en ajoutant un script qui récupère les fichiers war, et le copie des le dossier webapps.

## Utilisation de l'image

## Utilisation de l'image depuis la registry

Le build est automatiqueon à chaque push.
* déclaration de la registry
```console
export CI_REGISTRY_USER='xxxxxx'
export CI_REGISTRY_PASSWORD='xxxxxxxxxxxxxx'
export CI_REGISTRY='gitlab-disi-registry.imt-atlantique.fr/contrats/infrastructure/tomcat-contrats'
```
* Lancement de l'image
```console
docker login -u "$CI_REGISTRY_USER"  -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY 
docker pull $CI_REGISTRY/tomcat-imta

docker run -p 8080:8080 $CI_REGISTRY/tomcat-imta
```

## Customisation avec des variables d'environnment (expérimental)

Gestion des droits d'accès à l'interface :

- `$TOMCAT_USERS_CONFIG` : remplace la déclaration des roles et user dans tomcat-users.xml

Gestion du context :

- `$TOMCAT_USERS_CONFIG` : active la customisation du fichier de context
- `$CONTEXT_FILE_CONTENT` : remplace intégralement le fichier context.xml
- sinon à prtir d'un temptate :
  - `CONTEXT_MEMCACHED` : insere une configuration pour l'acces à memcached
  - `CONTEXT_DATASOURCE_CONFIG` : insere une configuration pour les datasource
  - `CONTEXT_CUSTOM` : insere une configuration personnalisée

Exemple :

```bash
export CONTEXT_MEMCACHED_CONFIG=<Manager className="de.javakaffee.web.msm.MemcachedBackupSessionManager" memcachedNodes="n1:memcached:11211" sticky="false" requestUriIgnorePattern=".*\.(ico|png|gif|jpg|css|js)$" />
export TOMCAT_USERS_CONFIG=true
export CONTEXT_DATASOURCE_CONFIG=<Resource name="jdbc/DBTest" type="javax.sql.DataSource" driverClassName="org.sqlite.JDBC" url="jdbc:sqlite:/usr/local/tomcat/db/app-ci-test-dev.db" />
export TOMCAT_USERS_CONFIG= <role rolename="manager-gui"/> <role rolename="manager-script"/> <role rolename="manager-jmx"/> <role rolename="manager-status"/> <role rolename="admin-gui"/> <role rolename="admin-script"/> <user username="human" password="human" roles="manager-gui,admin-gui"/> <user username="nothuman" password="nothuman" roles="manager-script,manager-jmx,admin-script"/>
```

### Utilisation de l'image en "mode Build"

Utilisation de l'image pour la rendre disponible en local pour Docker. aucune modification du dépot n'est possible.

Pour récupérer l'image tomcat-contrat "deploy-token" permettant un accès en lecture au dépot en http.

1. configurer environnement de proxy si besoin
```bash
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy=localhost,127.0.0.1,`hostname -i`
```
2. Recupérer l'image tomcat-contrat depuis le dépot GIT
``` bash
cd ~
# token : tomcat-contrats
git clone https://gitlab-disi.imt-atlantique.fr/contrats/infrastructure/tomcat-contrats.git
cd tomcat-contrats
```
3. builder l'image
```console
./build.sh
cd ../..
``` 
4. Vérifier
```console
docker images
```



## Modificiation de l'image

### Prérequis

Necessite de disposer d'une clé privée autorisée pour un accès en ecriture sur le dépot. 
Pour activer l'utilisation de la clé :

```bash
eval $(ssh-agent -s)
ssh-add <CHEMIN_COMPLET-VERS-LA-CLE-PRIVEE>
```

### Recupération du dépot

```console
git clone git@gitlab-disi.imt-atlantique.fr:contrats/infrastructure/tomcat-contrats.git
```

### Tests en local

1. [optionnel] Configurer l' environnement de proxy
```console
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy=localhost,127.0.0.1,'`hostname -i`
```
2. builder l'image
```console
cd tomcat-imta
./build.sh 
cd ../..
``` 
3. vérifier que l'image est disponible en local
```console
docker images
```
4. Lancer l'image pour valider
```console
docker run -p 8080:8080 local/tomcat-imta
``` 

### Mise à jour de l'image
* rappel pour prise en compte des modification dans git :
```console
git add <NOM-DU-FICHIER>
git commit -m "<TEXTE-EXPLICATIF-DE-LA-MODIFICATION>"
```
* Mise à jour du dépot distant
```console
git push
```

La chaine de CI mise en place dans le dépot génére automatiquement une nouvelle image dans la registry du projet dans Gitlab.
