# Création des secrets.

Opérations à faire sur chacun des manager Swram.


- Génération des secrets, et lancement de la stack

```bash

cd /swarm-stacks
git clone https://deploiement_RO:hAiA2KihSEt4sEfyGBsM@gitlab.imt-atlantique.fr/contrats-poc-v2/deploiement.git

cd deploiement/environment
ls
# au choix
# cd inte
# cd rec
# cd prod

docker secret create contrats-tomcat-context.xml context.xml
docker secret create contrats-tomcat-server.xml server.xml
docker secret create contrats-tomcat-tomcat-users.xml tomcat-users.xml

docker secret ls

cd ../../..

rm -rf deploiement

```

- Suppression des secrets (necessite d'arrêter les stacks)
```bash
docker secret rm contrats-tomcat-context.xml
docker secret rm contrats-tomcat-server.xml
docker secret rm contrats-tomcat-tomcat-users.xml

docker secret ls

```
