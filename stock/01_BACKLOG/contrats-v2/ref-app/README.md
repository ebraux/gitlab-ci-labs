Cette application concerne l'ensemble des personnels non-permanents ayant une activité sur l'un des campus , hormis les intérimaires, les prestataires et les vacataires intervenant en enseignement. Elle est dédiée à la RH et les Finances

**A chaque commit il faut modifier la version "app.version" dans pom.xml**   

Lors d'une modification de css ne pas oubliez de faire une montée de version du fichier app.<version>.css et modifier la constante FIC_CSS_PNP avec le nom du fichier app.<version>.css