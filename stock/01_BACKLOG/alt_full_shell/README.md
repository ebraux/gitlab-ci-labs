# Utilisation du Lab en ligne de commande



## Créer un environnement de travail

```bash
cd $HOME

mkdir lab-ci
export LAB_HOME=${PWD}/lab-ci
cd $LAB_HOME
```

Copier le contenu des labs
```bash
cd $LAB_HOME
git clone https://gitlab.com/ebraux/gitlab_ci_introduction.git
```

Créer un dossier local

Dans ce dossier créer un fichier README.md
```bash
cd  lab-ci
echo "LAB Gitlab CI" >> README.md
```

Et un fichier .gitignore, pour ne pas intégrer le dépot du sujet du LAB dans votre dépot 
```bash
cd  lab-ci
echo "/gitlab_ci_introduction" >> .gitignore
```

Créer un dépot dans gitlab

- se connecter à gitlab [https://gitlab.com](https://gitlab.com)
- Créer un projet `lab-ci`


Executer les commandes d'initialisation d'un dépot dans un dossier existant. L'exemple ci-dessous est à adapter à votre configuration :

```bash
cd existing_folder
git init
git remote add origin git@gitlab.com:XXXXXX/lab-ci.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

Vous disposez maintenant d'un dépot GIT contenant un fichier README.md. Par exemple accessible via une url du type "https://gitlab.com/XXXXXX/lab-ci".


## Configuration de votre environnement pour simplifier le lab

Mettre en place des alias et fonction shell dans votre environnement :
```bash
source $LAB_HOME/gitlab_ci_introduction/docs/labs/alt_full_shell/git-ci.bashrc
```

Cette commande :

- mets en place les raccourcis :
   - suivre un fichier : `gita fichier`
   - faire un commit : `gitc 'message' `
   - faire un push : `gitp`
   - vérifier le status du dépot : `gits`
- mets en place une fonction dédiée `gitci` qui : 
   - copier le fichier en paramètre la place de ${LAB_HOME}/lab-ci/.gitlab-ci.yml
   - le prendre en compote dans un commit 
   - synchroniser le dépot


Par exemple, pour tester le fichier "gitlab_ci_introduction/docs/labs/first-pipeline/hello_gitlab-ci.yml"

Manuellement : 
```bash
cd ${LAB_HOME}
cp gitlab_ci_introduction/docs/labs/first-pipeline/hello_gitlab-ci.yml ${LAB_HOME}/.gitlab-ci.yml
git add .gitlab-ci.yml
git commit -m 'test de first-pipeline/hello_gitlab-ci.yml'
git push
cd -
```

Avec la fonction 
```bash
gitci test-gitlab-ci.yml
```


