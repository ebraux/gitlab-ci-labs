alias gita='git add '
alias gitc='git commit -m '
alias gitp='git push'
alias gits='git status'

function gitci {
   echo "Update lab-ci with $@"
   cp "$@" ${LAB_HOME}/.gitlab-ci.yml
   pushd ${LAB_HOME}
   gita .
   gitc $@
   gitp
   popd
}