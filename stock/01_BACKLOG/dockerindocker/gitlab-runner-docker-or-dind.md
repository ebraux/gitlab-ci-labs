
https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4260

Thanks for opening the issue @timw we actually faced the same issue in our own infrastructure.
The reason for this is because if you look at the config file we are mounting the socket:
volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
With !1261 (merged) we started mounting volumes to services as well. Now this breaks this case where you are mounting the docker.sock but then using the dind service where it tries to create a new socket that is why we get can't create unix socket /var/run/docker.sock: device or resource busy. At GitLab we removed the volume mount from the configuration since it does not make sense to have it mounted at all times, looking at https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding we suggest either using the dind service or using the volume mounting, not both together.
We plan to add a feature flag with !1352 (merged) that creates a new feature flag where you can use the previous method (meaning volumes are not mounted to services), but we plan to deprecate this soon. The feature flag will land in 11.11.1 in the coming week.
Hope this makes sense for you and sorry for the trouble caused!