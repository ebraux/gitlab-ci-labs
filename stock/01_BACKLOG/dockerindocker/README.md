# Utilisation de Docker In Docker (dind)

Existe une image "docker", qui permet de lancer des commande docker (client)

Utilisation - interagir avec un daemon docker : 

- qui s'execute en local, en montant les sockets Unix
- qui s'execute en distant, en faisant des appel sur l'API

Ce mode peut être utilisé avec un runner Docker. Le runner lance une image docker, qui accède au daemon local en utilisant les socket UNIX. Dans ce cas :

- tout est géré sur l'HOST : chargement des images, création des volumes, lancement des container, ... Il faut donc gérer la suppression des ressources
- tous les processus lancés peuvent accéder aux ressources gérée par Docker présentes sur l'HOST.
  - accès à des volumes créés par d'autres utilisateurs
  - accès à d'autres container en cours d'execution ,...

Pour éviter ces problèmes et isoler les executions, on utilise Docker In Docker. Ce qui revient à lancer en plus du container Docker "client", un autre container en mode Daemon, avec lequel le client va interagir.

Tout est géré sur ce container Daemon. Et une fois les traitements terminés, il est tout simplement détruit.

---
## Version de DinD et configuration de TLS : "Cannot connect to the Docker daemon"

A partir de la version 18.09+, docker:dind génère automatique des certificats TLS, et impose leur utilisation pour les connexions distantes. Si ces certificats ne sont pas utilisé, la connexion est impossible, et la chaine d CI renvoie le message `docker: Cannot connect to the Docker daemon at tcp://docker:2375. Is the docker daemon running?.`

Les certificats sont généré dans le dossier spécifié par la variable d'environnement `DOCKER_TLS_CERTDIR`.

Utiliser Docker Dind, il faut donc soit :

- utiliser une version antérieur à la version 18.09, 
- utiliser une version >18.09, et désactiver la configuration TLS au niveau du container DinD
- utiliser une version >18.09, et configurer l'utilisation de TLS entre le client Docker et le container DinD

### Utilisation d'une version antérieure à "18.09"

```yaml
testdind:
  image: docker:18.09.7
  
  services:
  - docker:18.09.7-dind

  script:
  - docker info
```




### Utilisation de DinD sans TLS

Cette solution est utilisée par Gitlab dans ces template, et c'est celle quon retouve dans le exemples d'utilisation de DinD avec la CI de Gitlab.

Elle necessite de configurer des variables : 

- DOCKER_TLS_CERTDIR: ""
- DOCKER_DRIVER: overlay2 (optionel)
- DOCKER_HOST='tcp://docker:2375'  (optionel)

```yaml
testdind:
  image: docker:19.03.12
  
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  
  services:
  - docker:19.03.12-dind

  script:
  - docker info
```


Rem : Pour DOCKER_HOST,  'tcp://docker:2375' est la valeur par défaut. Elle peut-être définie soit au niveau de variables dans le fichier de CI, soit dans la configuration du runner  

```ini
[runners.docker]
  host = "tcp://docker:2375"
```

DOCKER_TLS_CERTDIR, doit être définie dans le fichier CI. La placer dans les variables "environment = ["DOCKER_TLS_CERTDIR="]" dans la configuration du runner ne fonctionne pas.



### Utilisation de DinD avec configuration de TLS

Modifier la configuration du runner, pour créer un volume partagé pour les certifictas :
```ini
volumes = ["/certs/client", "/cache"]
```

Et configurer le chemin des certificats dnas le fichier de CI

```yaml
DOCKER_TLS_CERTDIR: "/certs"
```

[https://about.gitlab.com/blog/2019/07/31/docker-in-docker-with-docker-19-dot-03/](https://about.gitlab.com/blog/2019/07/31/docker-in-docker-with-docker-19-dot-03/)


# Ressources 

Documentations :

- [https://about.gitlab.com/blog/2019/07/31/docker-in-docker-with-docker-19-dot-03/](https://about.gitlab.com/blog/2019/07/31/docker-in-docker-with-docker-19-dot-03/)
- [https://www.testcontainers.org/supported_docker_environment/continuous_integration/gitlab_ci/](https://www.testcontainers.org/supported_docker_environment/continuous_integration/gitlab_ci/)
- [https://hub.docker.com/_/docker/](https://hub.docker.com/_/docker/), notamment la section "TLS"
- [https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1986](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1986)
- [https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-the-docker-image-docker-in-docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-the-docker-image-docker-in-docker)
- [https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4501](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4501)


Exemples :

- [https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml)
```bash
Starting in 18.09+, the dind variants of this image will automatically generate TLS certificates in the directory specified by the DOCKER_TLS_CERTDIR environment variable.

 Warning: in 18.09, this behavior is disabled by default (for compatibility).
 
  If you use –network=host, shared network namespaces (as in Kubernetes pods), or otherwise have network access to the container (including containers started within the dind instance via their gateway interface), this is a potential security issue (which can lead to access to the host system, for example).
   It is recommended to enable TLS by setting the variable to an appropriate value (-e DOCKER_TLS_CERTDIR=/certs or similar). In 19.03+, this behavior is enabled by default.
```

```bash
# When you use the dind service, you must instruct Docker to talk with
  # the daemon started inside of the service. The daemon is available
  # with a network connection instead of the default
  # /var/run/docker.sock socket. Docker 19.03 does this automatically
  # by setting the DOCKER_HOST in
  # https://github.com/docker-library/docker/blob/d45051476babc297257df490d22cbd806f1b11e4/19.03/docker-entrypoint.sh#L23-L29
  #
  # The 'docker' hostname is the alias of the service container as described at
  # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services.
  #
  # Specify to Docker where to create the certificates. Docker
  # creates them automatically on boot, and creates
  # `/certs/client` to share between the service and job
  # container, thanks to volume mount from config.toml
```  




