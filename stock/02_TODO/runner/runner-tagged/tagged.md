# Gestion des runner

## Objectifs

- Utiliser les runner par défaut
- Déployer sont propre runner

---

## Utilisation des runner par défaut


### Accéder à la liste des runner 

Dans votre projet, accéder à la la liste des runners :

![](img/menu_ci-cd.png){: style="height:300px"}
![](img/section-runner.png){: style="height:150px}


La liste des runners comprend : 

- des runners "Specific", dédiés au projet (aucun par défaut)
- des runners "Shared", partagés entre tous les projets

![](img/list-runner.png){: style="height:150px}


### Lancer un job avec le runner par défaut

Dans l'onglet "Write Pipeline Configuration", saisir un job simple permettant d'afficher les informations sur l'OS du container utilisé

```yaml
job_os_version :
  script :
    - apt-get update && apt-get install -y lsb-release
    - lsb_release -d
```
*source : [os-version_gitlab-ci.yml](os-version_gitlab-ci.yml)*

Dans le détail du Job, on peut observer :

- Le nom de l'image par défaut : ici `ruby:2.5`

![](img/default-image.png){: style="height:120px"}

- Les informations sur l'OS : 

![](img/default-os.png){: style="height:60px"}


## Configurer l'image Docker utilisée

### Changer l'image Docker par défaut

Il est possible  de choisir l'image à utiliser avec le mot-clé `image`

- [https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#define-image-in-the-gitlab-ciyml-file](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#define-image-in-the-gitlab-ciyml-file)

Exemple d'utilisation du paramètre `image` pour utiliser une image `ubuntu`

```yaml
image: ubuntu:focal

job_os_version :
  script :
    - apt-get update && apt-get install -y lsb-release
    - lsb_release -d
```
*source : [custom-image_gitlab-ci.yml](custom-image_gitlab-ci.yml)*

- Le nom de l'image est maintenant `ubuntu:focal`

![](img/ubuntu-image.png){: style="height:120px"}

- Idem pour l'OS : 

![](img/ubuntu-os.png){: style="height:60px"}


### Changer l'image utilisée pour un job spécifique

il est possible de définir une image différente par job.

```yaml
job_os_version :
  script :
    - apt-get update && apt-get install -y lsb-release
    - lsb_release -d

job_ubuntu :
  image: ubuntu:focal
  script :
    - apt-get update && apt-get install -y lsb-release
    - lsb_release -d
```
*source : [job-image_gitlab-ci.yml](job-image_gitlab-ci.yml)*

Pour le premier job, l'image est l'image par défaut : `ruby:2.5̀`
Pour le deuxième job, l'image est une image spécifique : `ubuntu:focal`


---

## Déployer un runner de projet

### Principe 

Déployer un runner spécifique au projet

La section "Runner" du menu "CI/CD" contient les information nécessaires au déploiement d'un Runner. Par exemple dans la copie d'écran en début de lab, on peut observer :

- L'url d'accès à l'instance gitlab : [https://gitlab.com/ ](https://gitlab.com/)
- Le "registration token": `J6AzHSAyr-o39sZsUFoz`

> un runner a uniquement besoin d'un accès en HTTP(S) à l'URL de l'instance Gitlab.

La section "Runner" contient également les information snecessaire pour déployer un runner dans différents système : Linux, MacOs, Windows,...

Si vous ne disposez pas d'un machine dédiée au runner, la solution recommandée est de déployer un runner sous forme de container Docker




---
## Sélectionner un runner pour le traitement des jobs

Pour choisir quel runner sera utilisé pour traiter un job, on utilise le filtrage par tags.

```yaml
job_os_version :
  image: ubuntu:focal
  tags:
    - lab-ci
  script :
    - apt-get update && apt-get install -y lsb-release
    - lsb_release -d
```
*source : [job-tag_gitlab-ci.yml](job-tag_gitlab-ci.yml)*

![](img/tagged-runner.png){: style="height:100px"}



Il est également possible de désactiver l'utilisation des "Shared Runners"
![](img/disable-shared.png){: style="height:350px"}

---
## Autres types de runners

Gitlab permet d'utiliser d'autre types d'"executor" pour les runners, en fonction de contrainte d'architecture, de performance, ... : kubernetes, docker, shell, ssh, virtualbox, ...

Il également possible de créer son propre "executor".

[https://docs.gitlab.com/runner/executors/](https://docs.gitlab.com/runner/executors/)


﻿

Par défaut, les jobs sont exécutés dans des conteneurs Docker.
- [https://docs.gitlab.com/runner/](https://docs.gitlab.com/runner/)


