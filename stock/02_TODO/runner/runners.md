

dans menu CI/CD, on al a liste des runners

- runner d'instance
  - fournis par le gestionnaire de l'infra
    - gitlab : gitlab.com
    - le getsionnaire de l'infra en mode "self manage"
- autres types de runners.
    - machine réélle
    - VM
    - Docker
    - K8S
    - ...
  
Principe des runners.
- a aucun moment gitlab n'envoie de job sur un runner
- gitlab va créer des job, avec status "en atente"
- les runner interrogent gitlab, pour récupérer un job en attente, et le traiter.


Tous les runners ne sont pas capables de lancer des images docker.
Il faut que les runners puissent exécuter les jobs


Quand un job commence :
- il prépare l'environnement d’exécution
- récupère le cache (comment où ?)
- clone le projet
- téléchargement des artifacts
- execute les scripts
- uload les artifacts.

mode opératoire  pour installer un runner
- installer le runner sur une machine.
  - choisir l' "executer", qui executera les instruction de la partie script des jobs

Créer un runner dans gitlab

- définir des étiquettes (tags)
  - seuls les jobs avec ces tags seront executés sur le runner
  - on peut aussi selectionner d'accepter les jobs m^me sans étiquettes

choisir les options :
- travailler avec des branches protégée
- timeout
- ...

 Ensuite : ecran d'installation, avec les commandes pour déployer le runner

Installer le runner :
``` bash
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

etape 1  : configurer
```bash
gitlab-runner register  --url https://gitlab.com  --token glrt-oJmxtm1gUzvP6_GqxQZe
```
- Valider les informations :
- choisir un nom :
- sélectionner l’exécuter : shell


2:
lancer la commende gitlab-runner run

On peut voir le runner dans la liste des runners du projet.

gitlab runner est plutôt un gestionnaire d’exécuteur de job qu'un executeur de job en lui même.
Il devrait s'appeler plutôt "gitlab-runner-manager"

Sur une instance de runner
- register : enregistre un nouveau runner sur gitlab
- list : liste les runner 
- run : lance les runner.

s'appui sur un fichier config, copié dan sle hoe de l'utilisateur.
attention, en cas d'installation en tant que paquet linux, il faut le configurer en tant que root, car c'ets le fichier /etc/gitlab-runner qui est utilisé !!!

---
## sudo ou non

!!! attention au fichier de configuration

Si pas configuré/lancé avec sudo : 
- configuration : $HOME/.gitlab-runner/config.toml
``` bash
# WARNING: Running in user-mode.                     
# WARNING: Use sudo for system-mode:                 
# WARNING: $ sudo gitlab-runner...   

# Starting multi-runner from /home/vagrant/.gitlab-runner/config.toml
```

Si configuré puis lancé avec sudo :
``` bash
# Starting multi-runner from /etc/gitlab-runner/config.toml
```

Il faut donc bien faire attention quand on lance la commande de `register`.

---
## register 

Support for registration tokens is deprecated
In GitLab Runner 15.6, the use of registration tokens and runner parameters in the 'register' command was deprecated. They have been replaced by authentication tokens. How does this impact my current registration workflow? 

---
## Lancement avec vagrant :
``` bash
vagrant init generic/ubuntu2204
# modif Vagrantfile
# port 8090
# update et install à la fin
vagrant up

vagrant ssh
mdp : vagrant


---
## Utilisation

Si plusieurs tags, c'est un "ET" qui est fait