

"Code quality" permet de générer un rapport de qualité du code produit dans un dépot.

Se base sur "codeclimate/codeclimate" [https://docs.codeclimate.com/docs](https://docs.codeclimate.com/docs). Aucune licenec codeclimate n'est nécessaire.


Code quality est défini dans un template au nieveu du serveur gitlab. le source de ce tempate est visible sur le serveur, mais est également disponible à l'adresse : [https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml)

Pour utiliser "Code Quality", il suffit d'inculre cette template dans le fichier de CI .gitlab-ci.yml : 

```yaml
include:
  - template: Code-Quality.gitlab-ci.yml
```

Le template codequality :
- utilise dockerInDocker pour lancer un container à partir d'une image "codeQuality" proposée par Gitlab
- enregistre un artifact avec le résultat: 
```yaml
    reports:
      codequality: gl-code-quality-report.json
```

Par défaut : 

- l'image utilisée est celle fournie par Gitlab : "registry.gitlab.com/gitlab-org/ci-cd/codequality:0.85.23"
- le job s'execute  dans le stage "test"


Il est possible d'adapter ce fonctionnement, et redéfinissant un job "code_quality":
  
- par exemple pour utiliser une image de code quality personnalisée: variable CODE_QUALITY_IMAGE; 
- pour lancer l'execution dans un autre stage
- pour stocker le résultat dans un autre artifact

```yaml
code_quality:
  variables:
    CODE_QUALITY_IMAGE: "registry.example.com/codequality-fork:latest"
  tags:
    - testproxy
```


Exmeple de code pour Code_quality indépendnat des templsate Gitlab
```yaml
code_quality:
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env SOURCE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/codequality:$SP_VERSION" /code
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
```

Ressources :

-  [https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#)
- [https://forge.etsi.org/rep/help/ci/examples/code_quality.md](https://forge.etsi.org/rep/help/ci/examples/code_quality.md)