
# listind du contenu d'un dossier

```yaml
image: alpine:latest
script:
    - mkdir -p public
    - echo "<html><body><h1>Directory listing:</h1>" > ./index.html
    - find -exec echo "<a href='{}'>{}</a><br/>" \; >> ./index.html
    - echo "</body></html>" >> ./index.html
    - cd ..
```


utilisation de la commande `tree`
```yaml
script:
    - apk install tree
    - mkdir -p public
    # the -I flag hides the files from the index, but they are still present in the directory
    - tree ./public -H '.' -T "Content" -I "reveal*|img*|style*|index*|content*|data" --noreport --charset utf-8 > ./public/index2.html
```
