pages:
  <<: *template_job
  stage : pages
  only:
    variables:
      - $CI_COMMIT_MESSAGE =~ /^\[doc\].*/i
  script:
    - pip install sphinx
    - sphinx-apidoc -o doc .
    - sphinx-build doc public 
  artifacts:
    paths:
      - public