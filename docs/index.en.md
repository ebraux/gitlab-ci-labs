# Gitlab CI Labs

---
## Description

Set of labs aimed at creating CI/CD files, based on different themes:

- Creating pipelines
- Managing runners
- Creating Docker images in the registry,
- ...

A solution version is provided for each Lab.

The first step is to create a project on a Gitlab server, in order to trigger continuous integration pipelines.

The second step is to deploy a Gitlab runner to execute jobs in the integration pipelines.

Then, the manipulations are carried out using the Gitlab graphical interface. It is of course possible to do them via the command line.

!!! warning

    "Pushes" are done directly on the "master" branch, which is not recommended as a best practice.

---
## Getting started with the lab:

- [Create a Git repository in Gitlab](./project-create/index.md)
- Deploy a runner :
    - [Principle of Gitlab runners](./runner/index.md)
    - [Register a project runner in Gitlab](./runner/project-runner-create.md)
    - [Deploy a Gitlab runner with Docker](./runner/deploy-docker.md)


---
## First steps in CI/CD:

- [First Pipeline](first-pipeline/index.md)

---
## Thematic Labs:

- [Organizing pipelines](pipelines/index.md)
- [Using variables](variables/index.md)
- [Using artifacts](artifacts/index.md)
- [Using the project's dedicated Docker Registry](docker-registry/index.md)
- [Using Gitlab Pages](pages/index.md)


---
## Gitlab documentation entry points:

- Key principles: [https://docs.gitlab.com/ee/ci/README.html](https://docs.gitlab.com/ee/ci/README.html)
- ".gitlab-ci.yml" reference: [https://docs.gitlab.com/ee/ci/yaml/](https://docs.gitlab.com/ee/ci/yaml/)