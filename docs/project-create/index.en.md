# Create a Project in Gitlab

- Log in to Gitlab [https://gitlab.com](https://gitlab.com)

- Create a new project ...

![](img/project-new.png)

- ... blank (Blank Project) ...

![](img/project-blank.png)

- ... named `lab-ci`, initialized with a README file:

![](img/lab-ci_details.png)

---

!!! success

    You now have a GIT repository containing a README.md file, accessible via a URL like "https://gitlab.com/XXXXXX/lab-ci".

---