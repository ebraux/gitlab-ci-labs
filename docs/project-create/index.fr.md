# Créer un Projet dans gitlab

- Se connecter à Gitlab [https://gitlab.com](https://gitlab.com)

- Créer un nouveau projet ...

![](img/project-new.png)

- ... vierge (Blank Project) ...

![](img/project-blank.png)

- ... dont le nom est `lab-ci`, initialisé avec un fichier README :

![](img/lab-ci_details.png)

---

!!! success

    Vous disposez maintenant d'un dépôt GIT contenant un fichier README.md, accessible via une url du type "https://gitlab.com/XXXXXX/lab-ci".

---