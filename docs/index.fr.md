# Labs Gitlab CI

---
## Description

Ensemble de labs dont l'objectif est de créer des fichiers de CI/CD, en fonction de différentes thématiques :

- Création de pipelines
- Gestion des runner
- Création d'images docker dans la registry,
- ...

Une version de la solution est proposée pour chaque Lab.


La première étape consiste à créer un projet sur un serveur Gitlab, afin de pouvoir déclencher des pipelines d'intégration continue.

La deuxième étape consiste à déployer un gitlab-runner afin de pouvoir exécuter les jobs dans les pipeline d'intégration.


Ensuite, les manipulations sont réalisées en utilisant l'interface graphique de Gitlab. Il est bien sûr possible de les réaliser en ligne de commande. 

!!! warning

    Dans la plupart des exemples, les "push" sont faits directement sur la branche "main", ce qui n'est pas recommandé dans les bonnes pratiques. 


---
## Prise main du lab :

- [Créer un dépôt Git dans Gitlab](project-create/index.md)
- Déployer un runner:
    - [Principe de fonctionnement des runners Gitlab](./runner/index.md)
    - [Déclarer un runner de projet dans Gitlab](./runner/project-runner-create.md)
    - [Déployer un gitlab-runner avec Docker](./runner/deploy-docker.md)

---
## Premiers pas en CI/CD :

- [Premier Pipeline](first-pipeline/index.md)

---
## Labs Thématiques :

- [Organiser les pipelines](pipelines/index.md)
- [Utiliser des variables](variables/index.md)
- [Optimiser les jobs avec l'héritage](./jobs-inheritance/index.md)
- [Utiliser les artifacts](artifacts/index.md) 
- [Utiliser la Docker Registry dédiée au projet](docker-registry/index.md)
- [Utiliser les Gitlab Pages](pages/index.md)
- [Utiliser les environnements](./environments/index.md)

---
## Autres

- Gérer les jobs    
    - [Héritage de jobs](./jobs-inheritance/index.md)
    - [Déclencher des job sur la modification d'un fichier](./jobs-control/changes.md)
    - [Gérer les erreurs](./jobs-control/errors.md)
    - [Ajouter une étape manuelle](./jobs-control/manual.md)
    - Filtrer le déclenchement des jobs (messages de commit, branche, merge request, ...)
        - [Utiliser des règles (rules)](./jobs-control/rules.md)
        - [Ne plus utiliser only/except (déprécié)](./jobs-control/only-except.md)
- Gérer les pipelines :
    - [Définir des variables en fonction des environnements](./environments/variables.md)
- Gérer les runners :
    - [Principe de fonctionnement des runners Gitlab](./runner/index.md)
    - [Créer un runner de projet dans Gitlab](./runner/project-runner-create.md)
    - [Déployer un Gitlab-runner avec Docker](./runner/deploy-docker.md)
    - [Supprimer un Runner déployé avec Docker](./runner/undeploy-docker.md)
    - [Quotas d'exécution pour les Runners d'Instance](./runner/quotas.md)
    - [Désactiver les Runner d'instance](./runner/instance-runners-disable.md)

    
---
## Points d'entrée documentation Gitlab :

- Les grands principes : [https://docs.gitlab.com/ee/ci/README.html](https://docs.gitlab.com/ee/ci/README.html)
- ".gitlab-ci.yml" référence : [https://docs.gitlab.com/ee/ci/yaml/](https://docs.gitlab.com/ee/ci/yaml/)
