# How Gitlab Runners Work

---
## Principle

A CI workflow in Gitlab is composed of "jobs" that are executed by Gitlab.

The Gitlab server does not execute the jobs itself but delegates their execution to sub-programs called "Gitlab-runners".

Gitlab-runners can be executed on any machine: a developer's workstation, a cloud instance, a dedicated server, etc.

The Gitlab server orchestrates the execution and determines which Gitlab-runner will be assigned the job.

Each Gitlab-runner regularly contacts the Gitlab server to check if a job has been assigned to it. If so:

- It retrieves all the necessary information for execution
- It executes the job
- Once the processing is complete, it transfers the result back to the Gitlab server

The routing of jobs to runners is done using tags associated with each runner.

When a tag is defined at the job level in a CI workflow, the job can only be executed by a runner that supports this tag.

!!! tip

    It is possible to define runners that can execute jobs for which no tag is defined.

---
## Constraints on Gitlab-runners

Gitlab-runners:

- Must be able to communicate with the Gitlab server via HTTPS
- Must be able to execute the job assigned to them.

And that's all.

---
## Different Levels of Runners

Most Gitlab platforms offer a list of **Instance runners** by default.

These are global runners shared by all projects and managed by the administrators of the Gitlab platform.

It is also possible to define runners dedicated to a project, called **Project runners**. These runners must be implemented by the project managers.

Below are some use cases where "Project Runners" should be used instead of "Instance Runners":

- Insufficient number of runners or resource limitations on jobs
- Confidentiality constraints
- Deployment management by placing runners directly in the environments
- ...

There is a third category, **Group runners**. If a project is part of a group, runners can be defined at the group level and used by all projects belonging to the group. These runners must be implemented by the group managers.

Finally, it is now possible to share a runner between multiple projects. Note that this feature is still recent and may pose runner management issues:

- The runner can only be managed in the project where it was created, and it is not possible to easily find this project in the interface.
- A runner cannot be deactivated as long as it is shared with another project.

!!! tip

    - The use of Instance runners can be blocked for a project, for example, to avoid running incorrectly tagged jobs on unsecured runners.
    - The use of Group runners can be blocked for a project belonging to a group to manage exceptions. This blocking can be implemented at the group level or the project level.

---
# Types of Runners (Executors)

Gitlab-runners can be configured to execute jobs in different types of environments:

- **Shell**: Executes shell commands on the machine hosting the runner. The job execution environment must be installed on this machine. This type of "executor" is generally used for deployment jobs.
- **Docker**: Launches a Docker container on the machine hosting the runner. Only Docker needs to be installed on the machine, and the job execution environment must be available in the image used to launch the container. This type of "executor" is the most versatile and widely used.
- **Kubernetes**: Launches a pod in an existing Kubernetes cluster. This type of "executor" is used in "cloud-native" environments or environments requiring a large number of jobs.
- **Instance**: Interacts with the autoscaling mechanisms provided by major Cloud Providers; AWS, GCP, and Azure.
- **Virtualbox** / **Parallels**: Interacts with hypervisors. The runner connects to a VM to launch the job.
- **Docker autoscaler**: Mechanism to create "pre-configured Docker servers" in the form of VMs or cloud instances. Based on Docker-machine, a solution abandoned by Docker in 2016, but a fork is still maintained by Gitlab.
- **Custom**: Allows the development of a custom "executor" based on scripts that will provision, launch, and delete the job execution environment.
- **SSH**: Connects via SSH to a remote machine. The machine hosting the runner must be able to connect via SSH to the remote machine. If possible, it is preferable to use other types of runners.

!!! tip

    In most cases, Docker-type runners are used
    
- *[https://docs.gitlab.com/runner/executors/index.html](https://docs.gitlab.com/runner/executors/index.html)*

---
## Gitlab.com Runners

The Gitlab.com instance offers default instance runners:

- These are only Docker-type runners
- They run on shared servers
- Available resources are limited
- The number of executions is capped (it is possible to purchase additional execution minutes)

These runners have been used to mine cryptocurrency. For accounts created after May 2021, it is mandatory to enter banking details to use them, even if their use is free.

[https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/)


---
## Resources

- [https://docs.gitlab.com/runner/](https://docs.gitlab.com/runner/)
- [https://docs.gitlab.com/runner/executors/index.html](https://docs.gitlab.com/runner/executors/index.html)

