
# Désactiver les Runner d'instance

Il est parfois nécessaire de ne pas utiliser les runner d'instance, par exemple pour des problématiques de sécurité, ou de gestion des limites de quota.

La désactivation des runner d'instance est paramétrée au niveau du projet.

Pour désactiver l'utiliser des runner d'instance pour un projet:

- Sélectionner le projet
- Ouvrir le menu **Settings > CI/CD > Runner**
- Décocher l'option "Enable instance runners for this project"

![](./img/disable-instance-runner.png)

!!! warning

    Il faut qu'un runner de projet ait été déclaré, et qu'il soit compatible avec les tags affectés aux job du pipeline : soit en mode "Run untagged jobs", soit avec la liste des tags utilisés dans le pipeline. Voir [Configure a project runner](./project-runner-create.md).
