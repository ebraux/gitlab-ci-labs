# Quotas d'exécution pour les Runners d'Instance

---
## Principe

Les gestionnaires d'une plateforme Gitlab peuvent mettre en place des quotas, et limiter le temps d'execution des runners

Par exemple, sur la plateforme Gitlab.com, le temps d'execution du plan gratuit est limité à 400 Unités. Pour utiliser plus de temps d'execution, 2 solutions sont possibles :

- Acheter des minutes supplémentaires
- [Configurer un runner de projet](./project-runner-create.md)

Par défaut, les quotas ne sont pas activés sur les instances "on premise" de Gitlab.

Les quotas ne s'appliquent qu'aux "Instance runner".

---
## Calcul du quota

Les quotas ne sont pas gérés au niveau projet, mais un niveau namespace (user, group).

Le quota est attribué sur un mois fixe, et est remis à zéro tous les 1er du mois.

Le temps d'exécution pris en compte n'est pas celui du pipeline, mais le temps cumulé de tous les jobs qui s'exécutent durant le pipeline.

Lorsque le temps d'exécution atteins 70% de la limite, un message d'information apparaît :

![](./img/quotas-alert.png)

Pour visualiser l'utilisation de son quota, afficher la page de quota, dans le menu **"User Settings > Edit Profile > Usage Quotas"** :

![](./img/quotas-pipelines.png)

Dans cet exemple, le quota de compute a atteint 72%, l'alerte est donc activée.

![](./img/quotas-compute-usage.png)


---
## Utilisation d'un runner de projet

Pour utiliser un runner de projet, et ne plus utiliser utiliser de runner d'instance :

- Selectionner le projet
- Ouvrir le menu **Settings > CI/CD > Runner**
- Décocher l'option "Enable instance runners for this project"

![](./img/disable-instance-runner.png)

!!! warning

    Il faut qu'un runner de projet ait été déclaré, et qu'il soit compatible avec les tags affectés aux job du pipeline : soit en mode "Run untagged jobs", soit avec la liste des tags utilisés dans le pipeline. Voir [Configure a project runner](./project-runner-create.md).




---
## Ressources

- [https://docs.gitlab.com/ee/ci/pipelines/compute_minutes.html](https://docs.gitlab.com/ee/ci/pipelines/compute_minutes.html)