# Déployer un Gitlab-Runner avec Docker

---
## Principe

Dans le cas du déploiement d'un Gitlab-Runner avec Docker, l'image Docker du Gitlab-Runner va permettre de lancer un conteneur Docker, qui aura pour fonctions :

- De contacter le serveur gitlab  pour obtenir la liste des taches à effectuer
- De lancer des conteneurs Docker sur le système local, pour exécuter ces taches

Ce qui impose comme contraintes :

- De configurer le runner qui s'execute dans le conteneur pour qu'il puisse contacter le serveur Gitlab (fichier config.toml)
    - La configuration peut se faire en ligne de commande une fois le conteneur lancé
    - Pour la rendre persistante, il faut attacher un volume au conteneur, pour y stocker la configuration.
- Qu'il puisse avoir accès à un environnement Docker pour lancer des conteneurs
    - Le conteneur doit pouvoir interagir avec le daemon Docker de la machine hôte, en général en ayant accès à la socket `/var/run/docker.sock`
    
    
!!! warning

    Le partage du dossier "/var/run/docker.sock" constitue potentiellement un risque de sécurité.

---
## Pré-requis

### Docker
Docker doit être disponible sur notre machine.

``` bash
docker version
# Client: Docker Engine - Community
#  Version:           27.3.1
```

Si ce n'est pas le cas, installer Docker (*[Documentation officielle](https://docs.docker.com/get-started/get-docker/)*)


### Gitlab Runner authentification token

Un Runner doit être déclaré au niveau du serveur Gitlab, et le "runner authentification token" doit avoir été généré.

Pour déclarer un runner, voir [Déclarer un Runner dans Gitlab](./project-runner-create.md)


---
## Lancer le runner

- Création du volume pour stocker la configuration du runner
``` bash
docker volume create gitlab-runner-01-config
```
- Lancement du runner
``` bash
docker run -d \
    --name gitlab-runner-01 \
    --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-01-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```    

!!! info

    - L'image du runner fait environ 800Mo.
    - Si besoin, remplacer "latest" par la version souhaitée du runner. Pour voir les versions disponibles : [https://hub.docker.com/r/gitlab/gitlab-runner](https://hub.docker.com/r/gitlab/gitlab-runner).

- Vérifier que le conteneur s’exécute correctement
``` bash
docker ps
# CONTAINER ID   IMAGE                         COMMAND                  CREATED          STATUS          PORTS     NAMES
# 60b92265803e   gitlab/gitlab-runner:latest   "/usr/bin/dumb-init …"   11 seconds ago   Up 10 seconds             gitlab-runner-01
```
- Vérifier la configuration des "executor" configurés (aucun dans l'exemple ci dessous)
``` bash
docker exec -it gitlab-runner-01 gitlab-runner list
# Runtime platform                                    arch=amd64 os=linux pid=58 revision=b92ee590 version=17.4.0
# Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
```
- Afficher le contenu du fichier de configuration `config.toml`
``` bash
docker exec gitlab-runner-01 cat /etc/gitlab-runner/config.toml
# concurrent = 1
# check_interval = 0
# connection_max_age = "15m0s"
# shutdown_timeout = 0

# [session_server]
#   session_timeout = 1800
```

---
## Associer le runner à Gitlab

Le runner doit maintenant être associé à un runner déclaré dans Gitlab.

Si besoin pour déclarer un runner, voir [Déclarer un Runner dans Gitlab](./project-runner-create.md)

Au niveau du serveur Gitlab, le runner est déclaré, mais non actif

![](./img/runner-waiting.png)

Pour associer le runner au serveur gitlab, lancer la commande d'enregistrement

``` bash
docker exec gitlab-runner-01  gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --token "glrt-mLdoEKqNRQ_pKwh4b5Px" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "lab-ci-docker-01"

# Runtime platform                                    arch=amd64 os=linux pid=133 revision=b92ee590 version=17.4.0
# Running in system-mode.                            
#                                                   
# Verifying runner... is valid                        runner=mLdoEKqNR
# Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
#
# Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml" 
```

- Afficher le fichier de configuration généré
``` bash
docker exec gitlab-runner-01 cat /etc/gitlab-runner/config.toml
# concurrent = 1
# check_interval = 0
# connection_max_age = "15m0s"
# shutdown_timeout = 0

# [session_server]
#   session_timeout = 1800

# [[runners]]
#   name = "lab-ci-docker-01"
#   url = "https://gitlab.com/"
#   id = 41483272
#   token = "glrt-mLdoEKqNRQ_pKwh4b5Px"
#   token_obtained_at = 2024-09-24T13:07:03Z
#   token_expires_at = 0001-01-01T00:00:00Z
#   executor = "docker"
#   [runners.custom_build_dir]
#   [runners.cache]
#     MaxUploadedArchiveSize = 0
#     [runners.cache.s3]
#     [runners.cache.gcs]
#     [runners.cache.azure]
#   [runners.docker]
#     tls_verify = false
#     image = "alpine:latest"
#     privileged = false
#     disable_entrypoint_overwrite = false
#     oom_kill_disable = false
#     disable_cache = false
#     volumes = ["/cache"]
#     shm_size = 0
#     network_mtu = 0
```

- Vérifier dans l'interface : le runner est désormais actif

![](./img/runner-activated.png)

 
---
## Ressources

- [https://docs.gitlab.com/runner/install/docker.html](https://docs.gitlab.com/runner/install/docker.html)
- [https://docs.gitlab.com/runner/register/index.html?tab=Docker](https://docs.gitlab.com/runner/register/index.html?tab=Docker)
