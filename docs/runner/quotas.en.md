# Execution Quotas for Instance Runners

---
## Introduction

The managers of a Gitlab platform can set up quotas to limit the execution time of runners.

For example, on the Gitlab.com platform, the execution time for the free plan is limited to 400 units. To use more execution time, two solutions are possible:

- Purchase additional minutes
- [Configure a project runner](./project-runner-create.md)

By default, quotas are not enabled on "on-premise" instances of Gitlab.

Quotas only apply to "Instance runners".

---
## Quota Calculation

Quotas are not managed at the project level but at the namespace level (user, group).

The quota is allocated on a fixed month and is reset every 1st of the month.

The execution time taken into account is not that of the pipeline but the cumulative time of all jobs that run during the pipeline.

When the execution time reaches 70% of the limit, an information message appears:

![](./img/quotas-alert.png)

To view the quota usage, display the quota page in the menu **"User Settings > Edit Profile > Usage Quotas"**:

![](./img/quotas-pipelines.png)

In this example, the compute quota has reached 72%, so the alert is activated.

![](./img/quotas-compute-usage.png)

---
## Using a Project Runner

To use a project runner and no longer use an instance runner:

- Select the project
- Open the menu **Settings > CI/CD > Runner**
- Uncheck the option "Enable instance runners for this project"

![](./img/disable-instance-runner.png)

!!! warning

    A project runner must have been declared and must be compatible with the tags assigned to the pipeline jobs: either in "Run untagged jobs" mode or with the list of tags used in the pipeline. Se [Configure a project runner](./project-runner-create.md)

---
## Resources

- [https://docs.gitlab.com/ee/ci/pipelines/compute_minutes.html](https://docs.gitlab.com/ee/ci/pipelines/compute_minutes.html)