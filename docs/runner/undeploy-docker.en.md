# Remove a Runner deployed with Docker

To remove a runner, you must first identify the container on which it is running:

- Identify the container(s) used for the runner. For example, here `gitlab-runner-01`.
``` bash
docker ps
# CONTAINER ID   IMAGE                         COMMAND                  CREATED      STATUS        PORTS     NAMES
# 60b92265803e   gitlab/gitlab-runner:latest   "/usr/bin/dumb-init …"   1 days ago   Up 25 hours             gitlab-runner-01
```

- List the active "executors" to validate the "executor" to unregister
``` bash
docker exec -it gitlab-runner-01 gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=20 revision=b92ee590 version=17.4.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
lab-ci-docker-01                                    Executor=docker Token=glrt-mLdoEKqNRQ_pKwh4b5Px URL=https://gitlab.com/
```

- Unregister the "executor"
``` bash
docker exec -it gitlab-runner-01 gitlab-runner unregister   --url https://gitlab.com  --token glrt-mLdoEKqNRQ_pKwh4b5Px
# Runtime platform                                    arch=amd64 os=linux pid=110 revision=b92ee590 version=17.4.0
# Running in system-mode.                            
                                                   
# Unregistering runner from GitLab succeeded          runner=9kKyfkYxf
# Updated /etc/gitlab-runner/config.toml             
```

- Check that it no longer appears in the list of active "executors"
``` bash
docker exec -it gitlab-runner-01 gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=20 revision=b92ee590 version=17.4.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
```


In the interface, the runner appears as unassociated

![](./img/runner-unregister.png)


!!! info

    The runner can be associated again later if needed



- If there is no other "executor", delete the container
``` bash
docker rm -f gitlab-runner-01
```


---
## Resources

- [https://docs.gitlab.com/runner/register/index.html?tab=Docker](https://docs.gitlab.com/runner/register/index.html?tab=Docker)