# Disable Instance Runners

It is sometimes necessary to not use instance runners, for example, for security issues or managing quota limits.

The deactivation of instance runners is configured at the project level.

To disable the use of instance runners for a project:

- Select the project
- Open the menu **Settings > CI/CD > Runner**
- Uncheck the option "Enable instance runners for this project"

![](./img/disable-instance-runner.png)

!!! warning

    A project runner must have been declared and must be compatible with the tags assigned to the pipeline jobs: either in "Run untagged jobs" mode or with the list of tags used in the pipeline. See [Configure a project runner](./project-runner-create.md).


