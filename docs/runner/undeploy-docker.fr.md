# Supprimer un Runner déployé avec Docker



Pour pouvoir supprimer un runner, il faut tout d'abord identifier le conteneur sur lequel il est executé :

- Identifier le ou les conteneur(s) utilisé(s) pour le runner. Par exemple ici `gitlab-runner-01`.
``` bash
docker ps
# CONTAINER ID   IMAGE                         COMMAND                  CREATED      STATUS        PORTS     NAMES
# 60b92265803e   gitlab/gitlab-runner:latest   "/usr/bin/dumb-init …"   1 days ago   Up 25 hours             gitlab-runner-01
```

- Lister les "executors" actifs, pour valider l'"executor" à dés-enregistrer
``` bash
docker exec -it gitlab-runner-01 gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=20 revision=b92ee590 version=17.4.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
lab-ci-docker-01                                    Executor=docker Token=glrt-mLdoEKqNRQ_pKwh4b5Px URL=https://gitlab.com/
```

- Dés-enregistrer l'"executor"
``` bash
docker exec -it gitlab-runner-01 gitlab-runner unregister   --url https://gitlab.com  --token glrt-mLdoEKqNRQ_pKwh4b5Px
# Runtime platform                                    arch=amd64 os=linux pid=110 revision=b92ee590 version=17.4.0
# Running in system-mode.                            
                                                   
# Unregistering runner from GitLab succeeded          runner=9kKyfkYxf
# Updated /etc/gitlab-runner/config.toml             
```

- Vérifier qu'il n’apparaît plus dans la liste des "executor" actifs
``` bash
docker exec -it gitlab-runner-01 gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=20 revision=b92ee590 version=17.4.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
```


Dans l'interface le runner apparaît comme non associé

![](./img/runner-unregister.png)

!!! info

    Le runner peut être à nouveau associé par la suite si besoin



- Si il n'y a pas d'autre "executor", supprimer le conteneur
``` bash
docker rm -f gitlab-runner-01
```


---
## Ressources

- [https://docs.gitlab.com/runner/register/index.html?tab=Docker](https://docs.gitlab.com/runner/register/index.html?tab=Docker)