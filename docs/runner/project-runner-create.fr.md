# Déclarer un Runner dans Gitlab


---
## Principe

- Déclarer un runner via l'interface de Gitlab, et obtenir un token spécifique à ce runner.
- Puis ensuite lancer une commande au niveau d'un Gitlab runner local, pour l'associer au runner déclaré au niveau du serveur.

Il faut avoir le rôle "Maintainer" du projet.

!!! note

    - Les "Gitlab Runner Authentication Token" sont préfixé par "glrt-". Par exemple `glrt-9kKyfkYxf6HZthoabc4C`
    - Avant la version 15.6, les Runner utilisaient un mécanisme de "runner registration token" pour s'authentifier. Le token était géré au niveau du projet ou groupe. Il faut désormais utiliser le mécanisme de "runner authentication token". Le token est spécifique à chaque runner.


---
## Déclaration d'un runner

- Dans le Menu **"Settings > CI/CD"** :
- Ouvrir la rubrique concernant les **"Runners"** :
- La liste des runners disponibles s'affiche :
    - Dans le cas présent, on peut voir la liste des runners proposés par l'instance gitlab.com. Aucun runner n'est associé au projet

![](./img/runner-list.png)



- Dans la version actuelle il est toujours possible d'utiliser des "runner registration token", mais c'est déconseillé :

![](./img/runner-deprecated-registration-token.png)

- Créer un runner de projet

![](./img/runner-new.png)

- Configurer le runner :
    - Pour l'instant aucun tag n'est utilisé. Le runner est un runner générique qui accepte tous les job (run untagged jobs)
    - Le runner est dédié à ce projet. Il ne peut pas être utilisé par d'autres projets (Lock to current project).

![](./img/runner-config.png)

- Une fois le runner créé au niveau du serveur Gitlab, Gitlab affiche les informations permettant de configurer le runner en local, en fonction des différents systèmes

![](./img/runner-registration-infos.png)

!!! warning

    L'information la plus importante est le **"Gitlab Runner Authentication Token"**, qu'il faut conserver précieusement, car il est indispensable pour configurer le runner, et ne pourra pas être affiché à nouveau par la suite :

![](./img/runner-registration-command.png)


Le runner est désormais créé au niveau du serveur Gitlab, en attendant qu'un gitlab-runner correspondant soit enregistré :

![](./img/runner-waiting.png)

Pour Déployer un Gitlab-Runner, voir

-  Le [mode opératoire en utilisant Docker](./deploy-docker.md)
-  La [documentation officielle de Gitlab](https://docs.gitlab.com/runner/install/)


---
## Ressources

- [https://docs.gitlab.com/ee/ci/runners/runners_scope.html#create-a-project-runner-with-a-runner-authentication-token](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#create-a-project-runner-with-a-runner-authentication-token)
- [https://docs.gitlab.com/runner/register/index.html?tab=Docker](https://docs.gitlab.com/runner/register/index.html)
