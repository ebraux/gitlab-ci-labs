# Principe de fonctionnement des runners Gitlab


---
## Principe

Un Worflow de CI dans Gitlab, est composé de "jobs", qui sont exécutés par Gitlab.

Le serveur Gitlab n'exécute pas lui même les jobs, mais il confie leur exécution à des sous-programmes, les "Gitlab-runner".

Les gitlab-runner peuvent être exécutés sur n'importe quelle machine : un poste de développeur, une instance cloud, un serveur dédié, ...

Le serveur Gitlab orchestre l'exécution et défini à quel gitlab-runner va être confiée le job.

Chaque gitlab-runner vient contacter régulièrement le serveur Gitlab, pour définir si un job lui a été attribué. Si c'est le cas :

- Il récupère toutes les informations nécessaires pour l'execution
- Il exécute le job
- Puis une fois le traitement terminé, il transfère le résultat vers le serveur Gitlab


L'aiguillage des jobs vers les runners se fait au moyen de tags associés à chaque runner.

Quand un tag est défini au niveau d'un job dans un Workflow de CI, le job ne pourra être exécuté que par un runner prenant en charge ce tag.

!!! tip

    Il est possible de définir des runners qui peuvent exécuter les jobs pour lesquels aucun tag n'est défini.


---
## Contraintes sur les gitlab-runner


Les gitlab-runner :

- Doivent pouvoir communiquer avec le serveur Gitlab en https
- Doivent pouvoir exécuter le job qui leur est confié.

Et c'est tout.

---
## Différents niveaux de Runner

La plupart des plateformes Gitlab proposent par défaut une liste d' **Instance runners**.

Ce sont des runners globaux, partagés par tous les projets, et gérés par les administrateurs de la plateforme Gitlab.

Il est également possible de définir des runners dédiés à un projet, les **Project runners**. Ces runners doivent être mis en oeuvre par les gestionnaires du projet.

Ci dessous, quelques cas d'usage ou des "Project Runners" doivent être utilisés à la place des "Instance Runner"  :

- Nombre insuffisants de runners, ou limitation des ressources sur les jobs
- Contraintes de confidentialité
- Gestion des déploiements en plaçant les runners directement dans les environnements
- ...


Il existe une 3ème catégorie, les **Group runners**. Si un projet fait partie d'un groupe, des runners peuvent être définis au niveau de ce groupe et être utilisés par l'ensemble des projets appartenant au groupe. Ces runners doivent être mis en oeuvre par les gestionnaires du groupe.


Enfin, il est désormais possible de partager un runner entre plusieurs projets. Attention cette fonctionnalité est encore récente, et peut poser des problèmes de gestion des runners :

- Le runner ne peut être géré que dans le projet dans lequel il a été créé, et il n'est pas possible de retrouver simplement ce projet dans l'interface.
- Un runner ne peut pas être désactivé tant qu'il est partagé avec un autre projet.

!!! tip

    - L'utilisation des Instance runners peut être bloquée pour un projet, afin par exemple d'éviter de lancer des jobs incorrectement tagués sur des runner non sécurisés.
    - L'utilisation des Group runners peut être bloquée pour un projet appartenant à un groupe, afin de gérer des exceptions. Ce blocage peut être mis en place au niveau du groupe, ou au niveau du projet.

---
# Types de runners (Executors)

Les gitlab-runner peuvent être configurés pour exécuter les jobs dans différents types d'environnements :

- **Shell** : exécute des commandes shell sur la machine hébergeant le runner. L'environnement d'execution du job doit être installé sur cette machine. Ce type d'"executor" est généralement utilisé pour des jobs de déploiement
- **Docker** : lancement d'un conteneur Docker sur la machine hébergeant le runner. Seul Docker doit être installé sur la machine, l'environnement d'exécution du job doit être disponible dans l'image utilisée pour lancer le conteneur. Ce type d'"executor" ets le plus polyvalent, et le plus utilisé.
- **Kubernetes** : lancement de pod dans un cluster Kubernetes existant. Ce type d'"executor" est utilisé dans les environnements "cloud native", ou dans des environnements nécessitant un grand nombre de jobs.
- **Instance** : interaction avec les mécanismes d'autoscaling proposés par les principaux Cloud Provider : AWS, GCP, et Azure
- **Virtualbox** / **Parallels** : interaction avec les hyperviseurs. Le runner va se connecter à une VM pour lancer le job.
- **Docker autoscaller** : mécanisme permettant de créer des "serveurs docker pré-configurés", sous forme de VM ou d'instance cloud. Basé sur Docker-machine, solution abandonnée par Docker en 2016, mais un fork est toujours maintenu par Gitlab.
- **Custom** : permet le développement d'un "executor" personnalisé, basé sur des scripts qui vont provisionner, lancer et supprimer l'environnement d'execution du job.
- **SSH** : connexion via SSH à une machine distante. La machine hébergeant le runner doit pouvoir se connecter en ssh à la machine distante. Dans la mesure du possible, il est préférable d'utiliser d'autres type de runner.

!!! tip

    Dans la majorité des cas, on utilise des runners de type Docker 
    
    
- *[https://docs.gitlab.com/runner/executors/index.html](https://docs.gitlab.com/runner/executors/index.html)*


---
## Les runner de gitlab.com

L'instance Gitlab.com propose des runners d'instance par défaut : 

- Ce sont uniquement des runners de type "Docker"
- Ils sont exécutés sur des serveurs mutualisés
- Les ressources disponibles sont limitées
- Le nombre d'execution est plafonné (il est possible d'acheter des minutes d'execution supplémentaires)

Ces runners ont été utilisés pour miner de la crypto monnaie. Pour les comptes créés après mai 2021 il est obligatoire de saisir des coordonnées bancaires pour pouvoir les utiliser, même si leur utilisation est gratuite.

[https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/)


---
## Ressources

- [https://docs.gitlab.com/runner/](https://docs.gitlab.com/runner/)
- [https://docs.gitlab.com/runner/executors/index.html](https://docs.gitlab.com/runner/executors/index.html)
