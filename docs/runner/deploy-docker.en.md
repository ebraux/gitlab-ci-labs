# Deploy a Runner with Docker

---
## Principle

In the case of deploying a Gitlab-Runner with Docker, the Docker image of the Gitlab-Runner will allow launching a Docker container, which will have the following functions:

- Contact the Gitlab server to obtain the list of tasks to perform
- Launch Docker containers on the local system to execute these tasks

This imposes the following constraints:

- Configure the runner that runs in the container so that it can contact the Gitlab server (config.toml file)
    - The configuration can be done via the command line once the container is launched
    - To make it persistent, a volume must be attached to the container to store the configuration.
- It must have access to a Docker environment to launch containers
    - The container must be able to interact with the Docker daemon of the host machine, generally by having access to the `/var/run/docker.sock` socket.

!!! warning

    Sharing the "/var/run/docker.sock" folder potentially constitutes a security risk.

---
## Prerequisites

### Docker
Docker must be available on our machine.
``` bash
docker version
# Client: Docker Engine - Community
#  Version:           27.3.1
```

If not, install Docker (*[Official Documentation](https://docs.docker.com/get-started/get-docker/)*)


### Gitlab Runner authentication token

A Runner must be registered on the Gitlab server, and the "runner authentication token" must have been generated.

To register a runner, see [Register a Runner in Gitlab](./project-runner-create.md)


---
## Launch the runner

- Create the volume to store the runner's configuration
``` bash
docker volume create gitlab-runner-01-config
```
- Launch the runner
``` bash
docker run -d \
    --name gitlab-runner-01 \
    --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-01-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```    

!!! info

    - The runner image is about 800MB.
    - If needed, replace "latest" with the desired version of the runner. To see the available versions: [https://hub.docker.com/r/gitlab/gitlab-runner](https://hub.docker.com/r/gitlab/gitlab-runner).

- Check that the container is running correctly
``` bash
docker ps
# CONTAINER ID   IMAGE                         COMMAND                  CREATED          STATUS          PORTS     NAMES
# 60b92265803e   gitlab/gitlab-runner:latest   "/usr/bin/dumb-init …"   11 seconds ago   Up 10 seconds             gitlab-runner-01
```
- Check the configuration of the "executors" configured (none in the example below)
``` bash
docker exec -it gitlab-runner-01 gitlab-runner list
# Runtime platform                                    arch=amd64 os=linux pid=58 revision=b92ee590 version=17.4.0
# Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
```
- Display the content of the `config.toml` configuration file
``` bash
docker exec gitlab-runner-01 cat /etc/gitlab-runner/config.toml
# concurrent = 1
# check_interval = 0
# connection_max_age = "15m0s"
# shutdown_timeout = 0

# [session_server]
#   session_timeout = 1800
```

---
## Associate the runner with Gitlab

The runner must now be associated with a runner declared in Gitlab.

If needed to declare a runner, see [Register a Runner in Gitlab](./project-runner-create.md)

At the Gitlab server level, the runner is declared but not active

![](./img/runner-waiting.png)

To associate the runner with the Gitlab server, run the registration command

``` bash
docker exec gitlab-runner-01  gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --token "glrt-mLdoEKqNRQ_pKwh4b5Px" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "lab-ci-docker-01"

# Runtime platform                                    arch=amd64 os=linux pid=133 revision=b92ee590 version=17.4.0
# Running in system-mode.                            
#                                                   
# Verifying runner... is valid                        runner=mLdoEKqNR
# Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
#
# Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml" 
```

- Display the generated configuration file
``` bash
docker exec gitlab-runner-01 cat /etc/gitlab-runner/config.toml
# concurrent = 1
# check_interval = 0
# connection_max_age = "15m0s"
# shutdown_timeout = 0

# [session_server]
#   session_timeout = 1800

# [[runners]]
#   name = "lab-ci-docker-01"
#   url = "https://gitlab.com/"
#   id = 41483272
#   token = "glrt-mLdoEKqNRQ_pKwh4b5Px"
#   token_obtained_at = 2024-09-24T13:07:03Z
#   token_expires_at = 0001-01-01T00:00:00Z
#   executor = "docker"
#   [runners.custom_build_dir]
#   [runners.cache]
#     MaxUploadedArchiveSize = 0
#     [runners.cache.s3]
#     [runners.cache.gcs]
#     [runners.cache.azure]
#   [runners.docker]
#     tls_verify = false
#     image = "alpine:latest"
#     privileged = false
#     disable_entrypoint_overwrite = false
#     oom_kill_disable = false
#     disable_cache = false
#     volumes = ["/cache"]
#     shm_size = 0
#     network_mtu = 0
```

- Check in the interface: the runner is now active

![](./img/runner-activated.png)


---
## Resources


- [https://docs.gitlab.com/runner/install/docker.html](https://docs.gitlab.com/runner/install/docker.html)
- [https://docs.gitlab.com/runner/register/index.html?tab=Docker](https://docs.gitlab.com/runner/register/index.html?tab=Docker)
