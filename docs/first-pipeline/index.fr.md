# Lancer un premier Pipeline

## Objectif ##

- Créer un pipeline de CI pour afficher "Hello world" et l'heure.
- Observer le résultat dans l'interface de Gitlab.

---
## Initialiser la chaîne de CI

### Accéder à l'éditeur

Dans votre projet, lancer l'éditeur de CI, via le menu "Build / Pipeline Editor":

![](img/ci-cd_editor.png)

Puis initialiser le workflow de CI/CD :

![](img/ci-cd_new-pipeline.png)


L'interface de configuration de pipeline s'affiche, contenant un squelette de pipeline pré-renseigné par Gitlab. Il est composé de jobs affichant des messages, et correspond à un cas d'usage standard, basé sur des phases de build/test/deploy : [default-gitlab.yml](./default-gitlab.yml).

Pour ce lab, un pipeline plus basique va être utilisé.


### Écrire le pipeline

Remplacer le pipeline par défaut par un job simple, permettant d'afficher "Hello World", et la date :
```yaml
job_test:
  stage: test
  script:
    - echo "Hello world"
    - date
```
Source : [hello_gitlab-ci.yml](hello_gitlab-ci.yml)

![](img/pipeline_hello.png)

Détail du code :

- Création d'un job "job_test"
- Utilisation du stage "test" , un des 3 stages par défaut (test, build, deploy)
- Lancement des commandes linux `echo "Hello world"` et `date`.

Plus d'informations :

- stages : [(https://docs.gitlab.com/ee/ci/yaml/#stages](https://docs.gitlab.com/ee/ci/yaml/#stages))
- jobs : [https://docs.gitlab.com/ee/ci/jobs/](https://docs.gitlab.com/ee/ci/jobs/)).
- script : [https://docs.gitlab.com/ee/ci/yaml/#script](https://docs.gitlab.com/ee/ci/yaml/#script))


### Vérifier le Pipeline

L'onglet "Validate" permet de valider la syntaxe du code qui a été saisi, propose une vision plus structurée du pipeline, et donne des informations complémentaires sur les jobs.

![](img/pipeline-validate-1.png)

![](img/pipeline-validate-2.png)


L'onglet Visualize permet de visualiser le contenu du pipeline. Ici, le stage "test", contient le job "job_test".

![](img/pipeline_visualize.png)

### Lancer le pipeline

Une fois le texte saisi et vérifié, revenir sur l'onglet "Edit", et cliquer  sur le bouton "Commit changes".

Cette action va intégrer la modification du fichier `.gitlab-ci.yml` dans le dépôt git (commit+push) sur la branche `main`, ce qui va déclencher le pipeline.

![](img/pipeline_commit.png)

Le status du pipeline évolue, en fonction de l'execution du job :

![](img/pipeline_status.png)

![](img/pipeline_running.png)

!!! info

    A cette étape, il est possible d'aller observer les détails du pipeline, en cliquant sur "View pipeline". 
    ![](img/pipeline_running-status.png)


---
## Déroulement du pipeline

### Suivi du déroulement du pipeline

Observer se résultat de l'execution de la chaîne de CI dans Gitlab

Depuis le menu **Build**, afficher les **Pipelines** :

![](img/pipeline-menu.png)

Liste des Pipelines: 

![](img/pipeline-status_passed.png)

Cliquer le pipeline qui vient d'être exécuté. Ce pipeline contient le job "job_test" du stage "Test".

![](img/pipeline-status_job_test.png)

Et enfin sur le job "job_test", observer le résultat des commandes :

![](img/pipeline-output_hello.png)

### Observer le traitement des jobs

Dans le résultats du job "job_test", avant les commandes placées dans le fichier de Manifest, on retrouve 2 étapes principales :

- Le lancement d'un "runner"

![](img/pipeline-output_runner.png)

- Le clone du dépôt, avant de réaliser les traitements
 
![](img/pipeline-output_git-clone.png)

Ainsi que des étapes secondaires (dans le cas présent):

- Préparation de l'environnement avant de cloner le dépôt

![](img/pipeline-output_preparing.png)

- Execution d'un script de prétraitement

![](img/pipeline-output_pre-script.png)

- Et le ménage à la fin de l'execution du job

![](img/pipeline-output_cleanup.png)

### Observer les runners

Dans la partie concernant le runner, on retrouve l'Id du runner qui a lancé le job
```bash
Running with gitlab-runner 17.4.0~pre.110.g27400594 (27400594)
...
```

Ainsi que le type, et autres informations sur le Runner
```bash
Preparing the "docker+machine" executor
Using Docker executor with image ruby:3.1 ...
...
```

!!! note

    - La liste des Runners disponibles se trouve dans la partie CI/CD des "settings" du projets.

