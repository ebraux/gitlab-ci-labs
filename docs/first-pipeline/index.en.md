# Launch a First Pipeline

## Objective ##

- Create a CI pipeline to display "Hello world" and the current time.
- Observe the result in the Gitlab interface.

---
## Initialize the CI chain

### Access the editor

In your project, launch the CI editor via the "Build / Pipeline Editor" menu:

![](img/ci-cd_editor.png)

Then initialize the CI/CD workflow:

![](img/ci-cd_new-pipeline.png)

The pipeline configuration interface appears, containing a pipeline skeleton pre-filled by Gitlab. It consists of jobs displaying messages and corresponds to a standard use case based on build/test/deploy phases  : [default-gitlab.yml](./default-gitlab.yml).

For this lab, a more basic pipeline will be used.

### Write the pipeline

Replace the default pipeline with a simple job to display "Hello World" and the date:
```yaml
job_test:
  stage: test
  script:
    - echo "Hello world"
    - date
```
Source : [hello_gitlab-ci.yml](hello_gitlab-ci.yml)

![](img/pipeline_hello.png)

Code details:

- Creation of a "job_test" job
- Use of the "test" stage, one of the 3 default stages (test, build, deploy)
- Execution of the Linux commands `echo "Hello world"` and `date`.

More information:

- stages: [https://docs.gitlab.com/ee/ci/yaml/#stages](https://docs.gitlab.com/ee/ci/yaml/#stages)
- jobs: [https://docs.gitlab.com/ee/ci/jobs/](https://docs.gitlab.com/ee/ci/jobs/)
- script: [https://docs.gitlab.com/ee/ci/yaml/#script](https://docs.gitlab.com/ee/ci/yaml/#script)

### Check the Pipeline

The "Validate" tab allows you to validate the syntax of the entered code, offers a more structured view of the pipeline, and provides additional information about the jobs.

![](img/pipeline-validate-1.png)

![](img/pipeline-validate-2.png)

The Visualize tab allows you to visualize the content of the pipeline. Here, the "test" stage contains the "job_test" job.

![](img/pipeline_visualize.png)

### Launch the pipeline

Once the text is entered and verified, return to the "Edit" tab and click the "Commit changes" button. This action will integrate the modification of the .gitlab-ci.yml file into the git repository (commit+push) on the branch `main`, which will trigger the pipeline.

![](img/pipeline_commit.png)

The pipeline status evolves:

![](img/pipeline_status.png)

![](img/pipeline_running.png)

> At this stage, it is possible to observe the details of the pipeline by clicking on "View pipeline".

![](img/pipeline_running-status.png)

---
## Pipeline Execution

### Monitoring the pipeline execution

Observe the result of the CI chain execution in Gitlab.

From the **Build** menu, display the **Pipelines**:

![](img/pipeline-menu.png)

List of Pipelines:

![](img/pipeline-status_passed.png)

Click the pipeline that has just been executed. This pipeline contains the "job_test" job of the "Test" stage.

![](img/pipeline-status_job_test.png)

And finally, on the "job_test" job, observe the result of the commands:

![](img/pipeline-output_hello.png)

### Observe job processing

In the results of the "job_test" job, before the commands placed in the Manifest file, 2 main steps can be found :

- The launch of a "runner"

![](img/pipeline-output_runner.png)

- The clone of the repository before performing the processing

![](img/pipeline-output_git-clone.png)

As well as secondary steps (in our case):

- Preparing the environment before cloning the repository

![](img/pipeline-output_preparing.png)

- Execution of a pre-processing script

![](img/pipeline-output_pre-script.png)

- And cleanup at the end of the job execution

![](img/pipeline-output_cleanup.png)

### Observe the runners

In the part concerning the runner, we find the Id of the runner that launched the job:
```bash
Running with gitlab-runner 17.4.0~pre.110.g27400594 (27400594)
...
```

As well as the type and other information about the Runner:
```bash
Preparing the "docker+machine" executor
Using Docker executor with image ruby:3.1 ...
...
```

!!! note

  - The list of available Runners can be found in the CI/CD section of the project's "settings".

