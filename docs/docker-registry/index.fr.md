
# La registry Docker Intégrée

---
## Principe

Gitlab met à disposition de chaque projet une registry Docker.

Elle est en général utilisée pour publier des images Docker générée depuis un pipeline de CI/CD, mais elle peut être utilisée en dehors d'un pipeline.

Cette registry peut être publique, ou privée. Dans le cas d'une registry privée, il est possible de générer des token d'accès, en lecture/écriture, ou en lecture seule.

La registry est accessible via le menu "**Deploy > Container Registry**"

![](./img/menu_container-registry.png)

Tant qu'aucune image n'a été ajoutée, Gitlab affiche les commande à utiliser pour ajouter manuellement une image à la registry :
``` bash
docker login registry.gitlab.com
docker build -t registry.gitlab.com/user1/lab-ci .
docker push registry.gitlab.com/user1/lab-ci
```

Pour faciliter son utilisation dans la chaîne de CI/CD, des variables prédéfinies sont disponibles dans l'environnement d'execution des jobs : 

- `$CI_REGISTRY` : adresse de la registry. Dans l'instance gitlab.com, contient `registry.gitlab.com`
- `$CI_REGISTRY_IMAGE` : par défaut contient "$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME", par exemple  `registry.gitlab.com/user1/lab-ci`
- `$CI_REGISTRY_USER` : le nom du token utilisé par la CI pour accéder à la registry. par défaut `gitlab-ci-token`
- `$CI_REGISTRY_PASSWORD` : le mot de passe associé au token

Exemple d'utilisation dans un job :

``` yaml
build:docker-image:
  stage: build
  image: docker:stable
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE .
    - docker push $CI_REGISTRY_IMAGE
```

!!! warning

    La commande Docker est exécutée dans le conteneur, il faut donc utiliser une image contenant `docker`, ici `docker:stable`.


---
## Construire une image Docker dans un job s'exécutant avec Docker

### Principe

Docker est un outil fonctionnant en mode client-serveur : 

- L'utilisateur interagit avec le client, en ligne de commande, ou graphique
- Le client interagit avec le serveur

Construire une image Docker, consiste à lancer la commande `docker build ...` avec le client, qui va se connecter au serveur pour construire l'image.

La partie `script` d'un job s'exécute dans le conteneur lancé par le runner. C'est elle qui va lancer la commande `docker build`, il faut donc utiliser une image source dans laquelle le client Docker est disponible.

Mais il faut ensuite que ce client puisse se connecter à un serveur Docker, pour réaliser l'opération de build.


### Docker in Docker

La solution la plus répandue consiste à lancer un serveur Docker en tant que `service`, en utilisant une image Docker in Docker (DinD).
``` yaml
  services:
    - docker:stable-dind
```
Cette solution nécessite que le runner exécute Docker en mode `privileged` (accès au matériel, au noyau, aux fichiers, ... ), ce qui n'est pas toujours le cas.

Si le runner n'est pas configuré en mode `privileged`, le message apparait dans les logs du job :
``` bash
error during connect: Post "http://docker:2375/v1.24/auth": dial tcp: lookup docker on xxx.xxx.xxx.xxx:53: no such host
```

Avec cette solution, puis que le service est associé au job, l'environnement Docker est réinitialisé à chaque lancement du job. C'est un avantage, car l'environnement est toujours dans le même état. Par contre les images utilisées doivent être rechargée à chaque job, ce qui peut poser des problème de performance dans le cas d'utilisation d'images volumineuse (code quality par exemple).


### Inception : s'appuyer sur le serveur Docker de l'hôte

Une autre solution consiste à permettre au conteneur client d'interagir directement avec le serveur Docker s'exécutant sur la machine hôte.

Il faut dans ce cas que le conteneur puisse avoir accès au fichier `/var/run/docker.sock`, une socket Unix qui permet la communication entre le client Docker et le démon Docker.

Exemple de configuration du runner dans ce cas :
``` yaml
[runners.docker]
  ...
  # Image Docker pour exécuter les jobs
  image = "docker:latest"
  # Activer le mode privilégié (important)
  privileged = true
  # Monter la socket Docker
  volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
```

Avec cette configuration, le client dispose d'un contrôle complet sur le serveur Docker, et peut donc par exemple lancer des conteneurs en mode `provileged`, ce qui constitue un risque de sécurité plus important qu'avec DinD.

Cette solution a toutefois l'avantage de pourvoir conserver les images chargées entre les jobs, ce qui peut faire gagner un temps précieux.

### Outil de build dédiés

Il existe également des outils indépendants de docker, permettant de construire une image sans la partie serveur de Docker, comme par exemple [buildah](https://buildah.io/), [kaniko](https://github.com/GoogleContainerTools/kaniko), ou [podman](https://podman.io/)



---
## Publication d'une image dans la registry

Objectif : créer une image dans la Registry Docker du projet, a partir de la CI de Gitlab


### Créer un runner en mode `privileged` :

Pour créer un runner en mode `privileged` :
``` bash
docker exec gitlab-runner-01  gitlab-runner register \
    --non-interactive \
    --url "__GITLAB_URL___" \
    --token "__RUNNER_TOKEN__" \
    --executor "docker" \
    --docker-privileged \
    --docker-image alpine:latest \
    --description "docker-privilegied"
```    

Il faut ensuite ajouter un tag au job
``` yaml

build:docker:
  ...
  tags:
    - privileged
  ...
```


### Créer un fichier Dockerfile

A partir d'une image Ubuntu. Par exemple ajouter le package "cowsay". 

*Exemple de [Dockerfile](Dockerfile)*

![](./img/dockerfile-create.png)

![](./img/dockerfile-commit.png)


### Intégration à la CI

- Utilisation d'un runner de type Docker, et du service docker-in-docker (dind)
- Utilisation des variable de CI : `$CI_REGISTRY`, `$CI_REGISTRY_USER`, `$CI_REGISTRY_PASSWORD` et `$CI_REGISTRY_IMAGE`

*[Exemple de solution .gitlab-ci.yaml](cowsay_gitlab-ci.yml)*

Une fois le job exécuté,l'image est publiée dans la registry du projet.

![](./img/container-registry-list.png)

![](./img/container-registry-detail.png)


---
## Ressources Complémentaires: 

- [https://docs.gitlab.com/ee/user/packages/container_registry/](https://docs.gitlab.com/ee/user/packages/container_registry/)
- [https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-the-docker-image-docker-in-docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-the-docker-image-docker-in-docker)
- [https://hub.docker.com/_/docker](https://hub.docker.com/_/docker)
- [http://memorandom.blog/builder-simplement-des-images-docker-avec-gitlab-ci-sans-dind/](http://memorandom.blog/builder-simplement-des-images-docker-avec-gitlab-ci-sans-dind/)