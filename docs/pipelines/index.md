# Organisaer les pipeline : jobs et stages


---
## Les jobs

Les jobs sont les éléments de base de GitLab CI. Tout traitement exécuté par un pipeline de CI/CD passe par un job.

De très nombreuses options sont disponibles pour ajuster le fonctionnement des jobs, leur environnement, ... 

- [https://docs.gitlab.com/ee/ci/yaml/#job-keywords](https://docs.gitlab.com/ee/ci/yaml/#job-keywords)
- [https://docs.gitlab.com/ee/ci/yaml/#script](https://docs.gitlab.com/ee/ci/yaml/#script)


!!! info

    - Un pipeline de CI/CD doit contenir au moins un job
    - Un job doit contenir au moins un élément "script" à exécuter


---
## Les stages 

Les jobs sont organisés en "stages". 

3 stages sont définis par défaut dans Gitlab : test, build, deploy, mais des stages personnalisés peuvent être définis simplement.

Les stages s'exécutent de façon séquentielle, mais les jobs dans les stages s'exécutent en parallèle.
  
*[https://docs.gitlab.com/ee/ci/yaml/#stages](https://docs.gitlab.com/ee/ci/yaml/#stages)*


---
## Lab #1 : Création d'un pipeline Multi-jobs 

Créer un pipeline comportant un "stage" "test", qui comprend 3 "jobs" :

- test_a : exécutant la commande `echo "Hello JOB 1"`
- test_b : exécutant la commande `echo "Hello JOB 2"`
- test_c : exécutant la commande `echo "Hello JOB 3"`

Solution : [jobs_gitlab-ci](jobs_gitlab-ci.yml)

![](img/pipelines-jobs.png)


---
## Lab #2 : Création d'un pipeline Multi-stages

Créer un fichier de CI qui exécute 3 jobs de "build", "test" et "deploy" de façon séquentielle.

Solution : [stages_gitlab-ci](stages_gitlab-ci.yml)

![](img/pipelines-stages.png)

---
## Lab #3 :  Création d'un pipeline Multi-stages / Multi-jobs

Mixer les 2 pipelines précédent pour créer un pipeline multi-stages et multi-jobs.

Ajouter un 4ème stage, différent des stages par défaut.

Observer que les stages s'exécutent de façon séquentielle, mais que les jobs s'exécutent en parallèle dans chaque stage.

Solution : [stages-and-jobs_gitlab-ci](stages-and-jobs_gitlab-ci.yml)

![](img/pipeline-multi-stage-jobs.png)


