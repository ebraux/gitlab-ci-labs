# Utilisation des pages


---
## Objectif : Publication d'une page Web "Hello World"

- Mise en place d'une page publique, affichant "Hello World".
- La page est généré de façon très basique en créant du contenu dans un fichier texte en ligne de commande :

``` bash
mkdir public
echo "<h1>Hello World</h1>" > public/index.html
```

---
## Principe

Gitlab propose un espace de publication de contenu.

Le mécanisme "pages" permet de publier du contenu statique. Que ce soit un simple fichier texte, ou un site web généré avec un outil comme [mkdocs](https://www.mkdocs.org/), [sphinx](https://www.sphinx-doc.org/en/master/), [jekyll](https://jekyllrb.com/), [Hugo](https://gohugo.io/), ...

Ce mécanisme est très souvent utilisé pour gérer la documentation d'un projet.

Pour utiliser le mécanisme pages, il suffit de créer un job, dont le nom est `pages`, et qui écrit du contenu dans un artifact dont le nom est `public`.

Lors de l'execution du pipeline, Gitlab va transférer le contenu de l'artifact `public`, vers l'espace de publication.

Par défaut l'url d'accès est au format `https://NOM-USER-OU-GOUP.gitlab.io/NOM-DU-PROJET`


!!! note

        Pour un serveur Gitlab hébergé, c'est au gestionnaire de l'instance d'activer ce service, et de définir le domaine d'accès aux contenus générés par pages (gitlab.io pour le serveur gitlab.com)


Si aucun job de type pages n'a été défini, Gitlab propose un assistant qui va permettre de générer un fichier `.gitlab-ci.yml` contenant le job pages.

L'assistant n'a pas une grande plus value, et si il va remplacer le fichier .gitlab-ci.yml, écrasant donc d'éventuel jobs pré-existants.

Dans la plupart, le job "pages" est créé manuellement manuellement.


---
## Création du pipeline avec l'assistant Gitlab

!!! warning

        L'assistant va générer un nouveau fichier `.gitlab-ci.yml`, qui va écraser celui existant


- Accéder au menu de configuration de "pages" :

![](./img/menu_pages.png)

- **Phase 1 :** définition de l'image Docker à utiliser pour générer le site statique. Ici, seule la commande "echo" est utilisée, une image "Alpine" est donc adaptée :

![](./img/1_image.png)

- **Phase 2 :** définition d'un script de préparation. Ici aucun script de ce type n'est nécessaire

![](./img/2_before.png)

- **Phase 3:** Définition du script à executer.

![](./img/3_script.png)

- **Phase 4 :** execution du pipeline

![](./img/4_pipeline.png)

- **Résultat :** La page est publiée

![](./img/result.png)

Le message "Hello World" s'affiche lorsqu'on consulte l'url constitué du nom du projet (lab-ci), du numéro du job (796145), et du domaine associé à gitlab pages (gitlab.io). Ce qui donne l'url `https://lab-ci-796145.gitlab.io`.


Exemple de [pipeline généré par Gitlab] (./gitlab-ci_hello-generated.yml)


---
## Création du job "pages" manuellement

Si le pipeline contient déjà des jobs, l'assistant Gitlab ne peut pas être utilisé. Il faut donc éditer le fichier de pipeline et ajouter un job "pages". 

Ce qui donne par exemple pour la page :

- Job `pages` seul : [hello world](./pages-hello_gitlab-ci.yml)
- Job `pages` intégré à d'autres jobs : [hello world + test](./pages-hello-plus-test_gitlab-ci.yml)


---
## Configurations complémentaires

- Par défaut, Gitlab propose une url unique pour chaque job mais il est possible de configurer une url fixe. Pour configurer une url fixe pour accéder au site "pages" du projet décocher l'utilisation d'un domaine unique :

![](./img/non_unique.png)

- Il est également possible de définir le protocole d'accès http, ou https. 

- Il est possible de configurer sont propre domaine. Dans ce cas, il faut également
    - Acheter le domaine
    - Gérer la déclaration DNS vers le serveur pages
    - Intégrer un certificat pour le https, ou utiliser [Let's Encrypt](https://letsencrypt.org/)

- Enfin, il est possible d'obtenir une adresse au format `https://NOM-USER-OU-GOUP.gitlab.io`, en créant un projet du même nom que USER/GROUP.


---
## Gestion de l'accès

Par défaut, pour un projet privé, les pages sont en accès privé, et pour un projet public, elle sont en accès public. 

L'accès peut être configuré, par exemple pour rendre les pages d'un projet privé accessibles à tous.

- Menu de configuration du projet :

![](./img/settings_menu.png)

- Section "Visibility, project features, permissions" : 

![](./img/private-project.png)

- Configuration de l'accès : 

![](./img/public-pages.png)


---
## Ressources

- [https://docs.gitlab.com/ee/user/project/pages/](https://docs.gitlab.com/ee/user/project/pages/)
- [https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/)
- [https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html)

