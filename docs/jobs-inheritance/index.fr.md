# Héritage de job

---
## Introduction

Dans un pipeline de CI, il est assez fréquent que des jobs réalisent des taches dans des contextes proches, voir même des taches quasiment similaires.

Par exemple pour l'execution de tests, pour la création d'image Docker, ou pour le déploiement dans différents environnements.


---
## Définition de paramètres globaux

Gitlab permet de factoriser certains paramètres d'un job en les plaçant dans la partie principale du fichier `.gitlab-ci.yaml`.

Dans ce cas, ces paramètre s'appliquent à tous les jobs. Par exemple `image`, `variables`, `before_script`, ...

Dans cet exemple, `image` et `before_script` s'appliqueront aux jobs `coding_style` et `test_code` :

``` yaml
image: python:3.7

before_script:
    - pip install -r requirements.txt

coding_style:
  stage: test
  script:
    - pip install pycodestyle
    - pycodestyle .

test_code:
  stage: test
  script:
    - pip install pytest
    - PYTHONPATH=. pytest tests
  allow_failure: true
```

---
## Utilisation des ancres et des alias de la syntaxe yaml

La syntaxe yaml permet également de factoriser du code, avec l'utilisation des ancres `&` et des alias `*`.

Dans l'exemple ci dessous, une ancre est définie, associée à `coding_style`.  Dans le job `test_code` un alias permet de reprendre les éléments définis dans le job `coding_style`. La partie `script` est ensuite remplacée, et `allow_failure` ajouté :

``` yaml
coding_style: &template_job
  stage: test
  image: python:3.7
  tags:
    - docker
  before_script:
    - pip install -r requirements.txt
  script:
    - python manage.py test

test_code:
  <<: *template_job
  script:
    - pip install pytest
    - PYTHONPATH=. pytest tests
  allow_failure: true
```  

Cette syntaxe est devenue assez rare dans les fichiers `.gitlab-ci.yml`. Elle est remplacée par le mécanisme d'héritage de jobs, plus lisible et plus souple.

---
## Le mécanisme d'héritage de job

Dans un fichier `.gitlab-ci.yaml`, le mot clé `extends` permet d' "hériter" de configurations définies dans un autre job.

Dans l'exemple ci dessous, le job `test_code` va reprendre les éléments définis dans le job `coding_style`, mais remplacera la partie `script`, et ajoutera `allow_failure` :

``` yaml
coding_style:
  stage: test
  image: python:3.7
  tags:
    - docker
  before_script:
    - pip install -r requirements.txt
  script:
    - python manage.py test

test_code:
  extends: coding_style
  script:
    - pip install pytest
    - PYTHONPATH=. pytest tests
  allow_failure: true
```  

Ce mécanisme est très utilisé avec le mécanisme de jobs cachés : les jobs dont le nom commence par un point `.` ne sont pas exécutés.

Si on reprend l'exemple précédent avec ce mécanisme : 

``` yaml

.base_test:
  stage: test
  image: python:3.7
  tags:
    - docker
  before_script:
    - pip install -r requirements.txt


coding_style:
  extends: .base_test
  script:
    - python manage.py test

test_code:
  extends: .base_test
  script:
    - pip install pytest
    - PYTHONPATH=. pytest tests
  allow_failure: true
```  

Associé au mot clé `include`, cette syntaxe permet de placer les jobs cachés dans d'autres fichiers, afin de créer une bibliothèque de template.

---
## Lab

Sur la base du [fichier de pipeline d'exemple](./files/gitlab-ci.yml), comprenant :

- Un job `coding_style`, affichant le message `"coding_style script"`
- Un job `test_code`, affichant le message `"test_code script"`
- Chaque job affichant le message `"before script message"`, en amont dans la partie `before_script`

Tester les différentes syntaxes :

- Utilisation des déclarations globales
- Utilisation des ancres et alias yaml
- Utilisation du mot clé `extends` 
- Utilisation d'un job caché


*Propositions de solutions :*

- [Déclarations globales](./files/gitlab-ci_global.yml)
- [Ancres et alias yaml](./files/gitlab-ci_alias.yml)
- [extends](./files/gitlab-ci_extends.yml) 
- [Job caché](./files/gitlab-ci_hiden.yml)

