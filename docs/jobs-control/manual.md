# Déclencher un job manuellement

Gitlab permet de définir des jobs qui ne sont pas exécutés automatiquement, mais doivent l'être manuellement, via l'interface de Gitlab.

Pour ajouter une exécution manuelle sur un job, utiliser l'option :
```bash
  when: manual
```

> Par défaut les jobs définis en execution manuelle ne bloquent pas le stage auquel ils appartiennent, car la variable 'allow_failure' est fixée à "True". Si on souhaite suspendre le pipeline tant que le job n'a pas été déclenché, il faut ajouter `allow_failure: false` au job.

- *[https://docs.gitlab.com/ee/ci/yaml/#when](https://docs.gitlab.com/ee/ci/yaml/#when)*
- *[https://docs.gitlab.com/ee/ci/yaml/#allow_failure](https://docs.gitlab.com/ee/ci/yaml/#allow_failure)*


---
## Lab : Job avec déclenchement manuel

Créer un pipeline contenant un job de déploiement qui doit être déclenché manuellement

[Proposition de solution](./files/manual_gitlab-ci.yml)

![](./img/pipeline-manual.png       )
