# Gestion des erreurs

Gitlab permet de :

- Définir si un job en erreur est bloquant ou non.
- Gérer les erreurs pour, par exemple, déclencher un job de rollback, ou sécuriser un déploiement en cas d'erreur sur un job.


Pour définir si un code d'erreur en valeur de retour d'un scripts est bloquant ou non, utiliser l'option :
```bash
  allow_failure: true / false
```

---
##  Définir si un job en erreur est bloquant ou non

Pour définir si un code d'erreur en valeur de retour d'un scripts est bloquant ou non, utiliser l'option :
```bash
  allow_failure: true / false
```

Quand un job est en erreur, le pipeline s'arrête au stage contenant le job. Mais si ce stage comprend d'autres jobs, ils seront exécutés.

La valeur par défaut est **false**, un erreur bloque donc le job.

Le traitement est toutefois différent pour les job avec déclenchement manuel *(voir [Déclencher un job manuellement](./manual.md))* :

- La valeur par défaut est **true**
- Mais si `manual` est utilisé dans une `rules`, la valeur par défaut devient vraie.

Il est donc plus prudent de défini systématiquement `allow_failure` quand on utilise des job avec déclenchement manuel.

- *[https://docs.gitlab.com/ee/ci/yaml/#allow_failure](https://docs.gitlab.com/ee/ci/yaml/#allow_failure)*


---
## Gestion des erreurs

Pour définir un job qui **sera** exécuté en cas d'erreur, utiliser l'option :
```bash
  when: on_failure
```


!!! warning

    Si `allow_failure: true` a été défini pour un job, alors le job sera considéré comme exécuté avec succès, et `on_failure` ne provoquera pas de déclenchement.


!!! warning

    L'option `when: on_failure` est activée en cas d'erreur dans les stages précédents, mais pas dans le stage en cours.

- *[https://docs.gitlab.com/ee/ci/yaml/#when](https://docs.gitlab.com/ee/ci/yaml/#when)*


---
## Lab #1 : Traitement d'un job en erreur

- Créer un pipeline avec 2 jobs dans le stage test, et un job dans le stage deploy
- Modifier un des job pour qu'il soit en échec
    - Observer l'impact sur l'autre job de test
    - Observer l'impact sur le job de deploy
- Modifier le pipeline pour que le test en échec ne soit pas bloquant

!!! tip

    Pour gérer un job en erreur, utiliser un script avec `/usr/bin/true` ou `/usr/bin/false`.

Propositions de solution :

- [Pipeline avec un job en échec](./files/errors-allow-failure_gitlab-ci.yml) : 

![](./img/errors-allow-failure_gitlab-ci.png)  

- [Pipeline avec un job en échec non bloquant](./files/errors-allow-failure-true_gitlab-ci.yml) : 

![](./img/errors-allow-failure-true_gitlab-ci.png)

---
## Lab #2 : Job avec déclenchement manuel en erreur

Ce lab reprend le scénario du lab #1, mais avec un des 2 jobs du stage de test en déclenchement `manuel`.

- Créer un pipeline avec 2 jobs, dont 1 en déclenchement manuel, dans le stage test, et un job dans le stage deploy 
- Modifier le job avec déclenchement manuel des job pour qu'il soit en échec
    - Observer l'impact sur l'autre job de test
    - Observer l'impact sur le job de deploy

Propositions de solution :

- [Pipeline avec un job manuel](./files/errors-allow-failure-manual_gitlab-ci.yml) : 

![](./img/errors-allow-failure-manual_gitlab-ci.png)



---
## Lab : 

- Créer un pipeline avec 2 jobs en erreur.
- Gérer l'arrêt du pipeline ou non en cas d'erreur sur un job.

!!! tip

    Pour gérer un job en erreur, utiliser un script avec `/usr/bin/true` ou `/usr/bin/false`.

Solution : [allow_failure_gitlab-ci](./files/allow_failure_gitlab-ci.yml)

![](img/pipeline-allow_failure.png)


 
---
## Lab : Déclenchement d'un job pour traiter les erreurs

- Créer un pipeline avec 2 jobs en erreur.
- Déclencher un job spécifique qui s'execute pour gérer des erreurs sur le job #2
- Déclencher un job qui génère un rapport en cas d'erreur sur un des 2 jobs.
- Vérifier le fonctionnement en supprimamnt les erreurs sur les jobs.


Solution : [on_failure_gitlab-ci](./files/on_failure_gitlab-ci.yml)

![](img/pipeline-on_failure.png)

![](img/pipeline-on_failure-no-error.png)

