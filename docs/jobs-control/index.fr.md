# Gestion des pipeline et jobs



Un pipeline de CI Gitlab est composé de [Jobs et de stages](../pipelines/index.md)


Gitlab propose de nombreuses fonctionnalités permettant de controller l'exécution des jobs :

- Définir des jobs qui seront exécutés [manuellement](./manual.md), via l'interface de Gitlab
- [Gérer les erreurs](./errors.md) sur les jobs (bloquant ou non, rollback, ...)
- Filtrer le déclenchement des jobs (messages de commit, branche, merge request, ...)
    - Ne plus utiliser [Only/except](./only-except.md)  !! déprécié
    - [Utiliser des `rules`](./rules.md)
- Déclencher des jobs lors de la [modification de fichier/dossier](./changes.md)


---
## Ressources

- [Controller l'execution des jobs](https://docs.gitlab.com/ee/ci/jobs/job_control.html)