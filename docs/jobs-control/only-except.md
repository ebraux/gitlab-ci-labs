# Utilisation des mots clé only/except

Les mots clé `only` et `except` sont dépréciés depuis la version 13.0 de Gitlab (May 22nd, 2020).

On trouve encore beaucoup d'exemples d'utilisation, parfois même dans le documentation de Gitlab, car même si leur développement est arrêté, ces mots clé fonctionnent encore.

Mais Gitlab recommande de les remplacer par le mot clé `rules`.


---
## Différence entre `only` et `rules` :

- **only** :
    - Plus simple et direct, mais limité en termes de logique conditionnelle.
    - Ne permet pas d'utiliser des conditions aussi riches ou précises que `rules`.
    - Bon pour des cas basiques comme exécuter des jobs sur des branches spécifiques, des merge requests, ou des tags.

- **rules** :
    - Plus flexible et puissant.
    - Permet des conditions plus complexes, comme l'utilisation de variables d'environnement, des combinaisons de branches, des événements spécifiques (par exemple, `merge_request_event`, `push`, etc.).
    - Peut définir des comportements conditionnels multiples dans un seul job.

GitLab recommande `rules` :

- **Flexibilité** : Avec `rules`, vous pouvez combiner des conditions basées sur des événements (`push`, `merge_requests`, `schedules`), des branches, des tags, ou même des valeurs de variables d'environnement.
- **Lisibilité** : Les pipelines deviennent plus clairs, car `rules` permet de regrouper plusieurs conditions dans un seul job.
- **Remplace `only/except`** : `rules` peut complètement remplacer les directives `only` et `except`, qui sont limitées à des cas d'utilisation simples.


---
## Quelques exemples de conversion

- Restriction d'exécution à une branche **`only: [branches]`** :
``` yaml
only:
    - main
```
``` yaml
 rules:
    - if: $CI_COMMIT_BRANCH == "master"
```
- Restriction d'exécution à la création d'une merge request **`only: merge_requests`** :
```yaml
only:
  - merge_requests
```
```yaml
rules:
  - if: $CI_PIPELINE_SOURCE == "merge_request_event"
```
- Restriction d'exécution en fonction de tags **`only: tags`** :
``` yaml
only:
  - tags
```
``` yaml
rules:
  - if: $CI_COMMIT_TAG != null
``` 
- Restriction d'exécution en lors de pipelines planifiés. **`only: schedules`** :
``` yaml
except:
    - schedules
```
``` yaml
rules:
  - if: $CI_PIPELINE_SOURCE != "schedule"
``` 


---
## Ressources

- [https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/#auto-devops-and-secure-configuration-templates-are-changing-to-rules-instead-of-onlyexcept](https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/#auto-devops-and-secure-configuration-templates-are-changing-to-rules-instead-of-onlyexcept)
- [https://kgaut.net/snippets/2020/gitlab-ci-migrer-depuis-les-only-exept-vers-les-rules.html](https://kgaut.net/snippets/2020/gitlab-ci-migrer-depuis-les-only-exept-vers-les-rules.html)
- ChatGPT : [https://chatgpt.com/](https://chatgpt.com/)


