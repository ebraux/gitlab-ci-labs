# Déclencher des jobs lors de la modification de fichier/dossier

Le mot clé `Change` permet de ne déclencher un job que lorsqu'un fichier, ou dossier a été modifié.

Il prend en paramètre un tableau, la syntaxe pouvant être :

- Le chemin vers un fichier, ex `Dockerfile`
- Le contenu d'un dossier, ex `config/*`
- Le contenu d'un dossier et de ses sous dossiers, ex `config/**/*`
- Uniquement certains fichiers en fonction de leur extension, ex :
    - A la racine, ou dans tout le dépôt, avec l'utilisation des doubles quotes, respectivement `"*.json"` et `"**/*.json"`
    - Dans un dossier `config/*.yml`
    - En indiquent des extensions mutltiples `app/*.{py,sh,js}`

!!! note

    Les chemins peuvent inclure des variables de CI/CD.

Lors des push sur une branche, la condition de déclenchement est, en toute logique, validée par comparaison avec le commit précédent sur la branche.

Mais dans d'autres situations, les conditions de déclenchement peuvent être trompeuses :

- Avec des pipeline de Merge Request, la comparaison est faite avec la branche cible de la MR.
- Lors de la création d'une nouvelle branche, la condition est toujours vraie.
- C'est également le cas lors d'execution manuelle, et globalement dans tous les déclenchement qui ne sont pas liés à un push.

Il est donc nécessaire de gérer des exceptions, afin dd'éviter de déclencher des actions non souhaitées.

Historiquement, le mot clé `changes` pouvait être utilisé directement dans un job. Il est devenu `rules:changes` car il est désormais obligatoire de l'associer à `rules`, pour clarifier son déclenchement.


Exemples : 

- Le job ci dessous s'exécutera lorsque le fichier README.md sera modifié, mais également lors de la création d'une branche, et ...
``` yaml
test-change:
  script:
    - echo "README has changed"
  rules:
    - changes:
      -  README.md    # le fichier README.md
```
- Dans l'exemple ci dessous, le job ne s'exécutera que dans le contexte d'une merge request
``` yaml
test:unit:
  script:
    - echo "Run unit tests"
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" # S'applique uniquement sur les MR
      changes:
        - src/**     # Si des fichiers dans le dossier "src" changent
        - tests/**   # Ou si des fichiers dans le dossier "tests" changent
```
- Dans celui ci, il s'exécutera uniquement lors des commits sur les branches autres que "main"
``` yaml
code:quality:update:
  script:
    - echo "Code quality"
  rules:
    - if: '$CI_COMMIT_BRANCH != "main"'  
      changes:
        - src/**     # Si des fichiers dans le dossier "src" changent
        - tests/**   # Ou si des fichiers dans le dossier "tests" changent
```

---
## Lab : Mise à jour de la documentation

- Mettre en place un job, contenant une règle lui permettant de ne se déclencher que sur la modification du fichier README.md *[proposition de solution](./files/change_gitlab-ci.yml)*
- La valider en : 
    - Modifiant le fichier README.md, puis en faisant un commit
    - Modifiant un autre fichier, puis en faisant un commit
- Tester le fonctionnement lors de la création d'une branche
- Modifier la règle pour qu'elle ne déclenche le job que lors de la modification du fichier README.md, lors d'un commit sur la branche `main` *[proposition de solution](./files/change-main_gitlab-ci.yml)*
- Refaire les tests précédents

---
## Ressources
 
- [https://docs.gitlab.com/ee/ci/yaml/#ruleschanges](https://docs.gitlab.com/ee/ci/yaml/#ruleschanges)
