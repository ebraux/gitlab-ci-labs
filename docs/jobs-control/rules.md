# Les rules
  
---
## Introduction

Les `rules` permettent de définir quand un job va être exécuté. Une `rules` peut filtrer l'execution d'un jobs, à partir de nombreux critères : 

- Le type d'action : commit, merge request, ...
- La branche sur laquelle est fait le push
- Une chaîne dans le message de commit
- Des tags
- ...

Ces critères peuvent être combinés en utilisant des opérateurs logiques *(voir les exemples ci après)* :

- Les règles placées dans des lignes séparées sont combinée par l'opérateur `OU`
- Pour combiner des règles avec l'opérateur `ET`, les placer sur la même ligne, et utiliser le sympbole `||`.


Les `rules` remplacent et étendent les mots clés `only/except`, qui sont dépréciés *[voir Utilisation des mots clé only/except](./only-except.md)*


---
## Exemple de règles

- Déclenchement sur la branche `main`
```yaml
  rules:
    - if: '$CI_COMMIT_REF_NAME == "main"'
```
- Déclenchement sur la branche par défaut
```yaml
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```
- Déclenchement lors des merge request
``` yaml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
- Déclenchement lors des merge request ou sur la branch `main`
```yaml
rules:
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
  - if: '$CI_COMMIT_REF_NAME == "main"'
```
- Déclenchement lors des merge request uniquement sur la sur la branche `main`
```yaml
rules:
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_COMMIT_REF_NAME == "main"'
```
- Déclenchement sur les branche qui correspondent à un certain nom : ici "vX.Y.Z-prod"
``` yaml
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^v[0-9]+\.[0-9]+\.[0-9]+-prod$/'
```
- Déclenchement uniquement sur les merge-request ou les push sur les branches contenant ""vX.Y.Z-staging" et ""vX.Y.Z-prod""
``` yaml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && ($CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^v[0-9]+\.[0-9]+\.[0-9]+-staging$/ || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^v[0-9]+\.[0-9]+\.[0-9]+-prod$/)'
      when : always
    - if: '$CI_PIPELINE_SOURCE == "push" &&  ($CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^v[0-9]+\.[0-9]+\.[0-9]+-staging$/ || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^v[0-9]+\.[0-9]+\.[0-9]+-prod$/)'
      when: always
    - when: never
```
- Déclenchement sur certains tags
``` yaml
  rules:
    - if: '$CI_COMMIT_TAG =~ /^RELEASE_v\d+\.\d+\.\d+-(alpha|beta|prod)$/'
      when: always
    - when: never
``` 


---
## Ressources

- [https://docs.gitlab.com/ee/ci/jobs/job_rules.html](https://docs.gitlab.com/ee/ci/jobs/job_rules.html)
