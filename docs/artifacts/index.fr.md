# Utilisation des Artifacts


## Objectifs 

- Utiliser des "artifacts" pour transmettre des fichiers entre des stages.
- Observer le mécanisme d'upload/download
- Personnaliser la gestion des Artifacts

---
## Principe

Dans un pipeline chaque job est indépendant et peut-être exécuté sur un Runner différent. Les runners étant eux même déployés sur des machines voir même des infrastructures différentes.

Cette architecture ne permet donc pas aux jobs de disposer d'un espace pour partager des données, ce qui est parfois nécessaire. Par exemple, un job peut prendre en entrée des données qui proviennent d'un autre job. 

Gitlab propose un mécanisme permettant de rendre transparent le transfert de données entre jobs : les **artifacts**.

Pour pouvoir utiliser un artifact, il faut le déclarer de façon explicite dans le job, en utilisant le mot clé `artifacts`, et en déclarant le chemin des artifacts dans une liste. Par exemple, avec la déclaration ci dessous, le job va pouvoir partager le dossier 'test' avec d'autres jobs. Le chemin est relatif à la racine du dépôt. 

```yaml
artifacts:
    paths:
      - test
``` 

Lorsqu'un artifact est déclaré dans un job, une nouvelle section apparaît dans l'interface de gestion du job, qui permet d'accéder au contenu du ou des artifacts liés au job 

![](./img/artifact-details.png)


Par défaut, chaque job partage ses artifacts avec les jobs du stage suivants. 

Les jobs ne peuvent pas partager d'artifact au sein du même stage, car il faut que le job qui consomme les données s'execute après le job qui les génère.

Le mot clé `needs` permet de gérer de façon plus souple les artifacts. Le fait de créer un lien entre les jobs entraîne le partage des artifacts. Ce qui permet qui rend les échanges possibles entre jobs dans un même stage, ou entre stage ne se suivant pas.



---
## Optimisation de l'utilisation des artifacts

D'un point de vue technique, avec les artifacts les données sont :

- **Uploadés** sur le serveur Gitlab à la fin du job qui le génère
- **Téléchargés** depuis le serveur Gitlab au début du job qui les utilise

S'agissant de stockage et de transfert de données à travers le réseau, leur utilisation peut avoir un impact important sur le temps de traitement d'un job. Il est recommandé d'optimiser leur gestion.

**Pour optimiser le stockage :**

- La durée de conservation des artifacts par défaut est de 30 jours
- Par défaut le dernier artifact généré par un job "successful" est conservé pour toujours.
- La rétention des artifacts peut être configurée avec `expire_in`.
- Pour désactiver ce fonctionnement de façon globale, dans le menu **Settings > CI/CD > Artifacts**, décocher "Keep artifacts from most recent successful jobs"


**Pour éviter les transferts inutiles : **

- Par défaut, les jobs vont télécharger tous les artifacts des jobs ayant réussi dans le job précédent. Pour limiter ou éviter ce fonctionnement, utiliser `dependencies`, ou `needs`.
- Pour créer une liste de fichiers ne devant pas être transférés, utiliser `exclude`.
  

---
## LAB#1 - Création d'un job générant un artifact

- Créer un job `generate-tests-results`, qui va générer du contenu dans un dossier test`, et l'afficher, par exemple avec les commandes shell ci dessous. *[Exemple de solution](./generate-tests-results.yml)*
  
``` bash
echo "Generate Tests results"
mkdir tests
echo "OK" > tests/results.txt
ls -A
cat tests/results.txt
```

- Observer les logs du job :

  ![](./img/basic-job-logs.png)

- Ainsi que les informations le concernant :

  ![](./img/basic-job-infos.png)

- Modifier ce job, pour déclarer le dossier `tests` en tant qu'artifact. *[Exemple de solution](./generate-tests-results-artifact.yml)*

- Observer les logs du job, ainsi que les informations le concernant :

    - Dans les logs, en fin de job, l'artifact est uploadé sur le serveur
  
    ![](./img/artifact-job-logs.png)
  
    - Dans les informations du job, la section permettant de gérer les artifact a été ajouté. 
  
    ![](./img/artifact-job-infos.png)

---
## LAB#2 - Création d'un job utilisant l'Artifact test

Ajouter un job utilisant l'artifact, par exemple en affichant le contenu du fichier avec la commande shell ci dessous. *[Exemple de solution](./display-test-echec.yml)*
  
``` bash
cat tests/results.txt
```

Le job d'affichage échoue avec le message `can't open 'tests/results.txt': No such file or directory`, car le fichier n'existe pas :

- Dans un stage les jobs s'exécutent en parallèle, le job de génération des tests est peut-être toujours en cours d'exécution
- L'artifact "test" n'est de toutes façons pas partagé entre les jobs du même stage


Si les 2 jobs sont placés dans des stages différents, cette fois, ça fonctionne. *[Exemple de solution](./display-test.yml)*

- Observer dans le pipeline, dans le nouveau job, les artifacts sont téléchargés

![](./img/results-logs.png)


Une autre approche consiste à utiliser `needs` pour forcer l'ordre d'exécution, et la propagation des artifacts entre les jobs. *[Exemple de solution](./display-test-needs.yml)*


---
## Ressources :

- [https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html)
- [https://docs.gitlab.com/ee/ci/yaml/#dependencies](https://docs.gitlab.com/ee/ci/yaml/#dependencies)
- [https://docs.gitlab.com/ee/ci/yaml/#artifactsexclude](https://docs.gitlab.com/ee/ci/yaml/#artifactsexclude)
- [https://docs.gitlab.com/ee/ci/yaml/#needs](https://docs.gitlab.com/ee/ci/yaml/#needs)
- [https://about.gitlab.com/blog/2020/12/10/basics-of-gitlab-ci-updated/](https://about.gitlab.com/blog/2020/12/10/basics-of-gitlab-ci-updated/)