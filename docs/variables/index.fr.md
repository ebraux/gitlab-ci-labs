
# Utilisation de variables

## Objectif

- Utiliser des variables dans un un fichier .gitlab.yml
- Utiliser les variables prédéfinies dans Gitlab CI
- Définir des variables personnalisées au niveau de Gitlab CI
- Mettre en oeuvre les options de visibilité des variables

---
## Principe

Gitlab propose un mécanisme de variables qui peuvent ensuite être utilisées dans la chaîne de CI.

Il existe des variables prè-définies par Gitlab. Elle correspondent dans la plupart de cas à des informations en rapport avec l'organisation interne de Gitlab : nom du projet, chemin du projet, ...

En complément, des variables personnalisées peuvent être définies :

- Dans l'interface de Gitlab, dans la section "Variable" de chaque projet, ou groupe
- Directement dans le pipeline (fichier `.gitlab.yml`), soit globalement, soit au niveau d'un job spécifique.

Le contenu des variables personnalisées peut être protégé, afin de ne pas être affiché dans le terminal d'un job, par exemple pour des mots de passe.

Il est également possible d'utiliser des **environnements**, pour définir des valeurs différentes pour une variable en fonction des cas d'usage (développement, test, production, ...) *[voir Utilisation de variables spécifiques aux environnements](../environments/variables.md)*

---
## LAB #1 - Définir des variables dans le fichier .gitlab.yml

Créer un fichier de pipeline, et utiliser la section "variables", qui peut être définie au niveau global, ou job.
```bash
variables:
  MESSAGE: "HELLO WORLD"
```

Et vérifier le résultat dans la console du job

![](img/variables-output.png)


Solutions : 

- Au niveau Global 
    - Solution [variables-file-global_gitlab-ci.yml](variables-file-global_gitlab-ci.yml)
- Au niveau job 
    - Solution [variables-file-job_gitlab-ci.yml](variables-file-job_gitlab-ci.yml)

---
## LAB #2 - Variables pré-définies dans Gitlab

Créer un fichier de pipeline qui :

- Affiche la variable prédéfinie contenant le nom du projet : `CI_PROJECT_NAME`.
- Affiche l'ensemble des variables disponibles pour le job.
  
[Exemple de fichier de pipeline](variables-predefined_gitlab-ci.yml)

Exemple de log :

![](./img/logs-predefined-variable.png)


Quelques variables prédéfinies les plus utilisées :

- CI_COMMIT_BRANCH : la branche qui porte le pipeline
- CI_DEFAULT_BRANCH : le nom de la branche par défaut
- CI_PROJECT_PATH : intéressant si on vient factoriser les scripts de pipeline, pour pouvoir adapter les templates
- GITLAB_USER_NAME : le nom de l'utilisateur qui a déclenché le pipeline


---
## LAB #3 - Variable personnalisée définie au niveau de Gitlab (CI/CD variables)

Déclarer une variable de CI personnalisée :

- Dans le Menu **Settings > CI/CD** :

![](img/variables-menu.png)

- Déplier la section **Variables** :

![](img/variables-detail.png)

- Ajouter une variables `CI_CUSTOM_MESSAGE`, contenant la valeur "Hello World CI" : 

![](img/variables-add.png)

- Puis modifier le pipeline de CI pour afficher cette variables.


[Exemple de fichier de pipeline](variables-custom_gitlab-ci.yml)

Exemple de log :

![](./img/logs-custom-message.png)

---
## LAB #4 - Visibilité des Variables

Des options sont disponibles pour gérer la visibilité des variables.

- **Visible** : valeur par défaut. La variable peut être affichée dans les logs d'un job, par exemple avec la commande `echo`
- **Masked** : la variable ne peut pas être affichée dans les logs du job. elle sera remplacée par `[MASKED]`
- **Masked and Hidden** : la variable n'est pas affichée dans le logs des jobs, mais en plus sa valeur ne peut plus être ni affichée ni modifiée via l'interface.


!!! note

    Les variables "Masked" et "Masked and Hidden" ne peuvent pas contenir de caractère espace.


Créer 2 variables supplémentaires :

- **CI_CUSTOM_MESSAGE_MASKED**, en activant l'option `Masked`
- **CI_CUSTOM_MESSAGE_HIDDEN**, en activant l'option `Masked and Hidden`

Modifier le pipeline de CI pour afficher les 3 variables, [par exemple](./variables-masked-hidden_gitlab-ci.yml)

Observer les logs du job :

![](img/logs-hidden-masked.png)


Les variables "Masked" et "Masked and Hidden" ne sont pas affichées

Dans l'interface de Gitlab :

- Editer la variable **CI_CUSTOM_MESSAGE_MASKED**. Sa valeur peut être modifiée

![](./img/masked-edit.png)


- Editer la variable **CI_CUSTOM_MESSAGE_HIDDEN**. Sa valeur **ne peut pas** être modifiée

![](./img/hidden-edit.png)


---
## LAB #5 - Utilisation de variables dans des variables 

Des variables peuvent être utilisée pour construire d'autres variables. La variable référencée étant remplacée par sa valeur.

Il est également parfois également nécessaire de créer une variable, qui contient une chaîne de caractère référençant une variable d'environnement, sans effectuer le remplacement. Le remplacement se faisant ensuite par exemple durant l'execution du job.

Gitlab supporte ces 2 cas d'usages, au travers du flag "expand".

- Quand le flag expand est activé (par défaut), la variable est remplacée par sa valeur
- Quand le flag expand est désactivé, la variable est laissée telle quelle.

Pour expérimenter ce fonctionnement, déclarer une variable, par exemple `ROOT_PATH`, avec pour valeur `/app`.

![](./img/ROOT_PATH.png)

Puis utiliser cette variable pour définir la valeur d'une autre variable. Par exemple `DEPLOY_PATH`, avec pour valeur `${ROOT_PATH}/deploy`

![](./img/DEPLOY_PATH.png)

![](./img/DEPLOY_PATH_EXPANDED.png)


Observer le résultat en fonction de la valeur de "Expand" : 

- Quand le tag expand est activé, la variable est remplacée par sa valeur. Par exemple `DEPLOY_PATH` contient `/app/deploy`
- Quand le tag expand n'est pas activé, la variable est laissée intacte. Par exemple `DEPLOY_PATH` contient `${ROOT_PATH}/deploy`

[Exemple de fichier de CI](./variables-expanded.yml)

Exemple de log :

![](./img/logs-expand.png)


---
## LAB #6 - Branches protégées

Gitlab propose un mécanisme permettant de rendre des variables disponibles uniquement dans des branches protégées.

![](./img/flag-protected.png)

Un cas d'usage consiste à créer une branche protégée dédiée aux déploiements, et de créer des variables contenant les token d'accès aux environnements de déploiement en activant le mode "Protect variable" , et de créer une branche protégée utilisée pour les déploiements.


--- 
## Ressources

- [https://docs.gitlab.com/ee/ci/variables/](https://docs.gitlab.com/ee/ci/variables/)
- [https://docs.gitlab.com/ee/ci/variables/README.html#mask-a-cicd-variable](https://docs.gitlab.com/ee/ci/variables/README.html#mask-a-cicd-variable)
- [https://docs.gitlab.com/ee/ci/variables/predefined_variables.html](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)