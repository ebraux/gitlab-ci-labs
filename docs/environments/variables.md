# Utilisation de variables spécifiques aux environnements

---
## Introduction

Les environnements permettent de gérer des variables spécifiques, ce qui permet :

- De définir des variables avec une valeur différente en fonction du contexte. Par exemple pour un déploiement, des niveaux de verbosité différents dans les logs, des quotas d' utilisation de ressources différents, ...
- De définir des variables uniquement là où c'est nécessaire, pour, par exemple, améliorer la sécurité (Token d'accès API, ...)

---
## Utilisation de variables d'environnement

Sur la base des 3 environnements `development`, `testing` et `production` créés dans le lab d'[initialisation des environnements](./index.fr.md).

Déclarer des variables suivantes via le menu **"Settings > CI/CD > Variables"**  *[Plus d'informations sur les variables](../variables/index.md)* :

- **APP_NAME** : 
    - Nom de l'application
    - Accessible et identique dans tous les environnements
    - Valeur = "Lab-CI"
- **API_TOKEN** : 
    - Token d'accès à l'API distante (fictif)
    - Accessible uniquement dans l'environnement de production
    - Valeur = "1234" 
- **LOG_LEVEL** : 
    - Niveau de log de l'application
    - Accessible dans tous les environnements, mais avec une valeur différente
    - Valeur :
        - défaut : "ERROR"
        - development : "DEBUG"
        - testing : "INFO"
        - production : "NONE"

Ce qui donne la configuration suivante :

- Pour APP_NAME :

![](./img/var-app_name.png)

- Pour API_TOKEN

![](./img/var-api_token.png)

- Pour LOG_LEVEL

![](./img/var-log_level-default.png)
![](./img/var-log_level-development.png)
![](./img/var-log_level-testing.png)
![](./img/var-log_level-production.png)



Une fois le pipeline exécuté, on retrouve dans les logs de chaque job les variables, dont la valeur est différente en fonction de l'environnement

- Pour le job sans environnement, on obtient les valeur par défaut : APP_NAME="Lab-CI", et LOG_LEVEL= "ERROR"

![](./img/logs-no-env.png)

- Pour le job dans l'environnement `development`, on obtient les valeur : APP_NAME="Lab-CI", et LOG_LEVEL= **"DEBUG"**
  
![](./img/logs-development.png)

- Pour le job dans l'environnement `testing`, on obtient les valeur : APP_NAME="Lab-CI", et LOG_LEVEL= **"INFO"**

![](./img/logs-testing.png)

- Pour le job dans l'environnement `production`, on obtient les valeur : APP_NAME="Lab-CI", et LOG_LEVEL= **"NONE"**, et la variable supplémentaire **API_TOKEN="1234"**

![](./img/logs-production.png)