# Les environnements

---
## Introduction

Les environnements dans Gitlab permettent de décrire dans un pipeline CI, dans quel contexte sont exécutés les jobs. 

Il est également possible de définir :

- Une URL d'accès au code correspondant à l'environnement, ce qui permet de dédier une url d'accès, et éviter les erreurs.
- Un cluster Kubernetes associé à l'environnement, ce qui permet de contrôler  où est déployé le code.

A chaque fois qu'un job est attaché à un environnement dans un pipeline de CI, Gitlab va garder une trace de ce déploiement pour cet environnement. Il conserve la trace de qui a lancé le job, quand, dans quel contexte, et sur quelle branche il a été exécuté.

Les environnements permettent également de gérer des variables, spécifiques au environnements. *[voir Utilisation de variables spécifiques aux environnements](./variables.md)*

!!! info

    Dans ses versions Premium et Ultimate Gitlab propose également un mécanisme de gestion de droits associé aux environments, afin de définir qui est autorisé à publier du code dans chaque environnement.


---
## Création d'un environnement 

Dans un projet, créer les environnements `development`, `testing` et `production`.

- Ouvrir le menu **"Operate > Environment"**
- Puis cliquer sur **"Create environment"**
- Remplir les champs :
    - Name : le nom de l'environnement
    - Description : [optionnel mais recommandé] une description
    - External URL : [optionnel], lien d'accès à l'application déployée dans cet environnement
    - GitLab agent : [optionnel], pour lier l'environnement à un namespace Kubernetes

![](./img/env-develop-create.png)


Le nom des environnements est en général standardisé, mais il peut être personnalisé :

- **development** : Développement, Review
- **testing** : Test de code, qualité de code,  ...
- **production** : Livraison en production, déploiement

!!! warning

    Le nom de l'environnement doit correspondre à un environnement technique et non un environnement fonctionnel. Éviter par exemple un nom comme  `frontend`, qui n'indique pas si c'est un environnement de production ou non


---
## Création d'un environnement directement depuis le pipeline


La création des environnements dans l'interface n'est pas indispensable. Lorsque'un environnement est déclaré dans un job de CI, si il n'existe pas, il est automatiquement créé.


    
---
## Suivi des jobs dans les environments


Créer un pipeline comportant les stages `build`, `test` et `deploy`, ainsi que des jobs dans chaque stage. Un des 3 stages doit contenir 2 jobs.*[Exemple de solution](./gitlab-ci-basic.yml)*

Valider le pipeline, et observer le résultat : 

- On retrouve les jobs dans le pipeline

![](./img/jobs-pipeline.png)

- Aucun job n’apparaît dans les environments

![](./img/environment-no-jobs.png)


Modifier le pipeline pour rattacher les job aux environnements. Pour le stage comportant 2 jobs, n'en rattacher qu'un seul.*[Exemple de solution](./gitlab-ci-environment.yml)*

Valider le pipeline, et observer le résultat : 

- Dans la liste des jobs, rien n'a changé. Mais quand on affiche les détails d'un job, on observe qu'il est maintenant rattaché à un environnement :

![](./img/job-in-environment.png)

- Dans la liste des environnements, on retrouve les jobs qui viennent d'être lancés, rattachés à leur environnement respectif :

![](./img/environment-with-jobs.png)


---
## Ressources 

- [https://docs.gitlab.com/ee/ci/environments/](https://docs.gitlab.com/ee/ci/environments/)
- [https://docs.gitlab.com/ee/ci/yaml/index.html#environment](https://docs.gitlab.com/ee/ci/yaml/index.html#environment)
- [https://docs.gitlab.com/ee/ci/environments/protected_environments.html](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
